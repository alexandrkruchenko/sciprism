var user = {};
var signPapers = [];
var selectedPapers = [];
var myPreferencesPapers = [];
var searchQuery = '';
var searchCategory = 0;
window.onpopstate = function(e){
    if(e.state){
        if ($.isEmptyObject(e.state.data)) {
            $('.modal-wrapper .close').click();
        }
        if (e.state.data.object == 'card') {
            $('#card-page').data('card-id', e.state.data.id).fadeIn();
            loadCard(
                $('#card-page .wrap'), 
                '/ajax/getCard',
                {card_id: e.state.data.id},
                '.chart-semi-card'
            );
        }
        if (e.state.data.object == 'paper') {
            $('#card-page-p').data('paper-id', e.state.data.id).fadeIn();
            loadCard(
                $('#card-page-p .wrap'), 
                '/ajax/getPaper',
                {paper_pubmed_id: e.state.data.id},
                '.chart-semi-paper'
            );
        }
    }
};

$(document).ready(function(){


    var count = 0;
    $('#sign-up-first .btn').click(function(e){
        e.preventDefault();
        var self = $(this);
        self.addClass('active');
        count++;        
        $('#sign-up-first .btn').each(function(){
            if($(this).hasClass('active')){              
               if(count <= 1 ){
                    $(this).text('Loading...');
                }                  
            }            
        });
        var label = '';
        if (self.hasClass('linkedin')) {
            label = 'LinkedIn';
        }
        if (self.hasClass('google')) {
            label = 'Google';
        }
        if (self.hasClass('facebook')) {
            label = 'ORCID';
        }
        if (self.hasClass('email')) {
            label = 'Email';
        }
        if (self.hasClass('mendeley')) {
            label = 'Mendeley';
        }
        if (self.hasClass('researchGate')) {
            label = 'ResearchGate';
        }
        if (self.hasClass('twitter')) {
            label = 'Twitter';
        }
        if (self.hasClass('facebook-f')) {
            label = 'Facebook';
        }
        if (label && !localStorage.getItem('socClick')) {
            gtag('event', 'click', {
                event_category: 'Authorization',
                event_label: label
            });
        }
        localStorage.setItem('socClick', 1);
    });


    if (cardId) {
        $('#card-page').data('card-id', cardId).fadeIn();
        loadCard(
            $('#card-page .wrap'), 
            '/ajax/getCard',
            {card_id: cardId},
            '.chart-semi-card'
        );
        rewriteUrl({object:'card', id: cardId}, '/innovation/' + cardId);
        $('body').css('overflow','hidden');
    }

    if (paperPubmedId) {
        $('#card-page-p').data('paper-id', paperPubmedId).fadeIn();
        loadCard(
            $('#card-page-p .wrap'), 
            '/ajax/getPaper',
            {paper_pubmed_id: paperPubmedId},
            '.chart-semi-paper'
        );
        rewriteUrl({object:'paper', id: paperPubmedId}, '/paper/' + paperPubmedId);
        $('body').css('overflow','hidden');
    } 

    $('.scroll').bind('click',function(e){
        e.preventDefault();
        var anchor = jQuery(this);
        jQuery('html, body').stop().animate({
            scrollTop:jQuery(anchor.attr('href')).offset().top - 70
        }, 1500,'easeInOutExpo');
        pde(e);
    })

    $('.faqs .question ').click(function(){
        var $item = $(this).closest('.item'),
            itemOther = $('.faqs .item');
        
        if(!$item.hasClass('active')){
            itemOther.removeClass('active');
            $item.addClass('active');
            itemOther.find('.answer').slideUp();
            $item.find('.answer').slideDown();
        }else{
            itemOther.removeClass('active');
            itemOther.find('.answer').slideUp();            
        }
    }); 
 
    $('.slider-customer').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        responsive: [
            {
              breakpoint: 800,
              settings: {
                slidesToShow: 2               
              }
            },          
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                arrows: false
              }
            }
        ]        
        
    });

    $('.manage-publication form .btn').click(function(e){
        e.preventDefault();
        $(this).closest('.etap-1').fadeOut(0);
        $('.manage-publication .etap-2').fadeIn(0);
    });
    $('.manage-publication .back').click(function(e){
        e.preventDefault();
        $(this).closest('.etap-2').fadeOut(0);
        $('.manage-publication .etap-1').fadeIn(0);
    });

    $('body').on('click', '.prompt .prompt-close', function(){
        $(this).closest('.prompt').fadeOut();
    });
    
    $('.cont').each(function(){
        var val = parseInt($(this).find('.percent').val());
        var $circle = $(this).find('.bar');

        if (isNaN(val)) {
            val = 0; 
        }
        else{
            var r = $circle.attr('r');
            var c = Math.PI*(r*2);

            if (val < 0) { val = 0;}
            if (val > 100) { val = 100;}

            var pct = ((100-val)/100)*c;

            $circle.css({ strokeDashoffset: pct});

            $(this).attr('data-pct',val);
        }    
    });
    
    /*$('.search input').on('input keyup', function(e) {
        var valueText = $(this).val(),
            lengthV = valueText.length;
        if(lengthV >= 3){
            searchDataBySearchBar(valueText);
        }else{
            $('.wrap-search').slideUp();
            $(this).closest('form').removeClass('active');
        }
    });*/

    $('.search input').on('click', function(e) {
        $('.wrap-search').slideDown();
        $(this).closest('form').addClass('active');
    }); 

    $('.search-form .checkbox').click(function(e){
        $(this).closest('form').find('input[type="text"]').focus();
    });
     
    $('.nav-tabs a').click(function(e){
        e.preventDefault();
        $('.nav-tabs a').removeClass('active');
        $(this).addClass('active');
        
    });
    
    $('.all-links-toggle a').click(function(e){
        e.preventDefault();
        $(this).toggleClass('active');
        $('.sidebar .other-links').slideToggle();
        
    });
    
    $('body').on('click', '[data-toggle]', function(e){
        e.preventDefault();
        var currentModal = $(this).closest('.modal-wrapper').attr('id');
        var link = $(this).attr('data-toggle');
        if (currentModal != link) {
            $(this).closest('.modal-wrapper').fadeOut();
        }
        if (link == 'preferences-wrapper') {
            myPreferencesPapers = [];
        }
        $('#'+link).fadeIn();
        $('body').css('overflow','hidden');
    });
    
    $('.modal-wrapper .close').click(function(e){
        $(this).closest('.modal-wrapper').fadeOut();
        $('body').css('overflow-y','auto');
        if (location.href.indexOf('paper') !== -1 || location.href.indexOf('innovation') !== -1) {
            rewriteUrl({}, '/feed');
        }
        
    });
    
    $('.careerFit a').click(function(e){
        e.preventDefault();
        var link = $(this).attr('data-tab');
        $('[data-tab]').removeClass('active');
        $(this).addClass('active');
        
        $('.tabs-Fit .tab').removeClass('active');
        $('#' + link).addClass('active');
    }); 
    
    $('#card-page, #card-page-p').on('click', '.linkTab-card a', function(e){
        e.preventDefault();
        var self = $(this);
        var link = self.attr('data-tab');
        var parent = self.closest('#card-page').length ? 
            self.closest('#card-page') : self.closest('#card-page-p');
        if (link) {
            $('[data-tab]').removeClass('active');
            $(this).addClass('active');
            
            $('.tabs-card .tab').removeClass('active');
            parent.find('#' + link).addClass('active');
        }
    });     
    
    $('.setting-linkTab a').click(function(e){
        e.preventDefault();
        var link = $(this).attr('data-tab');
        $('[data-tab]').removeClass('active');
        $(this).addClass('active');
        
        $('.setting-tab .tab').removeClass('active');
        $('#' + link).addClass('active');
    });     
    
    $('.preferences-tabLink a').click(function(e){
        e.preventDefault();
        var link = $(this).attr('tab-link');
        $('[tab-link]').removeClass('active');
        $(this).addClass('active');
        
        $('.preferences-tabs .tab').removeClass('active');
        $('#' + link).addClass('active');
    }); 
    
    $('.profile--tabLink a').click(function(e){
        e.preventDefault();
        var link = $(this).attr('data-tab');
        $('[data-tab]').removeClass('active');
        $(this).addClass('active');
        
        $('.profile--tabs .tab').removeClass('active');
        $('#' + link).addClass('active');
    });
    
    $('.wrap-check .checkbox').click(function(e){
        e.preventDefault();
        $(this).toggleClass('active');
    });
    
    //bell
    $('.bell').click(function(e){
        e.preventDefault();
        $(this).toggleClass('active');
        $('.notifications').slideToggle();
    }); 
    
    $(document).click(function(e){
        var bell = $('.bell');
        if ($('.bell').hasClass('active')) {
            if ($(e.target).closest('li').length===0) {
                $('.bell').removeClass('active');
                $('.notifications').slideUp();
            }
        }
    });     
    
    
    //sidebar mobile
    $('.toggle-sidebar').click(function(e){
        e.preventDefault();
        $('.toggle-sidebar .toggle').toggleClass('active');
        $('.sidebar').slideToggle();
    });     
    
    //profile header
    $('.toggle-profile').click(function(e){
        e.preventDefault();
        $(this).toggleClass('active');
        $('.profile-nav').slideToggle();
    });     
        
    //mobile menu
    $('.toggle-menu').click(function(e){
        e.preventDefault();
        $(this).toggleClass('active');
        $('.site-header .mob-wrap ').slideToggle();
    });     
    $(document).click(function(e){
        var bell = $('.bell');
        var profile = $('.toggle-profile');
        
        if ($('.bell').hasClass('active')) {
            if ($(e.target).closest('li').length===0) {
                $('.bell').removeClass('active');
                $('.notifications').slideUp();
            }
        }
        if ($('.toggle-profile').hasClass('active')) {
            if ($(e.target).closest('.profile').length===0) {
                $('.toggle-profile').removeClass('active');
                $('.profile-nav').slideUp();
            }
        }       
    });     
    
    
    if($('body').width() < 992 ){

        $('#sidebar').removeAttr('id');

        $(document).mousedown(function(e){
            var input = $('.search input');
            if (input.closest('form').hasClass('active')) {
                if ($(e.target).closest('.search').length===0) {
                    input.closest('form').removeClass('active');
                    $('.wrap-search').slideUp();
                }
            }
        });

    }else{
        
        $(document).click(function(e){
            var input = $('.search input');
            if (input.closest('form').hasClass('active')) {
                if ($(e.target).closest('.search').length===0) {
                    input.closest('form').removeClass('active');
                    $('.wrap-search').slideUp();
                }
            }
        });

        (function(){
        var a = document.querySelector('#sidebar'), b = null, P = 0;
        window.addEventListener('scroll', Ascroll, false);
        document.body.addEventListener('scroll', Ascroll, false);
        function Ascroll() {
          if (b == null) {
            var Sa = getComputedStyle(a, ''), s = '';
            for (var i = 0; i < Sa.length; i++) {
              if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                s += Sa[i] + ': ' +Sa.getPropertyValue(Sa[i]) + '; '
              }
            }
            b = document.createElement('div');
            b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
            a.insertBefore(b, a.firstChild);
            var l = a.childNodes.length;
            for (var i = 1; i < l; i++) {
              b.appendChild(a.childNodes[1]);
            }
            a.style.height = b.getBoundingClientRect().height + 'px';
            a.style.padding = '0';
            a.style.border = '0';
          }
          var Ra = a.getBoundingClientRect(),
              R = Math.round(Ra.top + b.getBoundingClientRect().height - document.querySelector('#center').getBoundingClientRect().bottom);  // селектор блока, при достижении нижнего края которого нужно открепить прилипающий элемент
          if ((Ra.top - P) <= 0) {
            if ((Ra.top - P) <= R) {
              b.className = 'stop';
              b.style.top = - R +'px';
            } else {
              b.className = 'sticky';
              b.style.top = P + 'px';
            }
          } else {
            b.className = '';
            b.style.top = '';
          }
          window.addEventListener('resize', function() {
            a.children[0].style.width = getComputedStyle(a, '').width
          }, false);
        }
        })();
        (function(){
        var a = document.querySelector('#rightbar'), b = null, P = 0;
        window.addEventListener('scroll', Ascroll, false);
        document.body.addEventListener('scroll', Ascroll, false);
        function Ascroll() {
          if (b == null) {
            var Sa = getComputedStyle(a, ''), s = '';
            for (var i = 0; i < Sa.length; i++) {
              if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                s += Sa[i] + ': ' +Sa.getPropertyValue(Sa[i]) + '; '
              }
            }
            b = document.createElement('div');
            b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
            a.insertBefore(b, a.firstChild);
            var l = a.childNodes.length;
            for (var i = 1; i < l; i++) {
              b.appendChild(a.childNodes[1]);
            }
            a.style.height = b.getBoundingClientRect().height + 'px';
            a.style.padding = '0';
            a.style.border = '0';
          }
          var Ra = a.getBoundingClientRect(),
              R = Math.round(Ra.top + b.getBoundingClientRect().height - document.querySelector('#center').getBoundingClientRect().bottom);  // селектор блока, при достижении нижнего края которого нужно открепить прилипающий элемент
          if ((Ra.top - P) <= 0) {
            if ((Ra.top - P) <= R) {
              b.className = 'stop';
              b.style.top = - R +'px';
            } else {
              b.className = 'sticky';
              b.style.top = P + 'px';
            }
          } else {
            b.className = '';
            b.style.top = '';
          }
          window.addEventListener('resize', function() {
            a.children[0].style.width = getComputedStyle(a, '').width
          }, false);
        }
        })()        
    }

    $(document).scroll(function(e){
        if ($(document).scrollTop() >= ($(document).height() - $(window).height()) - 200) {
            var saved = $('.wrap-check .checkbox').hasClass('active');
            if ($('.list-publication.innov-list').is(':visible')) {
                var offset = $('.list-publication.innov-list').data('offset');
                if (offset != -1) {
                    if (!searchQuery && !searchCategory) {
                        loadCardsFeed(saved ? 'following' : 'recent', offset);
                    } else {
                        loadCardsFeed('recent', offset);
                    }
                }
            }
            if ($('.list-publication.pub-list').is(':visible')) {
                var offset = $('.list-publication.pub-list').data('offset');
                if (offset != -1) {
                    if (!searchQuery && !searchCategory) {
                        loadAllRecentPublications(saved ? 'following' : 'recent', offset);
                    } else {
                        loadAllRecentPublications('recent', offset);
                    }
                }
            }
        }
    });

    //click on pin
    $('body').on('click', '.pin', function(e){
        e.preventDefault();
        var cardId = $(this).data('card-id');
        $('#papers-wrapper').data('card-id', cardId);
        loadUserCardPublications(cardId, 0);
    });

    //click on follow innovations or papers
    $('body').on('click', '.follow', function(e){
        e.preventDefault();
        var self = $(this);
        var parent = $(this).closest('.list-publication');
        if (parent.length) {
            var type = parent.hasClass('innov-list') ? 'card' : 'paper';
        } else {
            parent = self.closest('.card-page');
            var type = parent.hasClass('innov-card') ? 'card' : 'paper';
        }
        var objectId = self.data(type + '-id');
        try { followXhr.abort(); } catch(e){}
        followXhr = $.post('/ajax/addToFollow', {
                    object_id: objectId,
                    type: type,
                    _token: csrf_token
                }).done(function() {
                    $('.follow[data-' + type + '-id="'+objectId+'"]').toggleClass('active');
        });
    });
    

    //click on card title
    $('body').on('click', '.list-publication .title, .list-publication .wr-chart a,'+
        ' .list-publication .image a,.modal .related, .wrap-search .list-topics a', function(e){
        e.preventDefault();
        var href = $(this).attr('href');
        var hrefParts = href.split('/');
        var object = href.indexOf('innovation') != -1 ? 'card' : 'paper';
        var id = hrefParts[hrefParts.length - 1];
        if (object == 'card') {
            $('#card-page').data('card-id', id);
            loadCard(
                $('#card-page .wrap'), 
                '/ajax/getCard',
                {card_id: id},
                '.chart-semi-card'
            );
        } else {
            $('#card-page-p').data('paper-id', id);
            loadCard(
                $('#card-page-p .wrap'), 
                '/ajax/getPaper',
                {paper_pubmed_id: id},
                '.chart-semi-paper'
            );
        }
        rewriteUrl({object: object, id: id}, href);
    });

    //user paper pagination
    $('#papers-wrapper .wrap .user-pins').on('click', '.paginattion-new a', function(e) {
        e.preventDefault();
        var offset = $(this).data('offset');
        var cardId = $('#papers-wrapper').data('card-id');
        loadUserCardPublications(cardId, offset);
    });

    $('[tab-link="papers"]').click(function(){
        if ($('#papers .wrap-list').html().trim() == '') {
            loadUserPublications(0);
        }
    });

    //papers pagination
    $('#papers').on('click', '.paginattion-new a', function(e){
        e.preventDefault();
        var offset = $(this).data('offset');
        loadUserPublications(offset);
    });

    $('#papers').on('click', 'input[type="checkbox"]', function(e){
        var self = $(this);
        var id = self.attr('id').replace('paper-', '');
        if (self.is(':checked')) {
            myPreferencesPapers = removeItemFromArray(id, myPreferencesPapers); 
        } else {
            myPreferencesPapers.push(id);
        }
    });

    $('#preferences-wrapper .save').click(function(e){
        e.preventDefault();
        var self = $(this);
        var categories = [];
        $('#preferences-wrapper .tab-areas input').each(function(){
            var self = $(this);
            if (self.is(':checked')) {
                categories.push(self.attr('id').replace('category-', ''));
            }
        });
        sendAjaxOnButtonClick(
            self, 
            '/ajax/saveUserPreferences',
            'post', 
            {
                papers: myPreferencesPapers,
                categories: categories
            },
            function() {
                if (!$('.modal-preferences.modal .prompt').length) {
                    $('.modal-preferences.modal').prepend(
                        '<div class="prompt green" style="padding:10px">'+
                            '<div class="prompt-close"><img src="/img/close-green.svg" alt=""></div>'+
                                '<p><b>Great!</b> Preferences successfull saved!</p>'+
                        '</div>'
                    );
                }
                loadUserPublications(0);

            },
            function() {

            }
        );
    });

    //user pin process
    $('#papers-wrapper').on('change', '.pin-checkbox', function(e) {
        e.preventDefault();
        var self = $(this);
        var tabType = $('.feed-tab.active').data('type');
        var paperId = self.attr('id').replace('ch-pinned-', '');
        var cardId = $('#papers-wrapper').data('card-id');
        var pinsAmountSelector = $('.' + tabType +' [data-card-id="'+cardId+'"]').find('.count');
        var pinsAmount = Number(pinsAmountSelector.text());
        var pinSelector = $('[data-card-id="'+cardId+'"]').find('.count');
        try { xhr.abort(); } catch(e){}
        xhr = $.post('/ajax/addPinToCard', {
                    paper_id: paperId,
                    card_id: cardId, 
                    _token: csrf_token
                }).done(function(response) {
                    if (self.is(':checked')){
                        pinSelector.text(pinsAmount + 1);
                    } else {
                        pinSelector.text(pinsAmount - 1);
                    }
                    //updateImpactAreas();
                });
    });

    $('.top--fead .nav-tabs a').click(function(){
        var self = $(this);
        var type = self.data('type');
        setActiveTabOnFeedPage(self, type);
    });

    $('.top--fead .nav-tabs a').click(function(){
        $('.search--p').remove();
        var saved = $('.wrap-check .checkbox').hasClass('active');
        var type = $(this).data('type');
        if (type == 'innov-list') {
            loadCardsFeed(saved ? 'following' : 'recent', 0);
        } else {
            if (saved) {
                loadAllRecentPublications('following', 0);
            } else {
                loadUserRecentPublications(0, 3);
                loadAllRecentPublications('recent', 0);
            }
        }
    });

    $('[data-toggle="modal-careerFit"]').click(function(e){
        e.preventDefault();
        var target = $(this).attr('data-target');
        $('#modal-careerFit').find('[data-tab="' + target + '"]').trigger('click');
    });

    $('.wrapper-publication .wrap-check .checkbox').click(function(e){
        var tab = $('.list-publication.pub-list').is(':visible') ? 0 : 1;
        var saved = $(this).hasClass('active');
        $('.list-publication').html('');
        if (tab == 0) {
            if (saved) {
                loadAllRecentPublications('following', 0);
            } else {
                loadUserRecentPublications(0, 3);
                loadAllRecentPublications('recent', 0);
            }
        } else {
            loadCardsFeed(saved ? 'following' : 'recent', 0);
        }
    });

    //save papers for user
    $('.manage-publication.modal').on('click', '.add', function(){
        if (selectedPapers.length) {
            var self = $(this);
            sendAjaxOnButtonClick(
                self, 
                '/ajax/addUserPapers',
                'post', 
                {
                    papers: selectedPapers
                },
                function() {
                    window.location.reload();
                },
                function() {

                }
            );
        }
    });

    //search pagination
    $('.manage-publication.modal').on('click', '.paginattion-new a', function(e){
        e.preventDefault();
        var offset = $(this).data('offset');
        var q = $('.manage-publication.modal input').val();
        getUserPapersInPubmed(q, offset);
    });

    //save selected papers
    $('.manage-publication.modal').on('click', '.search-result-papers', function() {
        var self = $(this);
        var id = self.attr('id').replace('ch-pinned-', '');
        if (self.prop('checked')) {
            //save item
            selectedPapers.push(id);
        } else {
            selectedPapers = removeItemFromArray(id, selectedPapers);
        }
    });

    //search button listener
    $('#search_publications').click(function(){
        var self = $(this);
        var inputModal = $('.manage-publication.modal input');
        var searchVal = self.parent().find('input').val();
        inputModal.val(searchVal);
        inputModal.trigger('keyup');
    });

    //search publications
    $('.manage-publication.modal input').keyup(function(){
        var q = $(this).val();
        if (q.length >= 3) {
            getUserPapersInPubmed(q, 0);
        } else {
            $('.manage-publication.modal .popup-search-results').remove();
            $('.manage-publication.modal #input_message').show();
        }
    });

    //add comment
    $('#card-page, #card-page-p').on('click', '#add_card_comment .add', function(e) {
        e.preventDefault();
        var self = $(this);
        var form = self.closest('form');
        var textarea = form.find('textarea[name="comment"]');
        var comment = textarea.val();
        var type = self.closest('#card-page').length ? 'card' : 'paper';
        var parent = self.closest('#card-page').length ? $('#card-page') : $('#card-page-p');
        if (comment) {
            sendAjaxOnButtonClick(
                self,
                form.attr('action'),
                'post',
                {
                    card_id: form.find('input[name="card_id"]').val(),
                    comment: comment,
                    private: form.find('input[name="private"]').is(':checked'),
                    type: type
                }, 
                function (response) {
                    var commentTab = form.closest('.card-comments');
                    var commentList = commentTab.find('.comments-list');
                    var html = '';
                    var commentsCountSelector = parent.find('#commentsCount');
                    var commentsCount = commentsCountSelector.text();
                    if (!commentList.length) {
                        html = '<ul class="comments-list">';
                        html += response.html;
                        html += '</ul>';
                        commentTab.append(html);
                    } else {
                        commentList.prepend(response.html);
                    }
                    commentsCountSelector.text(Number(commentsCount) + 1);
                    textarea.val('');
                }, 
                function () {
                    alert('Error. Please, try later.');
                }
            );
        }
    });

    //show more in card page
    $('#card-page, #card-page-p').on('click', '.card-comments .more', function(e) {
        e.preventDefault();
        var self = $(this);
        var offset = self.data('offset');
        var id = self.data('id');
        var type = self.closest('#card-page').length ? 'card' : 'paper';
        var url = '/ajax/moreComments';
        var wrapper = self.closest('.card-comments').find('.comments-list');
        showMore(
            self,
            wrapper,
            url,
            {
                id: id,
                offset: offset,
                type: type
            }
        );
    });

    $('#card-page, #card-page-p').on('click', '.card-publications .paginattion-new a', function(e) {
        e.preventDefault();
        var self = $(this);
        var offset = $(this).data('offset');
        var type = 'card';
        var objectId = $('#card-page-p').data('paper-id');
        if (self.closest('#card-page').length) {
            var type = 'paper';
            var objectId = $('#card-page').data('card-id');
        }
        getJson(
            '/ajax/moreArticlesForCardPage', 
            {offset: offset, object_id: objectId, type: type}, 
            function(response) {
                self.closest('.card-publications').find('.list').html(response.html);
            }
        );
    });

    $('#settings_form button').click(function(e){
        e.preventDefault();
        var self = $(this);
        var form = self.closest('form');
        sendAjaxOnButtonClick(
            self,
            form.attr('action'),
            'post',
            {
                'update_impact_areas': form.find('input[name="update_impact_areas"]').is(':checked') ? 1 : 0,
                'update_publications_interest': form.find('input[name="update_publications_interest"]').is(':checked') ? 1 : 0,
                'update_technologies_interest': form.find('input[name="update_technologies_interest"]').is(':checked') ? 1 : 0,
                'update_co_authors_joining': form.find('input[name="update_co_authors_joining"]').is(':checked') ? 1 : 0,
                'update_users_areas_impact': form.find('input[name="update_users_areas_impact"]').is(':checked') ? 1 : 0,
                'update_new_matching_jobs': form.find('input[name="update_new_matching_jobs"]').is(':checked') ? 1 : 0,
                'update_new_matching_events': form.find('input[name="update_new_matching_events"]').is(':checked') ? 1 : 0,
                'private_message': form.find('input[name="private_message"]').is(':checked') ? 1 : 0,
                'view_profile': form.find('input[name="view_profile"]').is(':checked') ? 1 : 0,
            },
            function(response) {
                if (response.success && !$('.modal--pSettings .wrap .prompt').length) {
                    $('.modal--pSettings .wrap').prepend(
                        '<div class="prompt green" style="padding:10px">'+
                            '<div class="prompt-close"><img src="/img/close-green.svg" alt=""></div>'+
                                '<p><b>Great!</b> Settings successfull updated!</p>'+
                        '</div>'
                    );
                }
            },
            function() {}
        );
    });

    $('#notifications_readed').click(function(e){
        e.preventDefault();
        try { xhr.abort(); } catch(e){}
        xhr = $.get('/ajax/notificationsReaded', function(){
            $('.bell').removeClass('active').html('');
            $('.notifications').slideUp(function(){
                $('.notifications').addClass('non');
            });
        });
    });

    //statistics
    $('.site-header .search').click(function(){
        return saveEvent('click', 'searchBar');
    });

    /*$('.wrapper-publication').on('click', '.pin', function(){
        return saveEvent('click', 'pinBtn');
    });*/

    $('.manage-publication').on('click', '.add', function(){
        return saveEvent('click', 'publicationAdd');
    });

    $('#search_publications').click(function(){
        return saveEvent('click', 'publicationAdd');
    });

    $('.explore').click(function(){
        return saveEvent('click', 'exploreBtn');
    });

    $('.profile--tabLink #jobs').click(function(){
        return saveEvent('click', 'jobsTab');
    });

    $('.profile--tabLink #networking').click(function(){
        return saveEvent('click', 'networkingTab');
    });

    $('.profile--tabs .survey').click(function(){
        return saveEvent('click', 'survey');
    });

    $('.discover-list .discover-item a').click(function(e) {
        e.preventDefault();
        $('.sing-up.btn').trigger('click');
    });

    /*if (user_authenticated) {
        setInterval(updateImpactAreas, 15000);
    }*/

    if (checkImpactAreas) {
        setInterval(updateImpactAreas, 5000);
    }

    $('.list-publication').on('click', '.show-all-publication', function(e) {
        e.preventDefault();
        var offset = $(this).data('offset');
        loadUserRecentPublications(offset, 5);
    });


    $('.search-form').submit(function(e){
        e.preventDefault();
        var form = $(this);
        searchQuery = form.find('input[name="q"]').val();
        var type = form.find('input[name="type"]').parent().hasClass('active') ? 'card' : 'paper';
        var tabType = type == 'card' ? 'innov-list' : 'pub-list';
        searchCategory = form.find('input[name="category"]:checked').val();
        if (!searchCategory) {
            searchCategory = 0;
        }
        if (!searchQuery) {
            searchQuery = '';
        }
        if (type == 'paper') {
            loadUserRecentPublications(0, 3);
            loadAllRecentPublications('recent', 0);
        } else {
            loadCardsFeed('recent', 0);
        }
        form.removeClass('active');
        $('.wrap-search').slideUp();
        form.find('input[name="q"]').blur();
        setActiveTabOnFeedPage($('.nav-tabs [data-type="' + tabType + '"]'), tabType);
    });

});

/**
 * Load papers feed
 * @param  {string} type
 * @param  {integer} offset
 * @param  {string} q
 * @param  {integer} category
 * @return {void}
 */
function loadCardsFeed(type, offset) {
    try { xhr.abort(); } catch(e){}
    if (offset == 0) {
        $('.list-publication.innov-list').html('');
    }
    xhr = $.getJSON('/ajax/getFeedCards?type=' + type + '&offset=' + offset + '&category_id=' + 
        searchCategory + '&q=' + searchQuery,
        function (response){
            if (response.html) {
                $('.list-publication.innov-list').append(response.html);
            }
            $('.list-publication.innov-list').data('offset', response.offset);
            $('body').trigger('feed_loaded');
            if (response.charts) {
                for (var i = response.charts.length - 1; i >= 0; i--) {
                    buildChart($('.chart-semi' + response.charts[i].id), response.charts[i].data);
                }
            }
            if (offset == 0 && (searchQuery || searchCategory)) {
                addFindInfo(response.count);
            }
    });
}

/**
 * Build chart
 * @param  {string} selector
 * @param  {array} data
 * @return {void}
 */
function buildChart(selector, data){
    if (!data[0].length) {
        return selector.parent().hide();
    }
    selector.highcharts({
        colors: data[1],
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        title: {
            text: null,
            align: 'center',
            verticalAlign: 'middle',
            y: 20
        },
        tooltip: {
            pointFormat: '{series.name} '
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    distance: -10,
                    style: {
                        fontWeight: 'bold',
                        color: 'white',
                        textShadow: '0px 1px 2px black'
                    },
                    formatter: function(){
                        return this.y;
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%']
            }
        },
        series: [{
            type: 'pie',
            name: ' ',
            innerSize: '65%',
            data: data[0]
        }]
    });
}

/**
 * Send Ajax request
 * @param  {object} button
 * @param  {string} url
 * @param  {object} data
 * @param  {function} successCallback
 * @param  {function} errorCallback
 * @return {void}
 */
function sendAjaxOnButtonClick(button, url, method, data, successCallback, errorCallback) {
    data._token = csrf_token;
    $.ajax({
        url: url,
        type: method,
        data: data,
        type: method,
        beforeSend: function() {
            button.attr('disabled', '');
            button.css('background-color', '#ccc');
        },
        success: function(response) {
            button.removeAttr('disabled');
            button.removeAttr('style');
            if (!response.error) {
                successCallback(response);
            } else {
                alert(response.error);
            }
        },
        error: function() {
            errorCallback();
            button.removeAttr('disabled');
            button.removeAttr('style');
        }
    });
}

/**
 * Load more content
 * @param  {object} button
 * @param  {object} container
 * @param  {string} url
 * @param  {object} date
 * @return {void}
 */
function showMore(button, container, url, data) {
    sendAjaxOnButtonClick(
        button, 
        url, 
        'get', 
        data, 
        function(response) {
            container.append(response.html);
            if (response.offset != -1) {
                button.data('offset', response.offset);
            } else {
                button.remove();
            }
        }, 
        function() {

        }
    ) 
}

/**
 * Load user publication for pin function
 * @param  {integer} cardId
 * @param  {integer} offset
 * @return {void}
 */
function loadUserCardPublications(cardId, offset) {
    $.getJSON('/ajax/getUserCardPapers?card_id=' + cardId + '&offset=' + offset, function (response){
        if (response.html) {
            $('#papers-wrapper .wrap .user-pins').html(response.html);
        }
    });
}

/**
 * Load user publication for pin function
 * @param  {integer} cardId
 * @param  {integer} offset
 * @return {void}
 */
function loadUserPublications(offset) {
    $.getJSON('/ajax/getUserPapers?offset=' + offset, function (response){
        if (response.html) {
            $('#papers .wrap-list').html(response.html);
            $('#papers .wrap-list input[type="checkbox"]').each(function(){
                var self = $(this);
                var id = self.attr('id').replace('paper-', '');
                if ($.inArray(id, myPreferencesPapers) != -1) {
                    self.removeAttr('checked');
                }
            });
        }
    });
}

/**
 * Load user publication for pin function
 * @param  {integer} offset
 * @param  {integer} limit
 * @return {void}
 */
function loadUserRecentPublications(offset, limit) {

    if (offset == 0) {
        $('.list-publication.pub-list').html('<a href=""'+
            ' class="show-all-publication" style="display: none;">'+
                'Your other <span class="count-pub">X</span> publications</a>');
    }
    try { uxhr.abort(); } catch(e){}
    uxhr = $.getJSON('/ajax/getUserPublications?offset=' + offset + '&limit=' + limit
        + '&q=' + searchQuery + '&category_id=' + searchCategory, function (response){
        if (response.html) {
            $('.list-publication.pub-list .show-all-publication').before(response.html);
            if (response.charts) {
                for (var i = response.charts.length - 1; i >= 0; i--) {
                    buildChart($('.chart-semi-p' + response.charts[i].id), response.charts[i].data);
                }
            }
            if (response.offset !== -1) {
                $('.show-all-publication').show();
                $('.list-publication.pub-list .show-all-publication').data('offset', response.offset);
                $('.list-publication.pub-list .show-all-publication .count-pub').text(response.sub_count);
            } else {
                $('.list-publication.pub-list .show-all-publication').remove();
            }
        }
    });
}

/**
 * Load all publication for pin function
 * @param  {string} type
 * @param  {integer} offset
 * @return {void}
 */
function loadAllRecentPublications(type, offset) {

    try { xhr.abort(); } catch(e){}
    xhr = $.getJSON('/ajax/getAllPublications?offset=' + offset + '&type=' + type
        + '&q=' + searchQuery + '&category_id=' + searchCategory, function (response){
        if (response.html) {
            $('.list-publication.pub-list').append(response.html);
            if (response.charts) {
                for (var i = response.charts.length - 1; i >= 0; i--) {
                    buildChart($('.chart-semi-p' + response.charts[i].id), response.charts[i].data);
                }
            }
            if (response.offset !== -1) {
                $('.list-publication.pub-list').data('offset', response.offset);
            }
            if (offset == 0 && (searchQuery || searchCategory)) {
                var self = $('.search--p');
                if (self.length) {
                    var span = self.find('span');
                    var currentCount = Number(span.text());
                    var calcCount = currentCount + response.count;
                    span.text(calcCount);
                } else {
                    addFindInfo(response.count);
                }
            }
        }
    });
}

/**
 * Load card(innovation or paper)
 * @param  {object} wrapper
 * @param  {string} url
 * @param  {object} data
 * @param  {object} chartSelector
 * @return {void}
 */
function loadCard(wrapper, url, data, chartSelector) {
    wrapper.html('');
    $.ajax({
        url: url,
        data: data,
        success: function (response){
            if (response.html) {
                wrapper.html(response.html);
            }
            if (response.chart) {
                buildChart($(chartSelector), response.chart.data);
            }
        }
    });
}

/**
 * Load papers in popup
 * @param  {string} q
 * @param  {int} offset
 * @return {void}
 */
function getUserPapersInPubmed(q, offset) {
    $('.manage-publication.modal .not-found').hide();
    $('.manage-publication.modal #input_message').hide();
    $('.manage-publication.modal #loader').show();
    $('.manage-publication.modal .popup-search-results').remove();
    try { xhr.abort(); } catch(e){}
    xhr = $.getJSON('/ajax/getPubmedPaper?q=' + q + '&offset=' + offset, function (reponse){
        $('.manage-publication.modal #loader').hide();
        if (reponse.html) {
            if ($('.manage-publication.modal input').val()) {
                $('.manage-publication.modal .search-popup-conent').append(reponse.html);
                //selected checked items
                $('.manage-publication.modal .search-popup-conent').find('.search-result-papers').each(function(){
                    var self = $(this);
                    var id = self.attr('id').replace('ch-pinned-', '');
                    for (var i = selectedPapers.length - 1; i >= 0; i--) {
                        if (selectedPapers[i] == id) {
                            self.prop('checked', true);
                        }
                    }
                });
            }
        } else {
            $('.manage-publication.modal .search-popup-conent .not-found').show();
        }
    });
}
/**
 * Load papers in popup
 * @param  {string} q
 * @param  {int} offset
 * @return {void}
 */
function getPaperForSignStep(q, offset) {
    $('.step2-sign .papers-alert').show();
    $('.step2-sign .papers-alert .not-found').hide();
    $('.step2-sign .papers-alert .load').show();
    $('.step2-sign .tab-papers').remove();
    try { xhr.abort(); } catch(e){}
    xhr = $.getJSON('/ajax/getPubmedPaperSign?q=' + q + '&offset=' + offset, function (reponse){
        $('.step2-sign .papers-alert .load').hide();
        if (reponse.html) {
            $('.step2-sign .papers-alert').hide();
            if ($('.step2-sign input[name="query"]').val()) {
                $('.step2-sign .top').after(reponse.html);
                //selected checked items
                $('.step2-sign').find('.sign-papers').each(function(){
                    var self = $(this);
                    var id = self.attr('id').replace('title-', '');
                    for (var i = signPapers.length - 1; i >= 0; i--) {
                        if (signPapers[i] == id) {
                            self.prop('checked', true);
                        }
                    }
                });
            }
        } else {
            $('.step2-sign .papers-alert .not-found').show();
        }
    });
}

/**
 * Save event for statistics
 * @param  {string} name
 * @param  {string} object
 * @return {void}
 */
function saveEvent(name, object) {
    return $.post('/saveEvent',{
        name: name,
        object: object,
        _token: csrf_token
    });
}
/**
 * Search data by user
 * @param  {string} q
 * @return {void}
 */
/*function searchDataBySearchBar(q) {
    $('.wrap-search').slideUp();
    $(this).closest('form').removeClass('active');
    try { xhr.abort(); } catch(e){}
    xhr = $.getJSON('/ajax/searchDataBySearchBar?q=' + q, function (reponse){
        if (reponse.html) {
            $('.wrap-search').html(reponse.html);
            $('.wrap-search').slideDown();
            $(this).closest('form').addClass('active');
        } else {
            $('.wrap-search').html('');
        }
    });
}*/

/**
 * Remove item from array
 * @param  {mixed} item
 * @param  {array} array
 * @return {array}
 */
function removeItemFromArray(item, array) {
    for (var i = array.length - 1; i >= 0; i--) {
        if (array[i] == item) {
            array.splice(i, 1);
            break;
        }
    }
    return array;
}

/**
 * Rewrite uri
 * @param  {object} data
 * @param  {string} urlPath
 * @return {void}
 */
function rewriteUrl(data, urlPath){
     window.history.pushState({'data': data}, '', urlPath);
 }

/**
 * Get json helper
 * @param  {string}   url
 * @param  {object}   data
 * @param  {Function} callback
 * @return {Function}
 */
function getJson(url, data, callback) {
    $.getJSON(url + '?' + $.param(data), function(response) {
        return callback(response);
    });
}

/**
 * Set active tab on feed page
 * @param {object} selector
 * @param {string} type
 */
function setActiveTabOnFeedPage(selector, type) {
    $('.top--fead .nav-tabs a').removeClass('active');
    selector.addClass('active');
    $('.list-publication').fadeOut(0);
    $('.list-publication.' + type).fadeIn(0);
}

/**
 * Add find info
 * @param {integer} count
 */
function addFindInfo(count) {
    $('.search--p').remove();
    $('.top--fead').after('<p class="search--p"><span>' + count + '</span> items found</p>');
    $('.search--p').trigger('findCountLoaded');
}

function updateImpactAreas() {
    if (!$('.side--impact .percent-ul').length) {
        $.get('/ajax/updateImpactAreas', function(response){
            if (response.html) {
                $('#sidebar .side--impact').html(response.html);
                initImpactAreas(response.colors, response.data);
            }
        });
    }
}