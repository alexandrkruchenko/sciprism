$(document).ready(function(){

    loadUserPublicationsForProfile(0);
    loadUserTabData(
        '/ajax/getUserJobs', 
        0, 
        '', 
        '', 
        0, 
        function(html){
            $('.row-table-network.jobs').html(html);
        }
    );


    $('.add-education, .add-experience').click(function(e){
        e.preventDefault();
        var self = $(this);
        var parent = self.parent();
        var template = parent.find('.hidden');
        var html = template.html();
        var oldIndex = self.data('count');
        var newIndex = oldIndex + 1;
        self.before('<div class="row">' + html + '</div>');
        template.find('input').each(function(){
            var inputName = $(this).attr('name');
            var newInputName = inputName.replace('[' + oldIndex + ']', '[' + newIndex + ']');
            $(this).attr('name', newInputName);
        });
        self.data('count', newIndex);
    });

    $('.edit--info label').click(function(e){
        var imgSelector = $('#upload_avatar input').trigger('click');
    });

    $('#upload_avatar input').change(function (){
        var image = $(this)[0].files[0];
        var filename = image.name.toLowerCase();
        var filenameExt = filename.split('.').pop();
        if ((image.size / 1024) < 800) {
            if (filenameExt == 'jpg' || filenameExt == 'png' || filenameExt == 'gif') {
                var form = new FormData();
                form.append('image', image);
                form.append('_token', csrf_token);
                $.ajax({
                    url: '/ajax/saveUserImg',
                    data: form,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    beforeSend: function() {
                        $('.edit--info label').attr('style', 'background-color:#ccc');
                        $('.edit--info label').text('Please, wait...');
                    },
                    success: function(response) {
                        $('.edit--info label').removeAttr('style');
                        $('.edit--info label').text('Select file');
                        $('.edit--info input[name="avatar"]').val(response.img);
                        $('.edit--info .photo img').attr('src', response.img);
                    }
                });
            } else {
                showProfileError('Wrong file extension.');
            }
        } else {
            showProfileError('The file is larger than 800Kb.');
        }
    });

    //validation
    $('#profile_form .save').click(function(e){
        e.preventDefault();
        var name = $('#profile_form input[name="name"]').val();
        var last_name = $('#profile_form input[name="last_name"]').val();
        var title = $('#profile_form input[name="title"]').val();
        var educationOrExperienceBlockError = 0;
        if (
            name == '' || 
            last_name == '' ||
            name.length > 128 ||
            last_name.length > 128 ||
            name.replace(/\D/g, '') != '' ||
            last_name.replace (/\D/g, '') != ''
        ) {
            return showProfileError('First Name and Last Name'+
                ' can not be empty or more than 128 characters and contain digits');
        }
        if (title.length > 190) {
            return showProfileError('Title '+
                ' can not be more than 190 characters');
        }
        $('.education--info .row:visible input, .experience--info .row:visible input').
            each(function(){
                var val = $(this).val();
                var name = $(this).attr('name');
                if ((name.indexOf('school') != -1 ||
                    name.indexOf('title') != -1 ||
                    name.indexOf('company') != -1) &&
                    !val ||
                    val.length > 190) {
                    educationOrExperienceBlockError = 1;
                    return showProfileError('School, Title, Company in Education or Experience block'+
                        ' can not be empty or more than 190 characters');
                }
                if ((name.indexOf('from_year') != -1 || 
                    name.indexOf('to_year') != -1) &&
                    (!$.isNumeric(val) || String(val).length > 4)) {
                    educationOrExperienceBlockError = 1;
                    return showProfileError('Year in Education or Experience block'+
                        ' can not be more than 4 numerals');
                }
        });
        if (!educationOrExperienceBlockError) {
            return $('#profile_form').submit();
        }
        return false;
    });

    //user expertise
    $('#expertise').keypress(function(e){
        if (e.keyCode == 13) {
            var self = $(this);
            var val = self.val();
            if (val != '') {
                $.ajax({
                    url: '/ajax/addExpertise',
                    type: 'post',
                    data: {
                        name: val,
                        _token: csrf_token
                    },
                    success: function(response) {
                        if (response.success) {
                            var template = $('.tags li.hidden').clone();
                            template.find('span').text(val);
                            template.removeClass('hidden');
                            $('.tags').append(template);
                        }
                        self.val('');
                    }
                });
            }
        }
    });
    $('.tags a .close').click(function(e){
        e.preventDefault();
        var self = $(this);
        var val = self.parent().find('span').text();
        $.ajax({
            url: '/ajax/removeExpertise',
            type: 'post',
            data: {
                name: val,
                _token: csrf_token
            },
            success: function() {
                self.closest('li').remove();
            }
        });
    });

    //publications pinned cards
    $('.your-publications').on('click','.pins-count', function(e){
        e.preventDefault();
        var paperId = $(this).data('paper-id');
        if (paperId) {
            $('#pinned-wrapper').data('paper-id', paperId);
            loadUserPinnedCards(paperId, 0);
        }
    });

    //user card pagination
    $('#pinned-wrapper .wrap').on('click', '.paginattion-new a', function(e) {
        e.preventDefault();
        var offset = $(this).data('offset');
        var paperId = $('#pinned-wrapper').data('paper-id');
        loadUserPinnedCards(paperId, offset);
    });

    //user pin process
    $('#pinned-wrapper').on('change', '.pin-checkbox', function(e) {
        e.preventDefault();
        var self = $(this);
        var paperId = $('#pinned-wrapper').data('paper-id');
        var cardId = self.attr('id').replace('ch-pinned-', '');
        var pinSelector = $('[data-paper-id="'+paperId+'"]');
        var pinsAmount = Number(pinSelector.text());
        try { xhr.abort(); } catch(e){}
        xhr = $.post('/ajax/addPinToCard', {
                    paper_id: paperId,
                    card_id: cardId, 
                    _token: csrf_token
                }).done(function() {
                    if (self.is(':checked')){
                        pinSelector.text(pinsAmount + 1);
                    } else {
                        pinSelector.text(pinsAmount - 1);
                    }
        });
    });

    $('#networking, #jobs').click(function(e){
        var id = $(this).attr('id');
        var url = $('.' + $(this).attr('id')+'.tab').data('get-url');
        var wrapper = $('.row-table-network.'+id);
        var parent = wrapper.closest('.tab');
        if (wrapper.html() == '') {
            loadUserTabData(
                url, 
                0, 
                parent.find('.keywords').val(), 
                parent.find('.location').val(), 
                0, 
                function(html){
                    wrapper.html(html);
                }
            );
        }
    });

    $('.switcher').click(function(){
        var parent = $(this).closest('.tab');
        var url = parent.data('get-url');
        var wrapper = parent.find('.row-table-network');
        loadUserTabData(
            url, 
            0, 
            parent.find('.keywords').val(), 
            parent.find('.location').val(),
            Number($(this).hasClass('active')), 
            function(html){
                wrapper.html(html);
            }
        );
    });

    $('.row-table-network').on('click', '.star', function(e) {
        e.preventDefault();
        var self = $(this);
        var url = self.closest('.tab').data('add-url');
        var object = self.data('object');
        try { xhr.abort(); } catch(e){}
        xhr = $.post(url, {
                    object: object, 
                    _token: csrf_token
                }).done(function() {
                    self.toggleClass('active');
        });
    });

    $('.row-table-network').on('click', '.paginattion-new a', function(e) {
        e.preventDefault();
        var self = $(this);
        var parent = self.closest('.tab');
        var url = self.closest('.tab').data('get-url');
        var offset = self.data('offset');
        var wrapper = self.closest('.tab').find('.row-table-network');
        var saved = Number(parent.find('.switcher').hasClass('active'));
        var keywords = parent.find('.keywords').val();
        var location = parent.find('.location').val();
        self.closest('div').html('<p>Wait...</p>');
        loadUserTabData(url, offset, keywords, location, saved, function(html){
            wrapper.html(html);
        });
    });

    $('.profile--tabs .form-search input').keyup(function(e){
        var self = $(this);
        var parent = self.closest('.tab');
        var val = self.val();
        var url = self.closest('.tab').data('get-url');
        if (val.length >= 3 || val == '') {
            var callback = function(html){
                parent.find('.row-table-network').html(html);
            }
            loadUserTabData(
                url, 
                0, 
                parent.find('.keywords').val(), 
                parent.find('.location').val(),
                0, 
                callback
            );
        }
    });

    $('[data-toggle="modal-careerFit"]').click(function(e){
        e.preventDefault();
        var target = $(this).attr('data-target');
        $('#modal-careerFit').find('[data-tab="' + target + '"]').trigger('click');
    });

    $('.your-publications .list').on('click', '.paginattion-new a', function(e){
        e.preventDefault();
        var self = $(this);
        var offset = $(this).data('offset');
        loadUserPublicationsForProfile(offset);
    });

});

/**
 * Show in profile edit form errors
 * @param  {string} text
 * @return {void}
 */
function showProfileError(text) {
    $('#modal-profEdit').scrollTop(0)
    $('.edit--info .prompt #text').text(text);
    $('.edit--info .prompt').fadeIn();
}

/**
 * Load user pinned cards
 * @param  {integer} paperId
 * @param  {integer} offset
 * @return {void}
 */
function loadUserPinnedCards(paperId, offset) {
    $.getJSON('/ajax/getUserPinnedCards?paper_id=' + paperId + '&offset=' + offset, function (response){
        if (response.html) {
            $('#pinned-wrapper .wrap').html(response.html);
        }
    });
}

/**
 * Load user tab data
 * @param  {string} url
 * @param  {integer} offset
 * @param  {integer} saved
 * @param  {string} keywords
 * @param  {string} location
 * @return void
 */
function loadUserTabData(url, offset, keywords, location, saved, callback) {
    var q = url + '?offset=' + offset + '&saved=' + saved +
        '&keywords=' + keywords + '&location=' + location;
    try { xhr.abort(); } catch(e){}
    xhr = $.getJSON(q, function (response){
        if (response.html) {
            callback(response.html);
        }
    });
}

/**
 * Load user publications for Profile page
 * @param  {[type]} offset [description]
 * @return {[type]}        [description]
 */
function loadUserPublicationsForProfile(offset) {
    try { pxhr.abort(); } catch(e){}
    pxhr = $.getJSON('/ajax/getUserPublicationsForProfile?offset=' + offset, function (response){
        if (response.html) {
            $('.your-publications .list li:not(:nth-child(1))').remove();
            $('.your-publications .list .paginattion-new').remove();
            $('.your-publications .list li').after(response.html);
        }
    });
}