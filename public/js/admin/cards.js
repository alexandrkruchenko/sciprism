$(document).ready(function() {
    $('.upload-button').click(function () {
        $returnid ="fileid";
        $type ='image';
        $href ='/packages/barryvdh/elfinder/browser.html?tnbrowser='+$returnid+'&type='+$type;
        window.open($href, "popupWindow", "width=1030,height=530,scrollbars=yes");
    });
    $('.form-element-files .btn-danger').click(function(e){
        Admin.Messages.confirm('', 'Are you sure?').then(function(){
            $('input[name="image"]').val('');
            $('.form-element-files a').attr('href', '');
            $('.form-element-files img').attr('src', '');
            $('.form-element-files').hide();
        }, function(){});
    });
});
function BrowseServer(file, id, type) {
    var baseUrl = Admin.Url._url + '/';
    var fileUrl = file.url.replace(baseUrl, '');
    $('input[name="image"]').val(fileUrl);
    $('.form-element-files a').attr('href', file.url);
    $('.form-element-files img').attr('src', file.url);
    $('.form-element-files').show();
}