$(document).ready(function() {
    $('#title').keyup(function(){
        var val = $(this).val().toLowerCase().replace(/ /g, '-').replace(/[^-0-9a-zA-z]/gim,'');
        $('#slug').val(val);
    });
});