<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

Route::group(['middleware' => 'web'], function () {

    //neural network routes
    Route::get('/neuralnetwork/training', 
        ['uses' => 'NeuralNetworkTrainingController@doTraining', 'as' => 'neuralnetwork training']
    );
    Route::get('/neuralnetwork/test', 
        ['uses' => 'NeuralNetworkTrainingController@test', 'as' => 'neuralnetwork testing']
    );
    Route::post('/neuralnetwork/predict', 
        ['uses' => 'NeuralNetworkTrainingController@predict', 'as' => 'neuralnetwork predict proccess']
    );
    Route::get('/neuralnetwork/convert', 
        ['uses' => 'NeuralNetworkTrainingController@convert', 'as' => 'convert file to csv']
    );

    //auth routes
    Route::post('/register', 'Auth\RegisterController@register')->name('register');
    Route::post('/registerEmail', 
        'Auth\RegisterController@registerEmail')->name('registerEmail');
    Route::post('/login', 'Auth\LoginController@login')->name('login');
    Route::post('/loginEmail', 'Auth\LoginController@loginEmail')->name('loginEmail');
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

    //home route
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/category/{slug}', 'MainController@category')->name('category');

    //ajax routes
    Route::get('/ajax/getPubmedPaperSign', 'AjaxController@getPubmedPaperSign')
        ->name('get pubmed articles for sign'); 
    Route::get('/ajax/getPubmedPaper', 'AjaxController@getPubmedPaper')
        ->name('get pubmed articles');
    Route::get('/ajax/getFeedCards', 'AjaxController@getFeedCards')
        ->name('get feed cards');
    Route::get('/ajax/searchDataBySearchBar', 'AjaxController@searchDataBySearchBar')
        ->name('search data by user');
    Route::get('/ajax/getCard', 'AjaxController@getCard')->name('get card data');

     Route::get('/ajax/getPaper', 'AjaxController@getPaper')->name('get paper data');

    //cron routes
    Route::get('/cron/predictAndSetCategory', 'CronController@predictAndSetCategory')
        ->name('set category');
    Route::get('/cron/removeEvents', 'CronController@removeEvents')
        ->name('remove events');
    Route::get('/cron/autopin', 'CronController@autopin')
        ->name('autopin');
    Route::get('/cron/predictAndSetCategoryForPaper', 'CronController@predictAndSetCategoryForPaper')
        ->name('set category paper');

    //static page
    Route::get('/page/{slug}', 'PageController@index')
        ->name('page');

    //blog
    Route::get('/blog', 'BlogController@index')
        ->name('blog');
    Route::get('/blog/{slug}', 'BlogController@post')
        ->name('post');

    //save event
    Route::post('/saveEvent', ['as' => 'save event', function (Request $request) {
        $statistic = new App\Statistic;
        $statistic->event = htmlspecialchars($request->input('name'));
        $statistic->object = htmlspecialchars($request->input('object'));
        if (Auth::user()) {
            $statistic->user_id = Auth::user()->id;
        }
        $statistic->save();
    }]);

    //save subscriber
    Route::post('/subscribers/add', ['as' => 'add subscriber', function (Request $request) {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email|unique:subscribers'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        App\Subscriber::create($request->all());
        return redirect()->back()->with('success', 'You have successfully subscribed!');
    }]);

    //save feedback
    Route::post('/page/contact-us/addFeedback', ['as' => 'add feedback', function (Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'subject' => 'required|string|max:255',
            'email' => 'required|email',
            'message' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        App\Feedback::create($request->all());
        return redirect()->back()->with('success', 'Your feedback is successfully sent!');
    }]);

    //import images feedback
    /*Route::get('/imageInsert', ['as' => 'image insert', function (Request $request) {
        $articles = App\Ssarticles::get();
        foreach ($articles as $article) {
            $parsed = Yaml::parse($article->image);
            $image = 'uploads/admin/images/'.$parsed['normal'];
            $card = App\Card::find($article->id);
            if ($card) {
               $card->image = $image;
               $card->save();
            }
        }
    }]);*/

    //test route
    /*Route::get('/test', ['as' => 'test', function (Request $request) {
        try {
            $image = Image::make('https://media.licdn.com/dms/image/C4E03AQHsVPqRdcgTwQ/profile-displayphoto-shrink_100_100/0?e=1533772800&v=beta&t=zJF45P3LPlWk0Zh7_Ioiwpg_xcW8V3f_VC6K1LfZa3E')->resize(160, 160);
        } catch(Intervention\Image\Exception\NotReadableException $e) {
            $image = Image::make(public_path('/uploads/default_user.png'))->resize(160, 160);
        }
        print_r($image);
    }]);*/

    Route::get('/', ['as' => 'redirect from main', function (Request $request) {
        return redirect()->route('index');
    }]);

});

Route::group(['middleware' => 'auth'], function () {

    //profile routes
    Route::get('/profile', ['as' => 'profile redirect', function (Request $request) {
        return redirect(Auth::user()->getUserLink());
    }]);
    Route::get('/profile/{slug}', 'ProfileController@index')->name('profile');
    Route::post('/profile/save', 'ProfileController@save')->name('save profile');
    Route::post('/profile/saveTestResults', 'ProfileController@saveTestResults')
        ->name('save test results');

    Route::get('/feed', 'MainController@index')->name('index');
    Route::get('/innovation/{id}', 'MainController@index')->name('innovation');
    Route::get('/paper/{id}', 'MainController@index')->name('paper');
    Route::get('/following', 'MainController@following')->name('following');

    //ajax routes
    Route::post('/ajax/addUserPapers', 'AjaxController@addUserPapers')->name('add user papers');
    Route::get('/ajax/getUserCardPapers', 'AjaxController@getUserCardPapers')
        ->name('get user card papers');
    Route::get('/ajax/getUserPapers', 'AjaxController@getUserPapers')
        ->name('get user papers');
    Route::post('/ajax/addPinToCard', 'AjaxController@addPinToCard')
        ->name('add pin to card');
    Route::post('/ajax/addToFollow', 'AjaxController@addToFollow')
        ->name('add card to follow');
    Route::post('/ajax/saveUserImg', 'AjaxController@saveUserImg')->name('save photo');
    Route::post('/ajax/addCardComment', 'AjaxController@addCardComment')->name('add card comment');
    Route::get('/ajax/moreComments', 'AjaxController@moreComments')->name('get more comment');
    Route::get('/ajax/moreArticlesForCardPage', 'AjaxController@moreArticlesForCardPage')
        ->name('get more articles');
    Route::post('/ajax/addExpertise', 'AjaxController@addExpertise')
        ->name('add expertise');
    Route::post('/ajax/removeExpertise', 'AjaxController@removeExpertise')
        ->name('remove expertise');
    Route::get('/ajax/getUserPinnedCards', 'AjaxController@getUserPinnedCards')
        ->name('get user pinned cards');
    Route::post('/ajax/saveUserSettings', 'AjaxController@saveUserSettings')
        ->name('save user settings');
    Route::post('/ajax/saveUserPreferences', 'AjaxController@saveUserPreferences')
        ->name('save user preferences');
    Route::get('/ajax/getUserEvents', 'AjaxController@getUserEvents')
        ->name('get user events');
    Route::post('/ajax/addUserEvent', 'AjaxController@addUserEvent')
        ->name('add user event');
    Route::get('/ajax/getUserJobs', 'AjaxController@getUserJobs')
        ->name('get user jobs');
    Route::post('/ajax/addUserJob', 'AjaxController@addUserJob')
        ->name('add user jobs');
    Route::get('/ajax/notificationsReaded', 'AjaxController@notificationsReaded')
        ->name('notifications readed');
    Route::get('/ajax/updateImpactAreas', 'AjaxController@updateImpactAreas')
        ->name('update impact areas');
    Route::get('/ajax/getUserPublications', 'AjaxController@getUserPublications')
        ->name('get user publications');

    Route::get('/ajax/getAllPublications', 'AjaxController@getAllPublications')
        ->name('get all publications');

    Route::get('/ajax/getUserPublicationsForProfile', 'AjaxController@getUserPublicationsForProfile')
        ->name('get user publications for profile');
});
