<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaperMeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paper_meta', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('paper_id')->index();
            $table->foreign('paper_id')
              ->references('id')->on('papers')
              ->onDelete('cascade')
              ->onUpdate('cascade');
            $table->string('name');
            $table->text('value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paper_meta');
    }
}
