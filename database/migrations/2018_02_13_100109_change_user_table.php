<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('last_name');
            $table->string('title');
            $table->mediumText('summary');
            $table->string('network');
            $table->string('network_user_id');
            $table->string('slug');
            $table->mediumText('network_token');
            $table->mediumText('avatar')->nullable();
            $table->string('ip', 15)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('network_id');
            $table->dropColumn('network_user_id');
            $table->dropColumn('network_token');
            $table->dropColumn('avatar');
        });
    }
}
