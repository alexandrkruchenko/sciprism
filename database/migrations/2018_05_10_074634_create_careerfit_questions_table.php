<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareerfitQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('careerfit_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('question');
            $table->unsignedInteger('question_id')->index();
            $table->foreign('type_id')
              ->references('id')->on('careerfit_question_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careerfit_questions');
    }
}
