<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareerfitProfessionQuestionScoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careerfit_p_q_scores', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('question_id')->index();
            $table->foreign('question_id')
              ->references('id')->on('careerfit_questions')
              ->onDelete('cascade')
              ->onUpdate('cascade');
            $table->unsignedInteger('profession_id')->index();
            $table->foreign('profession_id')
              ->references('id')->on('careerfit_professions')
              ->onDelete('cascade')
              ->onUpdate('cascade');
            $table->enum('score', [1, 2, 3, 4, 5]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careerfit_profession_question_scores');
    }
}
