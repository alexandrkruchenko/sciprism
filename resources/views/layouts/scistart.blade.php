<!DOCTYPE html>
<html lang="en">
<head>
    @if (isset($ga_code)){!! $ga_code !!}@endif
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-97480972-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-97480972-1');
    </script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="@if(isset($meta_description)) {{ $meta_description }} @endif">
    <meta name="keywords" content="@if(isset($meta_keywords)) {{ $meta_keywords }} @endif">
    <meta property="og:title" content="@if(isset($meta_title)) {{ $meta_title }} @endif" />
    <meta property="og:url" content="" />
    <meta property="og:image" content="{{ asset('img/title.jpg') }}" />
   <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        @if(isset($meta_title)) {{ $meta_title }} @else {{ config('app.name', 'Sciprism') }}@endif
    </title>
 
    
    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">    
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    @if (isset($links))
        @foreach($links as $link)
            <link rel="stylesheet" href="{{ asset($link) }}">
        @endforeach
    @endif
    <!-- CSS -->
    @if(Auth::user() && Route::currentRouteName() == 'index')
        <link rel="stylesheet" 
            href="{{ asset('packages/product-tour/css/product-tour.css') }}"> <!-- Resource style -->
    @endif
    
    @if (!Auth::user())
    <!-- Facebook SDK -->
    <script>
      /*window.fbAsyncInit = function() {
        FB.init({
          appId      : '1969549903296676',
          cookie     : true,
          xfbml      : true,
          version    : 'v2.12'
        });
          
        FB.AppEvents.logPageView();   
          
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "https://connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));*/
    </script>
    <!-- /Facebook SDK -->

    <!-- LinkedIn SDK-->
    <script type="text/javascript" src="//platform.linkedin.com/in.js" async defer>
        api_key: 78a3vu7d5gl2ud
    </script>
    <!-- /LinkedIn SDK -->
    
    <!-- Google SDK-->
    <script src="https://apis.google.com/js/platform.js"></script>
    <!-- /Google SDK -->
    @endif
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-97480972-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-97480972-1');
    </script>

</head>
<body>
<script>
    var csrf_token = '{{ csrf_token() }}';
    var user_authenticated = @if (Auth::user()) true @else false @endif;
    var checkImpactAreas = false;
</script>
<header class="site-header header @if (!Auth::user()) new @endif">
    <div class="container">
        <div class="toggle-menu">
            <span class="bar bar1"></span>
            <span class="bar bar2"></span>
            <span class="bar bar3"></span>
        </div>          
        <div class="logo">
            <a href="/feed"><img src="{{ asset('img/logo-mini.jpg') }}" alt=""></a>
        </div>
        @if (Auth::user())
            <div class="search mob-hidden">
                <form class="search-form" action="">
                    <input type="text" name="q"
                        placeholder="Search on SciPrism" value="" autocomplete="off">
                    <input type="submit">
                    <div class="wrap-search">
                        <div class="item">
                            <div class="top">
                                <p>Search</p>
                                <div class="wrap-check">
                                    <span>publications</span>
                                    <label class="checkbox">
                                        <input type="checkbox" name="type">
                                    </label>
                                    <span>innovations</span>
                                </div>                          
                            </div>
                             <div class="tab-areas tab active" id="areas_of_interest">
                                 @for ($i = 0; $i < $amount_categories; $i++)
                                    @if ($i == 0 || $i == ceil($amount_categories / 2))
                                        <div class="col">
                                    @endif
                                        <div class="form-group">
                                            <input type="radio"
                                                id="category-{{ $categories[$i]->id }}" 
                                                name="category" value="{{ $categories[$i]->id }}">
                                            <label for="category-{{ $categories[$i]->id }}" class="checkbox">
                                                {{ $categories[$i]->title }}
                                                <i class="fas fa-check-circle"></i>
                                            </label>
                                        </div>
                                    @if ($i == (ceil($amount_categories / 2) - 1) || $i + 1 == $amount_categories)
                                        </div>
                                    @endif
                                @endfor                
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        @endif
        <div class="nav">
            <ul class="nav-page">
                @if (Auth::user())
                    <li><a 
                        @if (Route::currentRouteName() == 'index')
                            href="#"
                        @else
                            href="/feed?showDemo=true"
                        @endif
                        id="demo">TOUR</a></li>
                @else                       
                    <li><a href="/page/about" class="mob-hidden">ABOUT US</a></li>
                    <li><a href="/page/faq" class="mob-hidden">FAQ</a></li>
                @endif
                @if (Auth::user())
                    <li>
                        <?php
                            $user_notifications = App\UserNotification::where('user_id', Auth::user()->id)
                                ->where('readed', 0)
                                ->limit(25)
                                ->get();
                           $user_notifications_count = count($user_notifications);
                        ?>
                        <a class="bell">
                            @if ($user_notifications_count)
                                <span class="circle"></span>
                            @endif
                        </a>
                        <div class="notifications  @if (!$user_notifications_count) non @endif">
                             <p> 
                                You don’t have any notifications
                             </p>
                            <div class="header-n">
                                <h5>Notifications</h5>
                                <a href="#" id="notifications_readed">
                                    Mark all as read
                                </a>
                            </div>
                            <ul class="list">
                                @foreach($user_notifications as $notification)
                                <li>
                                    <a href="{{ $notification->event_uri }}">
                                        <span class="title">{{ $notification->text }}</span>
                                        <span class="date">
                                            {{ 
                                                App\Services\ProfileService::timeElapsedString(
                                                    $notification->created_at
                                                ) 
                                            }}
                                        </span>
                                    </a>
                                </li>
                                @endforeach                                                     
                            </ul>
                        </div>
                    </li>
                @endif
            </ul>
            @if (Auth::user())
                <div class="profile">
                    <a href="{{ Auth::user()->getUserLink() }}" class="photo">
                        <img src="{{ Auth::user()->avatar }}" alt="">
                    </a>
                    <a href="" class="toggle-profile">
                        <span class="name">
                            {{ Auth::user()->name }} {{ Auth::user()->last_name }}
                        </span>
                        <img src="{{ asset('img/arrow-blue.png') }}" alt="" class="arrow">
                    </a>
                    <ul class="profile-nav">
                        <li><a href="{{ Auth::user()->getUserLink() }}">Profile</a></li>
                        @if (Auth::user()->isAdmin())
                        <li><a href="/admin">Admin Panel</a></li>
                        @endif
                        <li><a href="" data-toggle="modal--pSettings">Settings</a></li>
                        <li><a href="/logout">Log out</a></li>
                    </ul>   
                </div>  
            @else  
                <div class="sing-ul">
                    <li><a href="" class="sing-in" data-toggle="sign-up">Log in</a></li>
                    <li><a href="" class="sing-up btn" data-toggle="sign-up">Sign Up</a></li>
                </div>
            @endif
        </div>
        <div class="mob-wrap">
            <div class="search">
                <form class="search-form" action="">
                    <input type="text" name="q" placeholder="Discover new heights with science ">
                    <input type="submit">
                    <div class="wrap-search">
                        <div class="item">
                            <div class="top">
                                <p>Search</p>
                                <div class="wrap-check">
                                    <span>publications</span>
                                    <label class="checkbox">
                                        <input type="checkbox" name="type">
                                    </label>
                                    <span>innovations</span>
                                </div>                          
                            </div>
                            <div class="tab-areas tab active" id="areas_of_interest">
                                @for ($i = 0; $i < $amount_categories; $i++)
                                    @if ($i == 0 || $i == ceil($amount_categories / 2))
                                        <div class="col">
                                    @endif
                                        <div class="form-group">
                                            <input type="radio" 
                                                id="category-m-{{ $categories[$i]->id }}" 
                                                name="category" value="{{ $categories[$i]->id }}">
                                            <label for="category-m-{{ $categories[$i]->id }}" class="checkbox">
                                                {{ $categories[$i]->title }}
                                                <i class="fas fa-check-circle"></i>
                                            </label>
                                        </div>
                                    @if ($i == (ceil($amount_categories / 2) - 1) || $i + 1 == $amount_categories)
                                        </div>
                                    @endif
                                @endfor
                            </div>
                        </div>
                    </div>                  
                </form>
            </div>  
        @include('inc.footer_mobile')              
        </div>
    </div>
</header>

@yield('content')

@if (Auth::user())
<div class="modal-wrapper" id="modal--pSettings">
    <div class="modal--pSettings modal">
        <div class="close"><img src="/img/close-modal.svg" alt=""></div>
        <div class="wrap">
            <h4>Settings</h4>
            <ul class="setting-linkTab">
                <li><a href="" class="active" data-tab="notification-tab">Notifications</a></li>
                <li><a href="" data-tab="profSet-privacy">Privacy</a></li>
            </ul>
            <?php
                $userSettings = Auth::user()->settings;
                $update_impact_areas = $userSettings->filter(function($item) {
                    return $item->name == 'update_impact_areas';
                })->first();
                $update_publications_interest = $userSettings->filter(function($item) {
                    return $item->name == 'update_publications_interest';
                })->first();
                $update_technologies_interest = $userSettings->filter(function($item) {
                    return $item->name == 'update_technologies_interest';
                })->first();
                $update_co_authors_joining = $userSettings->filter(function($item) {
                    return $item->name == 'update_co_authors_joining';
                })->first();
                $update_users_areas_impact = $userSettings->filter(function($item) {
                    return $item->name == 'update_users_areas_impact';
                })->first();
                $update_new_matching_jobs = $userSettings->filter(function($item) {
                    return $item->name == 'update_new_matching_jobs';
                })->first();
                $update_new_matching_events = $userSettings->filter(function($item) {
                    return $item->name == 'update_new_matching_events';
                })->first();
                $private_message = $userSettings->filter(function($item) {
                    return $item->name == 'private_message';
                })->first();
                $view_profile = $userSettings->filter(function($item) {
                    return $item->name == 'view_profile';
                })->first();
            ?>
            <form id="settings_form" class="setting-tab" action="/ajax/saveUserSettings"
                method="post">
                {{ csrf_field() }}
                <div class="notification-tab tab active" id="notification-tab">
                    <div class="form-group">
                        <input type="checkbox" id="setting-1" name="update_impact_areas"
                            @if ($update_impact_areas === NULL)
                                checked="" 
                            @elseif ($update_impact_areas->value === '1')
                                checked=""
                            @endif
                        >
                        <label for="setting-1" class="checkbox">
                            <span class="check"></span>
                            <span class="text">Updates on your impact areas</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" id="setting-2" name="update_publications_interest"
                            @if ($update_publications_interest === NULL)
                                checked="" 
                            @elseif ($update_publications_interest->value === '1')
                                checked=""
                            @endif
                        >
                        <label for="setting-2" class="checkbox">
                            <span class="check"></span>
                            <span class="text">Updates on publications in your interest</span>
                        </label>
                    </div>  
                    <div class="form-group">
                        <input type="checkbox" id="setting-3" name="update_technologies_interest"
                            @if ($update_technologies_interest === NULL)
                                checked="" 
                            @elseif ($update_technologies_interest->value === '1')
                                checked=""
                            @endif>
                        <label for="setting-3" class="checkbox">
                            <span class="check"></span>
                            <span class="text">Updates on technologies in your interest</span>
                        </label>
                    </div> 
                    <div class="form-group">
                        <input type="checkbox" id="setting-4"  name="update_co_authors_joining"
                            @if ($update_co_authors_joining === NULL)
                                checked="" 
                            @elseif ($update_co_authors_joining->value === '1')
                                checked=""
                            @endif>
                        <label for="setting-4" class="checkbox">
                            <span class="check"></span>
                            <span class="text">Updates on your co-authors joining the network</span>
                        </label>
                    </div> 
                    <div class="form-group">
                        <input type="checkbox" id="setting-5" name="update_users_areas_impact"
                            @if ($update_users_areas_impact === NULL)
                                checked="" 
                            @elseif ($update_users_areas_impact->value === '1')
                                checked=""
                            @endif>
                        <label for="setting-5" class="checkbox">
                            <span class="check"></span>
                            <span class="text">Updates on users in your areas of impact</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" id="setting-6" name="update_new_matching_jobs"
                            @if ($update_new_matching_jobs === NULL)
                                checked="" 
                            @elseif ($update_new_matching_jobs->value === '1')
                                checked=""
                            @endif>
                        <label for="setting-6" class="checkbox">
                            <span class="check"></span>
                            <span class="text">Updates on your new matching jobs</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" id="setting-7" name="update_new_matching_events"
                            @if ($update_new_matching_events === NULL)
                                checked="" 
                            @elseif ($update_new_matching_events->value === '1')
                                checked=""
                            @endif>
                        <label for="setting-7" class="checkbox">
                            <span class="check"></span>
                            <span class="text">Updates on your new matching events</span>
                        </label>
                    </div>              
                </div>
                <div class="profSet-privacy-tab tab" id="profSet-privacy">
                    <div class="form-group">
                        <p>Who can write you a privat message?</p>
                        <div class="col">
                            <input type="radio" id="radio-01" name="private_message"
                                @if ($private_message === NULL)
                                    checked="" 
                                @elseif ($private_message->value === '1')
                                    checked=""
                                @endif
                            >
                            <label for="radio-01" class="radio checkbox">
                                <span class="check"></span>
                                <span class="text">Anyone</span>
                            </label>                            
                        </div>
                        <div class="col">
                            <input type="radio" id="radio-02" name="private_message" 
                                @if ($private_message && $private_message->value === '0')
                                    checked=""
                                @endif>
                            <label for="radio-02" class="radio checkbox">
                                <span class="check"></span>
                                <span class="text">Nobody</span>
                            </label>                            
                        </div>                      
                    </div>
                    <div class="form-group">
                        <p>Who can view your profile?</p>
                        <div class="col">
                            <input type="radio" id="radio-11" name="view_profile"
                                @if ($view_profile === NULL)
                                    checked="" 
                                @elseif ($view_profile->value === '1')
                                    checked=""
                                @endif
                            >
                            <label for="radio-11" class="radio checkbox">
                                <span class="check"></span>
                                <span class="text">Anyone</span>
                            </label>                            
                        </div>
                        <div class="col">
                            <input type="radio" id="radio-12" name="view_profile"
                                @if ($view_profile && $view_profile->value === '0')
                                    checked=""
                                @endif
                            >
                            <label for="radio-12" class="radio checkbox">
                                <span class="check"></span>
                                <span class="text">Nobody</span>
                            </label>                            
                        </div>                      
                    </div>                  
                </div>
                <button class="update">Update</button>
            </form>
        </div>
    </div>
 </div>
@endif

<script>
    var cardId = @if(isset($card_id)) {{ $card_id }} @else 0 @endif;
    var paperPubmedId = @if(isset($paper_pubmed_id)) {{ $paper_pubmed_id }}; @else 0 @endif;
</script>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.js') }}"></script>
<script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
<!-- Utility Scripts -->
<script src="{{ asset('js/canvasjs.js') }}"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script> 
<script src="{{ asset('js/modernizr.min.js') }}"></script>
<script src="{{ asset('js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('js/jquery.nice-select.min.js') }}"></script>
<script src="{{ asset('js/wow.min.js') }}"></script>
<script src="{{ asset('js/jquery-migrate-1.2.1.min.js') }}"></script>
<script src="{{ asset('js/slick.min.js') }}"></script>
<script src="{{ asset('js/scripts.js') }}"></script>
@if (isset($scripts))
    @foreach($scripts as $script)
        <script src="{{ asset($script) }}"></script>
    @endforeach
@endif

@if (Route::currentRouteName() == 'home')
    <script>
    $(document).ready(function(){   
        
        var idV = $('#video-main').attr('id');
        var video = document.getElementById(idV);
        video.play();    
      
         
    });     
    </script>
@endif

@if (Auth::user() && (Route::currentRouteName() == 'index' || Route::currentRouteName() == 'profile') )
  <script>        
    $(function() {
        $('.range').slider({
            range: false,
            value: 1,
            min: 1,
            max: 5,
            create: function(event, ui) {
                var val = $(event.target).closest('.row').find('input').val();
                $(event.target).slider('value', val);  
            },
            slide: function(event, ui) {
                handle = ui.value;
                $(ui.handle).closest('.row').find('input').val(ui.value);
            }
        });   
    });      
  </script>
@endif

@if (!Auth::user())
    @include('inc.popups.auth')
    <script>
        $(document).ready(function(){
            $('.category-interest').each(function() {
                $(this).prop('checked', false);
            });

            $('.category-interest').change(function() {
                var interestSelected = false;
                $('.category-interest').each(function() {
                    if ($(this).is(':checked')) {
                        interestSelected = true;
                        $('.next.to-step2').removeAttr('style');
                        return;
                    }
                });
                if (!interestSelected) {
                    $('.next.to-step2').css('background-color', '#ccc');
                }
            });

            $('.to-step1').click(function(e){
                e.preventDefault();
                $(this).closest('.step-s').fadeOut(0);
                $('.step1-sign').fadeIn(0);
            }); 

            $('.to-step2').click(function(e){
                e.preventDefault();
                var interestSelected = false;
                var categoryInterest = [];
                $('.category-interest').each(function() {
                    if ($(this).is(':checked')) {
                        interestSelected = true;
                        categoryInterest.push($(this).attr('id').replace('category-', ''));
                    }
                });
                if (interestSelected) {
                    $(this).closest('.step-s').fadeOut(0);
                    $('.step2-sign').fadeIn(0);
                }
                if (categoryInterest.length) {
                    localStorage.setItem('categoryInterest', JSON.stringify(categoryInterest));
                }
            });


            //add listeners for social buttons
            /*$('#facebook_login').click(function(e){
                var self = $(this);
                FB.getLoginStatus(function(response) {
                    if(response.status === 'connected') {
                        facebookAuth(response, self);
                    } else {
                        FB.login(function(response) {
                            facebookAuth(response, self);
                            return response
                        }, {
                            scope: 'public_profile,email'
                        });
                    }
                });
            });*/

            @if (isset($orcid_auth) && $orcid_auth)
                @if ($orcid_error)
                    alert('{{$orcid_error}}');
                @else
                    user.id = '{{ $orcid_data['orcid'] }}';
                    user.params = {
                        social : 'orcid',
                        access_token : '{{ $orcid_data['access_token'] }}'
                    };
                    nextSignStep($('#orcid_login'));
                @endif
            @endif

            $('#linkedin_login').click(function(e){
                var self = $(this);
                IN.User.authorize(function() {
                    user.id = IN.ENV.auth.member_id;
                    user.params = {
                        social : 'linkedin',
                        access_token : IN.ENV.auth.oauth_token
                    };
                    nextSignStep(self)
                });
            });

            $('.btn-sign-up.register').click(function(e) {
                e.preventDefault();
                var self = $(this);
                var form = $('.email-form-sign.register');
                var fullName = form.find('input[name="full_name"]').val();
                var email = form.find('input[name="email"]').val();
                var password = form.find('input[name="password"]').val();
                var self = $(this);
                sendAjaxOnButtonClick(
                    self, 
                    '/registerEmail',
                    'post',
                    {
                        full_name: fullName,
                        email: email,
                        password: password
                    },
                    function(reponse) {
                        self.closest('.modal-wrapper').fadeOut();
                        $('#modal-stepSingup').fadeIn();
                        $('body').css('overflow','hidden');
                        $('.step1-sign').hide();
                        $('.step2-sign').show();
                        localStorage.setItem('authType', 'email');
                    },
                    function() {

                    }
                );
            });

            $('.btn-sign-up.login').click(function(e) {
                e.preventDefault();
                var self = $(this);
                var form = $('.email-form-sign.login');
                var email = form.find('input[name="email"]').val();
                var password = form.find('input[name="password"]').val();
                var remember = form.find('input[name="remember"]').val();
                var self = $(this);
                sendAjaxOnButtonClick(
                    self, 
                    '/loginEmail',
                    'post',
                    {
                        email: email,
                        password: password,
                        remember: remember
                    },
                    function(reponse) {
                        window.location.href = '/feed';
                    },
                    function() {

                    }
                );
            });

            (function() {
                gapi.load('auth2', function(){
                    var self = $('#google_login');
                    auth2 = gapi.auth2.init({
                        client_id: '813003395344-ajvprmjanibdogrdk8gt87j05pptqmlt.apps.googleusercontent.com',
                        cookiepolicy: 'single_host_origin'
                    });
                    auth2.attachClickHandler(document.getElementById('google_login'), {},
                        function(googleUser) {
                            user.id = googleUser.getId();
                            user.params = {
                                social : 'google',
                                access_token : googleUser.getAuthResponse().id_token
                            };
                            nextSignStep(self);
                        }, function(error) {
                    });
                });
            })();

            //load papers
            $('.step2-sign input[name="query"]').keyup(function(){
                var q = $(this).val();
                if (q.length >= 3) {
                    getPaperForSignStep(q, 0);
                } else {
                    $('.step2-sign .tab-papers').remove();
                    $('.step2-sign .papers-alert').show();
                    signPapers = [];
                }
            });
            $('.step2-sign').on('click', '.paginattion-new a', function(e){
                e.preventDefault();
                var offset = $(this).data('offset');
                var q = $('.step2-sign input[name="query"]').val();
                getPaperForSignStep(q, offset);
            });

            //save selected papers
            $('#modal-stepSingup').on('click', '.sign-papers', function() {
                var self = $(this);
                var id = self.attr('id').replace('title-', '');
                if (self.prop('checked')) {
                    //save item
                    signPapers.push(id);
                } else {
                    //remove item
                    for (var i = signPapers.length - 1; i >= 0; i--) {
                        if (signPapers[i] == id) {
                            signPapers.splice(i, 1);
                        }
                    }
                }
            });


            //sign done
            $('.next.done').click(function(e){
                e.preventDefault();
                if (!$(this).is("[disabled]")) {
                    var authType = localStorage.getItem('authType');
                    var self = $(this);
                    var data = {
                        papers: signPapers 
                    };
                    if (authType == 'social') {
                        data.user = user;
                        sendAjaxOnButtonClick(
                            self, 
                            '/register',
                            'post',
                            data,
                            function(reponse) {
                                window.location.href = '/feed';
                            },
                            function() {

                            }
                        );
                    } else {
                        sendAjaxOnButtonClick(
                            self, 
                            '/ajax/addUserPapers?noflash=true',
                            'post',
                            data,
                            function(reponse) {
                                window.location.href = '/feed';
                            },
                            function() {

                            }
                        );
                    }
                }
            });
        });

        /**
         * Save facebook data and do step
         * @param  {object} response
         * @param  {object} ths
         * @return {void}
         */
        /*function facebookAuth(response, ths) {
            if(response.status === 'connected') {
                user.id = response.authResponse.userID;
                user.params = {
                    social : 'facebook',
                    access_token : response.authResponse.accessToken
                };
                nextSignStep(ths);
            }
        }*/

        /**
         * Do step after chose social network
         * @param  {object} ths 
         * @return {void}
         */
        function nextSignStep(ths) {

            $.ajax({
                url: '/login',
                type:'post',
                data: {
                    user: user,
                    _token: csrf_token
                },
                success: function(response) {
                    if (!response.error) {
                        if (response.user_logged) {
                            //do sign up
                            window.location.href = '/feed';
                        } else {
                            // do sign in
                            var categoryInterest = 
                                JSON.parse(localStorage.getItem('categoryInterest'));
                            /*if (!categoryInterest) {

                                //load first step
                                ths.closest('.modal-wrapper').fadeOut();
                                $('#modal-stepSingup').fadeIn();
                                $('body').css('overflow','hidden');

                            } else if(categoryInterest) {*/

                                //load second step
                                ths.closest('.modal-wrapper').fadeOut();
                                $('#modal-stepSingup').fadeIn();
                                $('body').css('overflow','hidden');
                                $('.step1-sign').hide();
                                $('.step2-sign').show();

                                //set interests in first step
                                /*for (var i = categoryInterest.length - 1; i >= 0; i--) {
                                    $('#category-' + categoryInterest[i]).prop('checked', true);
                                }*/

                                //set default color button in first step
                                $('#modal-stepSingup .to-step2').removeAttr('style');
                                localStorage.setItem('authType', 'social');
                            //}
                        }
                    } else {
                        alert(reponse.error);
                    }
                },
                error: function() {
                    alert('Please, try again, after page reload.');
                }
            });
        }
    </script>
@endif
@if(Auth::user() && Route::currentRouteName() == 'index')
    <script src="{{ asset('packages/product-tour/js/product-tour.js') }}"></script>
    <script>
        var tourInited = false;
        function initTour() {
            if (!tourInited) {
                $('.cd-tour-wrapper').remove();
                $('.overlay-tour').remove();
                tourInited = true;
                //initialize constructor
                var productTour = new ProductTour({
                    overlay:true
                });
                //can only be called once
                productTour.steps([
                {//pass an array of tour steps
                    element: '#upload_publication',
                    title: 'Upload your publications',
                    content: 'This  is  important.  It  enables '+ 
                    'SciPrism    to  understand  and ' + 
                    'predict impact  areas   for your ' +    
                    'publications.',
                    position: 'right'
                },
                {
                    element: '.wr-chart:first',
                    title: 'Discover impact of publications',
                    content: 'Every publication  impacts several ' +
                    'areas   of  science.    Click   on  diagram ' + 
                    'to  see a   more    detailed    impact ' +
                    'analysis.',
                    position: 'bottom'
                },
                {
                    element: '.side--impact',
                    title: 'Discover your personal impact',
                    content: 'This  is  the impact  of  all ' +
                    'your publications. Click to discover a ' +
                    'more detailed analysis in your ' +
                    'profile.',
                    position: 'right'
                },
                {
                    element: '.top--fead .nav-tabs',
                    title: 'View publications & innovations',
                    content: 'Toggle to view publications and ' +
                    'innovations in your field. Your ' +   
                    'publications will appear on the top ' +
                    'of the feed. Scroll down to discover ' +    
                    'more.',
                    position: 'bottom'
                },
                {
                    element: '.search:first',
                    title: 'Explore your interests',
                    content: 'Discover  more    publications and ' +
                    'innovations  in  different   areas.',
                    position: 'bottom'
                },
                {
                    element: '.career--fit',
                    title: 'Discover your career fit',
                    content: 'Complete the survey to find out ' +
                    'which career options are best for ' +
                    'you.',
                    position: 'left'
                },
                {
                    element: '.opportunity',
                    title: 'Discover jobs and events',
                    content: 'Explore opportunities that fit your ' + 
                    'career  and networking  interests.',
                    position: 'left'
                }
                ]);

                productTour.startTour();//initialize the tour

                tourInited = true;
            }
        }
        @if (session('welcome_message') || $show_demo)
            $('body').on('feed_loaded', function(){
                setTimeout(initTour, 1000);
            });
        @endif
        $('#demo').click(function(e){
            e.preventDefault();
            /*if ($('.pin').length) {
                initTour();
            }*/
            $('.site-header').css({ 'position' : 'relative', 'z-index' : 'auto' });
            $('body').css({ 'padding-top' : '0' });
            tourInited = false;
            initTour();
        });
        $('body').on('tourColosed', function(){
            $('.site-header').css({ 'position' : 'fixed', 'z-index' : '999' });
            $('body').css({ 'padding-top' : '56px' });    
        });
    </script>
@endif
 
@if (isset($impact_areas) && !$impact_areas)
    <script>
    $(function() {
      $("#chart-1").drawDoughnutChart([
        { title: "Tokyo", value: 100, color: "#2C3E50" },
        { title: "San Francisco", value: 80, color: "#FC4349" },
        { title: "New York", value: 70, color: "#6DBCDB" },
        { title: "London", value: 50, color: "#F7E248" },
        { title: "Sydney", value: 50, color: "#D7DADB" },
        { title: "Berlin", value: 30, color: "#FFF" }
      ]);
        
        setTimeout(function() {
            $('.chart').children().fadeOut();
            
           $("#chart-1").drawDoughnutChart([
            { title: "Tokyo", value: 50, color: "#2C3E50" },
            { title: "San Francisco", value: 120, color: "#FC4349" },
            { title: "New York", value: 70, color: "#6DBCDB" },
            { title: "London", value: 50, color: "#aaa" },
            { title: "Sydney", value: 60, color: "#D7DADB" },
            { title: "Berlin", value: 30, color: "#FFF" }
          ]);
        }, 2500);
        setTimeout(function() {
            $('.chart').children().fadeOut();
            
           $("#chart-1").drawDoughnutChart([
            { title: "Tokyo", value: 120, color: "#20e220" },
            { title: "San Francisco", value: 60, color: "#FC4349" },
            { title: "New York", value: 70, color: "#6DBCDB" },
            { title: "London", value: 50, color: "#F7E248" },
            { title: "Sydney", value: 60, color: "#D7DADB" },
            { title: "Berlin", value: 30, color: "#1c59ff" }
          ]);
        }, 5000);    
        setTimeout(function() {
            $('.chart').children().fadeOut();
            
           $("#chart-1").drawDoughnutChart([
            { title: "Tokyo", value: 30, color: "#488348" },
            { title: "San Francisco", value: 60, color: "#FC4349" },
            { title: "New York", value: 70, color: "#ffb101" },
            { title: "London", value: 50, color: "#F7E248" },
            { title: "Sydney", value: 60, color: "#d422ff" },
            { title: "Berlin", value: 70, color: "#1c59ff" }
          ]);
        }, 7500);    
        setTimeout(function() {
            $('.chart').children().fadeOut();
            
           $("#chart-1").drawDoughnutChart([
            { title: "Tokyo", value: 40, color: "#ff11a3" },
            { title: "San Francisco", value: 30, color: "#FC4349" },
            { title: "New York", value: 70, color: "#ffb101" },
            { title: "London", value: 140, color: "#F7E248" },
            { title: "Sydney", value: 60, color: "#d422ff" },
            { title: "Berlin", value: 20, color: "#7110e2" }
          ]);
        }, 10000);   
        setTimeout(function() {
            $('.chart').children().fadeOut();
            
           $("#chart-1").drawDoughnutChart([
            { title: "Tokyo", value: 40, color: "#ff11a3" },
            { title: "San Francisco", value: 30, color: "#FC4349" },
            { title: "New York", value: 70, color: "#ffb101" },
            { title: "London", value: 140, color: "#44e5d2" },
            { title: "Sydney", value: 60, color: "#d422ff" },
            { title: "Berlin", value: 120, color: "#10e218" }
          ]);
        }, 12500);     
    });
     
    (function($, undefined) {
      $.fn.drawDoughnutChart = function(data, options) {
        var $this = this,
          W = $this.width(),
          H = $this.height(),
          centerX = W / 2,
          centerY = H / 2,
          cos = Math.cos,
          sin = Math.sin,
          PI = Math.PI,
          settings = $.extend(
            {
              segmentShowStroke: true,
              segmentStrokeColor: "#0C1013",
              segmentStrokeWidth: 1,
              baseColor: "rgba(0,0,0,0.5)",
              baseOffset: 3,
              edgeOffset: 15, //offset from edge of $this
              percentageInnerCutout: 75,
              animation: true,
              animationSteps: 100,
              animationEasing: "easeInOutExpo",
              animateRotate: true,
              tipOffsetX: -8,
              tipOffsetY: -45,
              tipClass: "doughnutTip",
              summaryClass: "doughnutSummary",
              summaryTitle: "TOTAL:",
              summaryTitleClass: "doughnutSummaryTitle",
              summaryNumberClass: "doughnutSummaryNumber",
              beforeDraw: function() {},
              afterDrawed: function() {},
              onPathEnter: function(e, data) {},
              onPathLeave: function(e, data) {}
            },
            options
          ),
          animationOptions = {
            linear: function(t) {
              return t;
            },
            easeInOutExpo: function(t) {
              var v = t < 0.5 ? 8 * t * t * t * t : 1 - 8 * --t * t * t * t;
              return v > 1 ? 1 : v;
            }
          },
          requestAnimFrame = (function() {
            return (
              window.requestAnimationFrame ||
              window.webkitRequestAnimationFrame ||
              window.mozRequestAnimationFrame ||
              window.oRequestAnimationFrame ||
              window.msRequestAnimationFrame ||
              function(callback) {
                window.setTimeout(callback, 1000 );
              }
            );
          })();

        settings.beforeDraw.call($this);

        var $svg = $(
            '<svg width="' +
              W +
              '" height="' +
              H +
              '" viewBox="0 0 ' +
              W +
              " " +
              H +
              '" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"></svg>'
          ).appendTo($this),
          $paths = [],
          easingFunction = animationOptions[settings.animationEasing],
          doughnutRadius = Min([H / 2, W / 2]) - settings.edgeOffset,
          cutoutRadius = doughnutRadius * (settings.percentageInnerCutout / 100),
          segmentTotal = 0;

        //Draw base doughnut
        var baseDoughnutRadius = doughnutRadius + settings.baseOffset,
          baseCutoutRadius = cutoutRadius - settings.baseOffset;
        $(document.createElementNS("http://www.w3.org/2000/svg", "path"))
          .attr({
            d: getHollowCirclePath(baseDoughnutRadius, baseCutoutRadius),
            fill: settings.baseColor
          })
          .appendTo($svg);

        //Set up pie segments wrapper
        var $pathGroup = $(
          document.createElementNS("http://www.w3.org/2000/svg", "g")
        );
        $pathGroup.attr({ opacity: 0 }).appendTo($svg);

        //Set up tooltip
        var $tip = $('<div class="' + settings.tipClass + '" />')
            .appendTo("body")
            .hide(),
          tipW = $tip.width(),
          tipH = $tip.height();

        //Set up center text area
        var summarySize = (cutoutRadius - (doughnutRadius - cutoutRadius)) * 2,
          $summary = $('<div class="' + settings.summaryClass + '" />')
            .appendTo($this)
            .css({
              width: summarySize + "px",
              height: summarySize + "px",
              "margin-left": -(summarySize / 2) + "px",
              "margin-top": -(summarySize / 2) + "px"
            });
        var $summaryTitle = $(
          '<p class="' +
            settings.summaryTitleClass +
            '">' +
            settings.summaryTitle +
            "</p>"
        ).appendTo($summary);
        var $summaryNumber = $(
          '<p class="' + settings.summaryNumberClass + '"></p>'
        )
          .appendTo($summary)
          .css({ opacity: 0 });

        for (var i = 0, len = data.length; i < len; i++) {
          segmentTotal += data[i].value;
          $paths[i] = $(
            document.createElementNS("http://www.w3.org/2000/svg", "path")
          )
            .attr({
              "stroke-width": settings.segmentStrokeWidth,
              stroke: settings.segmentStrokeColor,
              fill: data[i].color,
              "data-order": i
            })
            .appendTo($pathGroup)
            .on("mouseenter", pathMouseEnter)
            .on("mouseleave", pathMouseLeave)
            .on("mousemove", pathMouseMove);
        }

        //Animation start
        animationLoop(drawPieSegments);

        //Functions
        function getHollowCirclePath(doughnutRadius, cutoutRadius) {
          //Calculate values for the path.
          //We needn't calculate startRadius, segmentAngle and endRadius, because base doughnut doesn't animate.
          var startRadius = -1.57, // -Math.PI/2
            segmentAngle = 6.2831, // 1 * ((99.9999/100) * (PI*2)),
            endRadius = 4.7131, // startRadius + segmentAngle
            startX = centerX + cos(startRadius) * doughnutRadius,
            startY = centerY + sin(startRadius) * doughnutRadius,
            endX2 = centerX + cos(startRadius) * cutoutRadius,
            endY2 = centerY + sin(startRadius) * cutoutRadius,
            endX = centerX + cos(endRadius) * doughnutRadius,
            endY = centerY + sin(endRadius) * doughnutRadius,
            startX2 = centerX + cos(endRadius) * cutoutRadius,
            startY2 = centerY + sin(endRadius) * cutoutRadius;
          var cmd = [
            "M",
            startX,
            startY,
            "A",
            doughnutRadius,
            doughnutRadius,
            0,
            1,
            1,
            endX,
            endY, //Draw outer circle
            "Z", //Close path
            "M",
            startX2,
            startY2, //Move pointer
            "A",
            cutoutRadius,
            cutoutRadius,
            0,
            1,
            0,
            endX2,
            endY2, //Draw inner circle
            "Z"
          ];
          cmd = cmd.join(" ");
          return cmd;
        }
        function pathMouseEnter(e) {
          var order = $(this).data().order;
          $tip.text(data[order].title + ": " + data[order].value).fadeIn(200);
          settings.onPathEnter.apply($(this), [e, data]);
        }
        function pathMouseLeave(e) {
          $tip.hide();
          settings.onPathLeave.apply($(this), [e, data]);
        }
        function pathMouseMove(e) {
          $tip.css({
            top: e.pageY + settings.tipOffsetY,
            left: e.pageX - $tip.width() / 2 + settings.tipOffsetX
          });
        }
        function drawPieSegments(animationDecimal) {
          var startRadius = -PI / 2, //-90 degree
            rotateAnimation = 1;
          if (settings.animation && settings.animateRotate)
            rotateAnimation = animationDecimal; //count up between0~1

          drawDoughnutText(animationDecimal, segmentTotal);

          $pathGroup.attr("opacity", animationDecimal);

          //If data have only one value, we draw hollow circle(#1).
          if (
            data.length === 1 &&
            4.7122 <
              rotateAnimation * (data[0].value / segmentTotal * (PI * 2)) +
                startRadius
          ) {
            $paths[0].attr("d", getHollowCirclePath(doughnutRadius, cutoutRadius));
            return;
          }
          for (var i = 0, len = data.length; i < len; i++) {
            var segmentAngle =
                rotateAnimation * (data[i].value / segmentTotal * (PI * 2)),
              endRadius = startRadius + segmentAngle,
              largeArc = (endRadius - startRadius) % (PI * 2) > PI ? 1 : 0,
              startX = centerX + cos(startRadius) * doughnutRadius,
              startY = centerY + sin(startRadius) * doughnutRadius,
              endX2 = centerX + cos(startRadius) * cutoutRadius,
              endY2 = centerY + sin(startRadius) * cutoutRadius,
              endX = centerX + cos(endRadius) * doughnutRadius,
              endY = centerY + sin(endRadius) * doughnutRadius,
              startX2 = centerX + cos(endRadius) * cutoutRadius,
              startY2 = centerY + sin(endRadius) * cutoutRadius;
            var cmd = [
              "M",
              startX,
              startY, //Move pointer
              "A",
              doughnutRadius,
              doughnutRadius,
              0,
              largeArc,
              1,
              endX,
              endY, //Draw outer arc path
              "L",
              startX2,
              startY2, //Draw line path(this line connects outer and innner arc paths)
              "A",
              cutoutRadius,
              cutoutRadius,
              0,
              largeArc,
              0,
              endX2,
              endY2, //Draw inner arc path
              "Z" //Cloth path
            ];
            $paths[i].attr("d", cmd.join(" "));
            startRadius += segmentAngle;
          }
        }
        function drawDoughnutText(animationDecimal, segmentTotal) {
          $summaryNumber
            .css({ opacity: animationDecimal })
            .text((segmentTotal * animationDecimal).toFixed(1));
        }
        function animateFrame(cnt, drawData) {
          var easeAdjustedAnimationPercent = settings.animation
            ? CapValue(easingFunction(cnt), null, 0)
            : 1;
          drawData(easeAdjustedAnimationPercent);
        }
        function animationLoop(drawData) {
          var animFrameAmount = settings.animation
              ? 1 / CapValue(settings.animationSteps, Number.MAX_VALUE, 1)
              : 1,
            cnt = settings.animation ? 0 : 1;
          requestAnimFrame(function() {
            cnt += animFrameAmount;
            animateFrame(cnt, drawData);
            if (cnt <= 1) {
              requestAnimFrame(arguments.callee);
            } else {
              settings.afterDrawed.call($this);
            }
          });
        }
        function Max(arr) {
          return Math.max.apply(null, arr);
        }
        function Min(arr) {
          return Math.min.apply(null, arr);
        }
        function isNumber(n) {
          return !isNaN(parseFloat(n)) && isFinite(n);
        }
        function CapValue(valueToCap, maxValue, minValue) {
          if (isNumber(maxValue) && valueToCap > maxValue) return maxValue;
          if (isNumber(minValue) && valueToCap < minValue) return minValue;
          return valueToCap;
        }
        return $this;
      };
    })(jQuery);
        
    </script>
@endif
<!-- endbuild -->
</body>
</html>
