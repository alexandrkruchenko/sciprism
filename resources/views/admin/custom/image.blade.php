<div class="form-group form-element-image">
    <label for="image" class="control-label">Image</label>
    <div>
        <div class="form-element-files clearfix" 
            @if(!isset($model->image) || !$model->image) style="display: none;" @endif
        >
            <div class="form-element-files__item">
                <a href="@if(isset($model->image)) {{ url($model->image) }} @endif" data-toggle="lightbox" class="form-element-files__image">
                    <img src="@if(isset($model->image)) {{ url($model->image) }} @endif">
                </a>
                <div class="form-element-files__info">
                    <a href="@if(isset($model->image)) {{ url($model->image) }} @endif" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>
                    <button type="button" class="btn btn-danger btn-xs">
                        <i class="fa fa-times"></i> Remove Image
                    </button>
                </div>
            </div>
        </div>
        <div>
            <div class="btn btn-primary upload-button dz-clickable">
                <i class="fa fa-upload"></i> Select Image
            </div>
        </div>
        <input name="image" value="@if(isset($model->image)) {{ url($model->image) }} @endif" type="hidden">
    </div>
</div>
<div id="elfinder"></div>
