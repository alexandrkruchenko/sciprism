<?php foreach($charts as $key => $chart): ?>
<div class="col-xs-6">
    <h4><?php echo $chart['title']; ?></h2>
    <ul class="nav nav-tabs" role="tablist">
         <li role="presentation" class="active">
            <a href="#<?php echo 'tab'.$key.'_per_day'; ?>"
                aria-controls="<?php echo 'tab'.$key.'_per_day'; ?>"
                    role="tab" data-toggle="tab">Day</a>
        </li>
        <li role="presentation">
            <a href="#<?php echo 'tab'.$key.'_per_week'; ?>"
                aria-controls="<?php echo 'tab'.$key.'_per_week'; ?>"
                    role="tab" data-toggle="tab">Week</a>
        </li>
        <li role="presentation">
            <a href="#<?php echo 'tab'.$key.'_per_month'; ?>"
                aria-controls="<?php echo 'tab'.$key.'_per_month'; ?>"
                    role="tab" data-toggle="tab">Month</a>
        </li>
        <li role="presentation">
            <a href="#<?php echo 'tab'.$key.'_per_year'; ?>"
                aria-controls="<?php echo 'tab'.$key.'_per_year'; ?>"
                    role="tab" data-toggle="tab">Year</a>
        </li>
    </ul>
    
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="<?php echo 'tab'.$key.'_per_day'; ?>">
                <div id="<?php echo $chart['table'].$key.$chart['periods'][0]; ?>"></div>
                <?php echo $lava->render(
                        'LineChart', 
                        $chart['table'].$key.$chart['periods'][0], 
                        $chart['table'].$key.$chart['periods'][0]
                    ); 
                ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="<?php echo 'tab'.$key.'_per_week'; ?>">
                <div id="<?php echo $chart['table'].$key.$chart['periods'][1]; ?>"></div>
                <?php echo $lava->render(
                        'LineChart', 
                        $chart['table'].$key.$chart['periods'][1], 
                        $chart['table'].$key.$chart['periods'][1]
                    ); 
                ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="<?php echo 'tab'.$key.'_per_month'; ?>">
                <div id="<?php echo $chart['table'].$key.$chart['periods'][2]; ?>"></div>
                <?php echo $lava->render(
                        'LineChart', 
                        $chart['table'].$key.$chart['periods'][2], 
                        $chart['table'].$key.$chart['periods'][2]
                    );
                ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="<?php echo 'tab'.$key.'_per_year'; ?>">
                <div id="<?php echo $chart['table'].$key.$chart['periods'][3]; ?>"></div>
                <?php echo $lava->render(
                        'LineChart', 
                        $chart['table'].$key.$chart['periods'][3], 
                        $chart['table'].$key.$chart['periods'][3]
                    );
                ?>
        </div>
    </div>
</div>
<?php endforeach; ?>