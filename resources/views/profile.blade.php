@extends('layouts.scistart')

@section('content')

<script>
window.onload = function () {
    @if ($impact_areas)
    CanvasJS.addColorSet("custom", [
        @foreach($impact_areas as $area)
            "{{ $area['color'] }}",
        @endforeach               
    ]);
    @endif
    var chart = new CanvasJS.Chart("chartContainer", {
        colorSet: @if ($impact_areas) "custom" @else null @endif,
        animationEnabled: true,
        data: [{
            type: "doughnut",
            startAngle: 60,
            //innerRadius: 60,
            indexLabelFontSize: 17,
            indexLabel: "{}",
            toolTipContent: "<b>{label}</b>",
            dataPoints: [
                @if ($impact_areas)
                    @foreach($impact_areas as $area)
                        { y: {{ $area['percent'] }}, label: "{{ $area['title'] }}" },
                    @endforeach
                @else
                    { y: 25, label: "Impact area 1" },
                    { y: 15, label: "Impact area 2" },
                    { y: 10, label: "Impact area 3" },
                    { y: 20, label: "Impact area 4" },
                    { y: 17, label: "Impact area 5" },
                    { y: 13, label: "Impact area 6" }
                @endif
            ]
        }]
    });
    chart.render();

    var chart1 = new CanvasJS.Chart("chartContainer1", {
        animationEnabled: true,
        colorSet: @if ($impact_areas) "custom" @else null @endif,
        data: [{
            type: "doughnut",
            startAngle: 60,
            //innerRadius: 60,
            indexLabelFontSize: 17,
            indexLabel: "{}",
            toolTipContent: "<b>{label}</b>",
            dataPoints: [
                @if ($impact_areas)
                    @foreach($impact_areas as $area)
                        { y: {{ $area['percent'] }}, label: "{{ $area['title'] }}" },
                    @endforeach
                @else
                    { y: 25, label: "Impact area 1" },
                    { y: 15, label: "Impact area 2" },
                    { y: 10, label: "Impact area 3" },
                    { y: 20, label: "Impact area 4" },
                    { y: 17, label: "Impact area 5" },
                    { y: 13, label: "Impact area 6" }
                @endif
            ]
        }]
    });
    chart1.render(); 
}
</script>

<div class="profile-page">
    <div class="container">
        @if (session('success'))
            <div class="prompt blue">
                <div class="prompt-close"><img src="/img/close-blue.svg" alt=""></div>
                <p>{!! session('success') !!}</p>
            </div>
        @endif
        @if ($errors->any())
            <div class="prompt red">
                <div class="prompt-close"><img src="/img/warrning-close.svg" alt=""></div>
                    <b>Hmm!</b>
                    @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
            </div>
        @endif

         <div class="profile--info">
            <div class="photo">
                <div class="wrap">
                    <img src="{{ $user->avatar }}" alt="">
                </div>
            </div>
            <div class="profile-text">
                @if ($is_owner)
                    <a href="#" class="edit-profile" data-toggle="modal-profEdit">Edit Profile</a>
                @endif
                <h4>{{ $user->name }} {{ $user->last_name }}</h4>
                <h5>{{ $user->title }}</h5>
                <p>{{ $user->summary }}</p>
            </div>
        </div>

        <div class="new-profile-items">
            <div class="item np-impact">
                <div class="title">Impact</div>
                <div class="side--impact">
                    <div class="wr-chart-side">
                        <div class="side-chart" id="chartContainer" style="height: 150px; width: 100%;"></div>
                    </div>   
                    @if ($impact_areas)
                        <ul class="percent-ul">
                            @foreach($impact_areas as $key => $area)
                                <li class="percent{{ $key + 1 }}">
                                    <span class="percent" style="background: {{ $area['color'] }}">{{ $area['percent'] }}%</span>
                                </li>
                            @endforeach                                             
                        </ul>
                    @endif
                </div>              
                <a href="#your_impact" class="scroll explore">
                    @if ($impact_areas)
                        Explore
                    @else
                        Complete
                    @endif
                </a>
            </div>
            <div class="item np-publick">
                <div class="title">Publications</div>
                <ul>
                    <li><b>{{ $count_papers }}</b> publications</li>
                    <li><b>{{ $pins_count }}</b> pins</li>
                </ul>
                <a href="#np-publick" class="scroll explore">
                    @if ($count_papers)
                        Explore
                    @else
                        Complete
                    @endif
                </a>                
            </div>
            <div class="item">
                <div class="title">Career fit</div>
                <div class="career--fit">
                     <ul>
                        <?php 
                            $bySkills = $user->getUserProfessionBy(1);
                            $countSkills = count($bySkills);
                        ?>
                         <li @if ($countSkills) class="active" @endif>
                            <a>
                                 <span>By skills</span>
                                 <div class="check"></div>
                             </a>
                         </li>
                         <?php 
                            $byInterests = $user->getUserProfessionBy(2);
                            $countInterests = count($byInterests);
                         ?>
                         <li @if ($countInterests) class="active" @endif>
                            <a>
                                 <span>By interests</span>
                                 <div class="check"></div>
                             </a>
                         </li>
                         <?php 
                            $byValues = $user->getUserProfessionBy(3);
                            $countValues = count($byValues);
                          ?>
                         <li @if ($countValues) class="active" @endif>
                            <a>
                                 <span>By Values</span>
                                 <div class="check"></div>
                             </a>
                         </li>                                               
                     </ul>
                </div> 
                <a href="#np-career" class="scroll explore">
                    @if ($countSkills || $countSkills || $countValues)
                        Explore
                    @else
                        Complete
                    @endif
                </a>
            </div>
            <div class="item">
                <div class="title">Opportunities</div>
                <div class="opportunity">
                    <?php
                        $eventsCount = 0;
                        $jobsCount = 0;
                        if ($user->expertises()->count()) {
                            $events = App\Services\EventbriteService::getEvents();
                            $eventsCount = $events->pagination->object_count; 
                            $jobs = App\Services\LinkUpService::getJobs();
                            $jobsCount = $jobs->result_info->total_jobs;
                        }
                    ?>
                    <ul>
                        <li>
                            <div class="icon">
                                <img src="/img/jobs.png" alt="">
                            </div>
                            <div class="count">{{ $jobsCount }}</div>
                            <span>Jobs</span>
                        </li>
                        <li>
                            <div class="icon">
                                <img src="/img/calendar.png" alt="">
                            </div>
                            <div class="count">{{ $eventsCount }}</div>
                            <span>Events</span>
                        </li>                   
                    </ul>
                </div>              
                <a href="#np-opportunities" class="scroll explore">
                    @if ($eventsCount || $jobsCount)
                        Explore
                    @else
                        Complete
                    @endif
                </a>
            </div>
        </div>
        
        <div class="row-impact" id="your_impact">
            <div class="side--impact">
                <div class="wr-chart-side">
                    <div class="title">Your impact</div>
                    <div class="side-chart" id="chartContainer1" style="height: 150px; width: 100%;"></div>
                </div>
                <ul class="percent-ul">
                @if ($impact_areas)
                    @foreach($impact_areas as $key => $area)
                        <li class="percent{{ $key + 1 }}">
                            <span class="percent" style="background: {{ $area['color'] }}">
                                {{ $area['percent'] }}%</span>
                            <span class="text">{{ $area['title'] }}</span>
                        </li>
                    @endforeach                                               
                @else
                    <a href="" class="btn add-btn empty" data-toggle="pinned-wrapper">
                        <span>Add Publications to find your impact.</span>
                    </a>
                @endif
                </ul>
            </div>              
        </div>

        <div class="your-publications" id="np-publick">
            <a href="" class="btn add-btn" data-toggle="manage-publication">
                <span>Add Publications</span>
            </a>           
            <h4> Your publications ( total {{ $count_papers }} )</h4>
            <ul class="list">
                @if ($pins_count)
                <li class="total">
                    <div class="text">
                    </div>
                    <a class="pins-count" data-paper-id="0">
                        {{ $pins_count }} Pins
                    </a>
                </li>
                @endif                              
            </ul>
        </div>
        
        <div class="analysis-c career--fit" id="np-career">
            <h4>Career  fit</h4>
            <table>
                <?php 
                    $by_skills = $user->getUserProfessionBy(1);
                    $countSkills = count($by_skills);
                ?>
                <tr>
                    <td>
                        <a href="" class="title-set" data-toggle="modal-careerFit" 
                            data-target="skills-tab">
                            <span>By Skills</span>
                            <img src="/img/settings.svg" alt="">
                        </a>
                    </td>
                    @if ($countSkills)
                    <td>
                        <ul>
                            <li>
                                <div class="best">Best</div>
                                <p>{{ $by_skills[0]->name }}</p>
                            </li>
                            <li>
                                <div class="Better">Better</div>
                                <p>{{ $by_skills[1]->name }}</p>
                            </li>
                            <li>
                                <div class="Good">Good</div>
                                <p>{{ $by_skills[2]->name }}</p>
                            </li>
                        </ul>
                    </td>
                    @else
                    <td>
                        <p>Complete the survey to find your career fit by skills</p>
                    </td>
                    <td>
                        <a href="#" class="survey"  
                            data-toggle="modal-careerFit" data-target="skills-tab">Survey</a>
                    </td>
                    @endif
                </tr>
                <?php 
                    $by_interests = $user->getUserProfessionBy(2);
                    $countInterests = count($by_interests);
                ?>
                <tr>
                    <td>
                        <a href="" class="title-set" data-toggle="modal-careerFit" data-target="interests-tab">
                            <span>By Interests</span>
                            <img src="/img/settings.svg" alt="">
                        </a>
                    </td>
                    @if ($countInterests)
                    <td>
                        <ul>
                            <li>
                                <div class="best">Best</div>
                                <p>{{ $by_interests[0]->name }}</p>
                            </li>
                            <li>
                                <div class="Better">Better</div>
                                <p>{{ $by_interests[1]->name }}</p>
                            </li>
                            <li>
                                <div class="Good">Good</div>
                                <p>{{ $by_interests[2]->name }}</p>
                            </li>
                        </ul>
                    </td>
                    @else
                    <td>
                        <p>Complete the survey to find your career fit by interests</p>
                    </td>
                    <td>
                        <a href="#" class="survey"  
                            data-toggle="modal-careerFit" data-target="interests-tab">Survey</a>
                    </td>
                    @endif
                </tr>
                <?php 
                    $by_values = $user->getUserProfessionBy(3);
                    $countValues = count($by_values);
                ?>
                <tr>
                    <td>
                        <a href="#" class="title-set" data-toggle="modal-careerFit" data-target="values-tab">
                            <span>By Values</span>
                            <img src="/img/settings.svg" alt="">
                        </a>
                    </td>
                    @if ($countValues)
                    <td>
                        <ul>
                            <li>
                                <div class="best">Best</div>
                                <p>{{ $by_values[0]->name }}</p>
                            </li>
                            <li>
                                <div class="Better">Better</div>
                                <p>{{ $by_values[1]->name }}</p>
                            </li>
                            <li>
                                <div class="Good">Good</div>
                                <p>{{ $by_values[2]->name }}</p>
                            </li>
                        </ul>
                    </td>
                    @else
                    <td>
                        <p>Complete the survey to find your career fit by values</p>
                    </td>
                    <td>
                        <a href="#" class="survey"  
                            data-toggle="modal-careerFit" data-target="values-tab">Survey</a>
                    </td>
                    @endif
                </tr>                                                       
            </table>
        </div>
        
        <div class="profile--dash" id="np-opportunities">
            <h3>Opportunities</h3>
            <ul class="profile--tabLink">
                <li><a href="" class="active" data-tab="tab2" id="jobs">Jobs</a></li>
                <li><a href=""   data-tab="tab3" id="networking">Events</a></li>
            </ul>
            <div class="profile--tabs">
                <div class="jobs tab active" id="tab2" 
                        data-get-url='/ajax/getUserJobs' data-add-url='/ajax/addUserJob'>
                    @if (!Auth::user()->getProfileKeywords())
                        <div class="wrap-empty">
                            <p>To discover relevant jobs, complete your profile</p>
                        </div>
                    @else
                        <div class="wrap-check">
                            <span>Recommended Jobs</span>
                            <label class="checkbox switcher">
                                <input type="checkbox">
                            </label>
                            <span>Saved Jobs</span>
                        </div>
                        <form action="" class="form-search">
                            <input class="keywords" placeholder="Search  by  keywords"
                                type="text" value="">
                            <input class="location" placeholder="Search  by  location"
                                type="text" value="">
                        </form>
                        <div class="row-table-network jobs"></div>
                    @endif                 
                </div>

                <div class="networking tab" id="tab3"
                    data-get-url='/ajax/getUserEvents' data-add-url='/ajax/addUserEvent'>
                    @if (!Auth::user()->getProfileKeywords())
                        <div class="wrap-empty">
                            <p>To discover relevant events, complete your profile</p>
                        </div>
                    @else
                        <div class="wrap-check">
                            <span>Recommended Events</span>
                            <label class="checkbox switcher">
                                <input type="checkbox">
                            </label>
                            <span>Saved Events</span>
                        </div>
                        <form action="" class="form-search">
                            <input class="keywords" placeholder="Search  by  keywords"
                                type="text" value="">
                            <input class="location" placeholder="Search  by  location"
                                type="text" value="">
                        </form>
                        <div class="row-table-network networking"></div>
                    @endif
                </div>
            </div>
        </div>
        
        @if ((!$is_owner && $count_expertises) || $is_owner)
        <div class="expertise">
            <h4>Expertise</h4>
            @if ($is_owner)
                <input id="expertise" type="text" 
                    placeholder="Enter your technical expertise..." value="">
                <ul class="tags">
                    <li class="hidden">
                        <a href="javascript:void(0);">
                            <span></span>
                            <img src="/img/close.png" alt="" class="close">
                        </a>
                    </li>
                    @foreach($expertises as $expertise)
                        <li>
                            <a href="javascript:void(0);">
                                <span>{{ $expertise->name }}</span>
                                <img src="/img/close.png" alt="" class="close">
                            </a>
                        </li>
                    @endforeach                                                         
                </ul>
            @else
                <ul class="tags">
                    @foreach($expertises as $expertise)
                        <li>
                            <a href="javascript:void(0);">
                                <span>{{ $expertise->name }}</span>
                            </a>
                        </li>
                    @endforeach                                                         
                </ul>
            @endif
        </div>
        @endif

        @if ($count_educations)
        <div class="education">
            <h4>Education</h4>
            <ul class="list-exp">
                @foreach($educations as $education)
                    <li>
                        <div class="icon">
                            <img src="/img/icon-head.svg" alt="">
                        </div>
                        <div class="info">
                            <h5>{{ $education->school }}</h5>
                            <p>{{ $education->from_year }} - 
                                @if ($education->to_year)
                                    {{ $education->to_year }}
                                @else
                                    Present
                                @endif
                            </p>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
        @endif

    </div>
</div>

@if ($is_owner)
<div class="modal-wrapper" id="modal-profEdit">
    <div class="modal-profEdit modal">
        <div class="close"><img src="/img/close-modal.svg" alt=""></div>
        <form id="profile_form" action="/profile/save" method="POST">
            {{ csrf_field() }}
            <div class="edit--info">
                <h4>Edit info</h4>
                <div class="prompt red" style="display: none;">
                    <div class="prompt-close"><img src="/img/warrning-close.svg" alt=""></div>
                        <b>Hmm!</b>
                        <p id="text"></p>
                </div>
                <div class="row">
                    <div class="left">
                        <div class="photo">
                            <img src="{{ $user->avatar }}" alt="" name="avatar">
                        </div>
                    </div>
                    <div class="right">
                        <label>
                            Select file
                        </label>
                        <input type="hidden" name="avatar" value="{{ $user->avatar }}">
                        <p>JPG, GIF or PNG. Max size of 800Kb</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h5>First Name</h5>
                        <input type="text" name="name" value="{{ $user->name }}" >
                    </div>
                    <div class="col">
                        <h5>Second Name</h5>
                        <input type="text" name="last_name" value="{{ $user->last_name }}" >
                    </div>              
                </div>
                <div class="row">
                    <h5>Title</h5>
                    <input type="text" name="title" value="{{ $user->title }}" >  
                </div>
                <div class="row">
                    <h5>Summary</h5>
                    <textarea name="summary">{{ $user->summary }}</textarea>   
                </div>          
            </div>
            <div class="education--info">
                <h4>Education</h4>
                @foreach($user->educations as $key => $education)
                    <div class="row">
                        <div class="col6">
                            <h5>School</h5>
                            <input type="text" name="educations[{{ $key }}][school]" 
                                value="{{ $education->school }}">
                        </div>
                        <div class="col2">
                            <h5>From Year</h5>
                            <input type="text" name="educations[{{ $key }}][from_year]"
                                value="{{ $education->from_year }}">
                        </div>
                        <div class="col2">
                            <h5>To Year (or expected)</h5>
                            <input type="text" name="educations[{{ $key }}][to_year]"
                                value="{{ $education->to_year }}">           
                        </div>
                    </div>
                @endforeach
                <?php $index = count($user->educations); ?>
                <div class="row hidden">
                    <div class="col6">
                        <h5>School</h5>
                        <input type="text" name="educations[{{ $index }}][school]" 
                            value="">
                    </div>
                    <div class="col2">
                        <h5>From Year</h5>
                        <input type="text" name="educations[{{ $index }}][from_year]"
                            value="">
                    </div>
                    <div class="col2">
                        <h5>To Year (or expected)</h5>
                        <input type="text" name="educations[{{ $index }}][to_year]"
                            value="">           
                    </div>
                </div>
                <a href="#" class="add-education" data-count="{{ $index }}">Add new education</a>
            </div>
            <div class="experience--info">
                <h4>Experience</h4>
                @foreach($user->experiences as $key => $experience)
                    <div class="row">
                        <div class="col3">
                            <h5>Title</h5>
                            <input type="text" name="experiences[{{ $key }}][title]"
                                value="{{ $experience->title }}">
                        </div>
                        <div class="col3">
                            <h5>Company</h5>
                            <input type="text" name="experiences[{{ $key }}][company]"
                                value="{{ $experience->company }}">
                        </div>
                        <div class="col2">
                            <h5>From Year</h5>
                            <input type="text" name="experiences[{{ $key }}][from_year]"
                                value="{{ $experience->from_year }}">
                        </div>
                        <div class="col2">
                            <h5>To Year (or expected)</h5>
                            <input type="text" name="experiences[{{ $key }}][to_year]"
                                value="{{ $experience->to_year }}">                     
                        </div>
                    </div>
                @endforeach
                <?php $index = count($user->experiences); ?>
                <div class="row hidden">
                    <div class="col3">
                        <h5>Title</h5>
                        <input type="text" name="experiences[{{ $index }}][title]"
                            value="">
                    </div>
                    <div class="col3">
                        <h5>Company</h5>
                        <input type="text" name="experiences[{{ $index }}][company]"
                            value="">
                    </div>
                    <div class="col2">
                        <h5>From Year</h5>
                        <input type="text" name="experiences[{{ $index }}][from_year]"
                            value="">
                    </div>
                    <div class="col2">
                        <h5>To Year (or expected)</h5>
                        <input type="text" name="experiences[{{ $index }}][to_year]"
                            value="">                     
                    </div>
                </div>
                <a href="#" class="add-experience" data-count="{{ $index }}">Add new experience</a>
            </div>  
            <button class="save">Save</button>      
        </form>
        <form id="upload_avatar">
            <input type="file" name="avatar" style="display: none;" accept=".jpg, .jpeg, .png">
        </form>
    </div>
</div>
@endif 

@if ($is_owner)
    @include('inc.popups.career_fit')
    @include('inc.popups.add_publications')
    @include('inc.popups.pinned_cards')
@endif
@endsection