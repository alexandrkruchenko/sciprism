@extends('layouts.scistart')

@section('content')
    <div class="about-us blog-title">
        <div class="about-main" style="
            @if ($post->thumbnail)
                background-image: url({{ url($post->thumbnail) }})
                @else
                    background-image: url(/img/about.jpg)
                @endif
            "
        >
            <div class="container">
                <h2>{{ $post->title }}</h2>
            </div>
        </div>
    </div>
    <div class="blog-inner">
    <div class="container">
        <div class="content">
            {!! $post->body !!}
            <div class="share">Share this story</div>
            <ul class="social">
                <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
                <li><a href=""><i class="fab fa-linkedin-in"></i></a></li>
                <li><a href=""><i class="fab fa-twitter"></i></a></li>
                <li><a href=""><i class="fab fa-google"></i></a></li>
            </ul>
            <div class="author">
                <!--<div class="photo">
                    <a href=""><img src="{{ asset('img/photo-img.png') }}" alt=""></a>
                </div>-->
                @if ($post->linkedin_link && $post->author)
                    <div  class="name">
                        By {{ $post->author }} 
                        <a href="{{ $post->linkedin_link }}"><i class="fab fa-linkedin-in"></i></a>
                    </div>
                @endif             
            </div>
        </div>
        <div class="right-column">
                <h3>RELATED ARTICLES</h3>
                <div class="wrap">
                    @foreach($post->getRelated() as $rp)
                        <div class="item">
                            <div class="image" 
                                style="
                                @if ($rp->thumbnail) 
                                    background-image: url({{ url($rp->thumbnail) }})
                                @else
                                    background-image: url(/img/about.jpg) 
                                @endif
                                "
                                >
                                <a href="/blog/{{ $rp->slug }}"></a>
                            </div>
                            <a href="/blog/{{ $rp->slug }}" class="title">
                                {{ $rp->title }}
                            </a>
                            <?php
                                $post_body = strip_tags($rp->body);
                            ?>
                            @if ($post_body)
                                <p>{{ App\Services\CardService::substrwords(strip_tags($post_body), 256) }}</p>
                            @endif
                        </div> 
                    @endforeach 
                </div>      
            </div>
        </div>
    </div>
    @include('inc.contact_form')
    @include('inc.footer_site')
@endsection