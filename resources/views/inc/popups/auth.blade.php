<div class="modal-wrapper" id="sign-up-first">
    <div class="modal-signUp mulltiple modal">
        <div class="close"><img src="img/close-modal.svg" alt=""></div>
        <div class="wrap">
            <div class="logo"><img src="img/logo.jpg" alt=""></div>
            <h4>Join the fastest growing community of researchers</h4>

            <div class="signUp-buttons">
                <a href="" class="btn linkedin">Linkedin</a>
                <a href="" class="btn google">Google</a>
                <a href="" class="btn facebook">ORCID</a>
                <a href="" class="btn email">Email</a>
            </div>
            <div class="signUp-buttons col-2">
                <a href="" class="btn mendeley">Mendeley</a>
                <a href="" class="btn researchGate">ResearchGate</a>
                <a href="" class="btn twitter">Twitter</a>
                <a href="" class="btn facebook-f">Facebook</a>
            </div>          
            <p>We will never post to any of yours accounts without your permission</p>
        </div>
    </div>
</div>


<div class="modal-wrapper" id="sign-up">
    <div class="modal-signUp modal">
        <div class="close"><img src="img/close-modal.svg" alt=""></div>
        <div class="wrap">
            <div class="logo"><img src="img/logo.jpg" alt=""></div>
            <h4>Join the fastest growing community of researchers</h4>
            <div class="signUp-buttons">
                <a href="https://orcid.org/oauth/authorize?client_id=APP-XC18CV1B3MUB1DFG&response_type=code&scope=/authenticate&redirect_uri=http://sciprism.com/home?auth=orcid" class="btn facebook" id="orcid_login">ORCID</a>
                <a href="" class="btn linkedin" id="linkedin_login" data-toggle="modal-stepSingup2">Linkedin</a>
                <a href="" class="btn google" id="google_login" data-toggle="modal-stepSingup3">Google</a>
                <a href="" class="btn email"  data-toggle="sign-email">Email</a>
            </div>
            <p>We will never post to any of yours accounts without your permission</p>
        </div>
    </div>
</div>



<div class="modal-wrapper sign-modal-wr" id="sign-email">
    <div class="modal-signUp modal">
        <div class="close"><img src="img/close-modal.svg" alt=""></div>
        <div class="wrap">
            <div class="logo"><img src="img/logo.jpg" alt=""></div>
            <h4>Join the fastest    growing science community</h4>
             <form class="email-form-sign register">
                 <input type="text" name="full_name" placeholder="Your full name" required="">
                 <input type="email" name="email" placeholder="Your email address" required="">
                 <input type="password" name="password" placeholder="Password (8+ characters)" required="">
             </form>
             <div class="link-sing">
                 <a href="" class="reset" data-toggle="sign-email-reset" style="display:none;">Reset  password</a>
                 <a href="" class="have_acc" data-toggle="sign-email-acc">Have an account?</a>
             </div>
             <a href="" data-toggle="sign-up" class="back">Back</a>
             <a href="" class="btn-sign-up register">Sign    up  </a>
             <p class="copy-modal">By clicking on Sign Up, you agree to our <a href="/page/terms">Terms</a></p>
        </div>
    </div>
</div>

<div class="modal-wrapper sign-modal-wr" id="sign-email-reset">
    <div class="modal-signUp modal">
        <div class="close"><img src="img/close-modal.svg" alt=""></div>
        <div class="wrap">
            <div class="logo"><img src="img/logo.jpg" alt=""></div>
            <h4>Join the fastest    growing science community</h4>
             <div class="reset-pass-block">
                 <h4>Forgot your    password?   </h4>
                 <form class="email-form-sign">
                    <input type="text" placeholder="Your email address">
                    <a href="" class="btn">Send    me  a   link    </a>
                 </form>
                 <a href="" class="back" data-toggle="sign-email">Go    back to Sign In</a>
             </div>
        </div>
    </div>
</div>

<div class="modal-wrapper sign-modal-wr" id="sign-email-acc">
    <div class="modal-signUp modal">
        <div class="close"><img src="img/close-modal.svg" alt=""></div>
        <div class="wrap">
            <div class="logo"><img src="img/logo.jpg" alt=""></div>
            <h4>Join the fastest    growing science community</h4>
             <form class="email-form-sign login">
                <h5>Already a user? Sign in</h5>
                 <input type="text" placeholder="Your email address" name="email">
                 <input type="password" name="password" placeholder="Password (8+ characters)">
                 <label class="checkbox">
                     <input type="checkbox" name="loginEmail">
                     <span class="text">Keep me logged in</span>
                 </label>
             </form>
  
             <a href="" class="btn-sign-up login">Sign up</a>
 
        </div>
    </div>
</div>

<div class="modal-wrapper" id="modal-stepSingup">
    <div class="modal-stepSingup modal">
        <div class="close"><img src="/img/close-modal.svg" alt=""></div>
        <div class="step1-sign step-s">
            <div class="top">
                <h5>Step 1 of 2</h5>
                <h3>Choose your areas of interest</h3>
            </div>
            <div class="tab-areas " >
                    <form>
                        @for ($i = 0; $i < $amount_categories; $i++)
                            @if ($i == 0 || $i == ceil($amount_categories / 2))
                                <div class="col">
                            @endif
                                <div class="form-group">
                                    <input type="checkbox" id="category-{{ $categories[$i]->id }}" 
                                        name="category[{{ $categories[$i]->id }}]" 
                                        class="category-interest">
                                    <label for="category-{{ $categories[$i]->id }}" class="checkbox">
                                        {{ $categories[$i]->title }}
                                        <i class="fas fa-check-circle"></i>
                                    </label>
                                </div>
                            @if ($i == (ceil($amount_categories / 2) - 1) || $i + 1 == $amount_categories)
                                </div>
                            @endif
                        @endfor
                    </form>
                </div>      
            <div class="wrap-btn">
                <a href="#" class="next to-step2" style="background-color: #ccc;">
                    <span>Next</span>
                    <img src="/img/arrow-next.svg" alt="">
                </a>
            </div>      
        </div>
        <div class="step2-sign step-s">
            <form id="sign_papers">
                <div class="top">
                    <!--<h5>Step 2 of 2</h5>-->
                    <h3>Let’s   build   your    impact  areas   <br>    
We  have    20+ million publications in  database.   Let’s   find    yours…  </h3>
                    <input type="text" placeholder="Enter your name (as it appears in your papers)" 
                        name="query" value="" autocomplete="off">
                </div>
                <div class="papers-alert">
                    <div class="wrap not-found" style="display: none">
                        <h3>Nothing is found</h3>
                        <p>Check the spelling or modify the query</p>
                    </div>
                    <div class="wrap load" style="display: none">
                        <h3>Wait...</h3>
                    </div>
                </div>
                <div class="wrap-btn">
                    <!--<a href="" class="prev to-step1">
                        <img src="/img/arrow-prev.svg" alt="">
                        <span>Back</span>
                    </a>-->         
                    <a href="javascript:void(0);" class="next done">
                        <span>All Done</span>
                        <img src="{{ asset('/img/check-white.svg') }}" alt="">
                    </a>
                </div> 
            </form>     
        </div>      
    </div>
</div>