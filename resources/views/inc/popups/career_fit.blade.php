<div class="modal-wrapper" id="modal-careerFit">
    <div class="modal-careerFit modal">
        <div class="close"><img src="/img/close-modal.svg" alt=""></div>
        <div class="wrap">
            <h3>Career fit</h3>
            <div class="careerFit-wr">
                <ul class="careerFit">
                    <li><a href="#" class="active" data-tab="skills-tab">By Skills</a></li>
                    <li><a href="#" data-tab="interests-tab">By Interests</a></li>
                    <li><a href="#" data-tab="values-tab">By Values</a></li>
                </ul>
            </div>
            <div class="tabs-Fit">
                <form action="/profile/saveTestResults" method="POST">
                    {{ csrf_field() }}
                    <div class="skills-tab tab active" id="skills-tab">
                        <div class="list-range">
                            <div class="row">
                                <div class="left">
                                </div>
                                <div class="right descrition-number">
                                    <ul>
                                        <li class="sep">1   (least) </li>
                                        <li class="sep">2</li>
                                        <li class="sep">3</li>
                                        <li class="sep">4</li>
                                        <li class="sep">5   (most)  </li>
                                    </ul>
                                </div>
                            </div>
                            @foreach($questions_by_skills as $q)
                            <?php
                                $value = 1;
                                $finded = $questions_results->filter(function($item) use($q) {
                                    return $item->question_id == $q->id;
                                })->first();
                                if ($finded) {
                                    $value = $finded->score;
                                }
                            ?>
                            <div class="row">
                                <input type="hidden" name="questions[{{ $q->id }}]" 
                                    value="{{ $value }}">
                                <div class="left">
                                    {{ $q->question }}
                                </div>
                                <div class="right">
                                    <ul>
                                        <li class="sep"></li>
                                        <li class="sep"></li>
                                        <li class="sep"></li>
                                        <li class="sep"></li>
                                        <li class="sep"></li>
                                    </ul>
                                    <div class="range"></div>
                                </div>
                            </div>
                            @endforeach
                        </div>                      
                    </div>
                    <div class="interests-tab tab" id="interests-tab">
                        <div class="list-range">
                            <div class="row">
                                <div class="left">
                                </div>
                                <div class="right descrition-number">
                                    <ul>
                                        <li class="sep">1   (least) </li>
                                        <li class="sep">2</li>
                                        <li class="sep">3</li>
                                        <li class="sep">4</li>
                                        <li class="sep">5   (most)  </li>
                                    </ul>
                                </div>
                            </div>
                            @foreach($questions_by_interests as $q)
                            <?php
                                $value = 1;
                                $finded = $questions_results->filter(function($item) use($q) {
                                    return $item->question_id == $q->id;
                                })->first();
                                if ($finded) {
                                    $value = $finded->score;
                                }
                            ?>
                            <div class="row">
                                <input type="hidden" name="questions[{{ $q->id }}]" 
                                    value="{{ $value }}">
                                <div class="left">
                                    {{ $q->question }}
                                </div>
                                <div class="right">
                                    <ul>
                                        <li class="sep"></li>
                                        <li class="sep"></li>
                                        <li class="sep"></li>
                                        <li class="sep"></li>
                                        <li class="sep"></li>
                                    </ul>
                                    <div class="range"></div>
                                </div>
                            </div>
                            @endforeach                             
                        </div>                              
                    </div>
                    <div class="values-tab tab" id="values-tab">
                        <div class="list-range">
                            <div class="row">
                                <div class="left">
                                </div>
                                <div class="right descrition-number">
                                    <ul>
                                        <li class="sep">1   (least) </li>
                                        <li class="sep">2</li>
                                        <li class="sep">3</li>
                                        <li class="sep">4</li>
                                        <li class="sep">5   (most)  </li>
                                    </ul>
                                </div>
                            </div>
                            @foreach($questions_by_values as $q)
                            <?php
                                $value = 1;
                                $finded = $questions_results->filter(function($item) use($q) {
                                    return $item->question_id == $q->id;
                                })->first();
                                if ($finded) {
                                    $value = $finded->score;
                                }
                            ?>
                            <div class="row">
                                <input type="hidden" name="questions[{{ $q->id }}]"
                                    value="{{ $value }}">
                                <div class="left">
                                    {{ $q->question }}
                                </div>
                                <div class="right">
                                    <ul>
                                        <li class="sep"></li>
                                        <li class="sep"></li>
                                        <li class="sep"></li>
                                        <li class="sep"></li>
                                        <li class="sep"></li>
                                    </ul>
                                    <div class="range"></div>
                                </div>
                            </div>
                            @endforeach                              
                        </div>                          
                    </div>
                    <button type="submit" class="save">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>