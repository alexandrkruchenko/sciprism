<div class="modal-wrapper" id="preferences-wrapper">
    <div class="modal-preferences modal">
        <div class="close"><img src="/img/close-modal.svg" alt=""></div>
        <div class="wrpa">
            <ul class="preferences-tabLink">
                <li><a href="#" class="active" tab-link="areas_of_interest">Areas of interest</a></li>
                <li><a href="#" tab-link="papers">Papers</a></li>
            </ul>
            <div class="preferences-tabs">
                <div class="tab-areas tab active" id="areas_of_interest">
                    <?php $userInterest = Auth::user()->categories; ?>
                    <div class="tab-content">
                        @for ($i = 0; $i < $amount_categories; $i++)
                            @if ($i == 0 || $i == ceil($amount_categories / 2))
                                <div class="col">
                            @endif
                                <div class="form-group">
                                    <input type="checkbox" id="category-{{ $categories[$i]->id }}" 
                                        name="category[{{ $categories[$i]->id }}]" 
                                        class="category-interest"
                                        @foreach($userInterest as $ui)
                                            @if ($ui->id == $categories[$i]->id)
                                                checked=""
                                            @endif
                                        @endforeach
                                    >
                                    <label for="category-{{ $categories[$i]->id }}" class="checkbox">
                                        {{ $categories[$i]->title }}
                                        <i class="fas fa-check-circle"></i>
                                    </label>
                                </div>
                            @if ($i == (ceil($amount_categories / 2) - 1) || $i + 1 == $amount_categories)
                                </div>
                            @endif
                        @endfor            
                    </div>
                </div>
                <div class="tab-papers tab" id="papers">
                    <div class="tab-content">
                        <div class="wrap-list">
                        </div>
                    </div>
                </div>
                <button class="save btn">Save</button>
            </div>
        </div>
    </div>
</div>