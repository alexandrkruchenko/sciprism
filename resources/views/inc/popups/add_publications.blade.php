<div class="modal-wrapper" id="manage-publication">
    <div class="manage-publication modal">
    <div class="close"><img src="/img/close-modal.svg" alt=""></div>
        <div class="wrap">
            <h3>Add your publications </h3>
            <input type="text" class="search" placeholder="Enter Pubmed ID, DOI, author’s name or title">
            <div class="search-popup-conent">
                <div class="wr">
                    <h4 id="loader" style="display: none;">Wait...</h4>
                    <div id="input_message">
                        <h4>Let’s get started!</h4>
                        <p>Enter your search criteria and we’ll do the rest</p>
                        <div class="flags">
                            <img class="img2" src="/img/line-icons2.png" alt="">
                            <img class="img1" src="/img/line-icons.png" alt="">
                        </div>
                    </div>
                </div>
            </div>          
        </div>     
    </div>
</div>