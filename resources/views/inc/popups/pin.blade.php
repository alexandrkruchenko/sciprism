<div class="modal-wrapper" id="papers-wrapper">
    <div class="modal-pinned modal">
        <div class="close"><img src="/img/close-modal.svg" alt=""></div>
        <div class="wrap">
            <h3>Pin your publication(s) to this topic</h3>
            <div class="user-pins">
            </div>
        </div>
    </div>
</div>