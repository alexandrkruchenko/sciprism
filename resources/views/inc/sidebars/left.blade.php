<div class="sidebar col-left col">
    <div id="sidebar">
        <div class="side--impact">
            @if ($impact_areas)
                <?php
                    echo App\Services\PaperService::renderImpactAreasForFeedPage($impact_areas);
                ?>
            @else
                <div class="wr-chart-side">
                    <div class="title">Your impact</div>
                     <div id="chart-1" class="chart"></div>
                </div>   
                <p>Your impact<br>  is  being   calculated</p>   
                <p>Wait…</p>
            @endif
        </div>
        <a href="" class="btn add-btn @if (!$impact_areas) empty @endif"  id="upload_publication" data-toggle="manage-publication">
            @if ($impact_areas)
                <span>Add Publications</span>
            @else
                <span>Add Publications to find your impact.</span>
            @endif
        </a>
    </div>
</div>
<script>
function initImpactAreas(colorset, impactAreas) {
    CanvasJS.addColorSet("custom", colorset);

    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        colorSet: "custom",
        data: [{
            type: "doughnut",
            startAngle: 60,
            //innerRadius: 60,
            indexLabelFontSize: 17,
            indexLabel: "{}",
            toolTipContent: "<b>{label}</b>",
            dataPoints: impactAreas
        }]
    });
    chart.render();
}
@if ($impact_areas)
    window.onload = function () {
        var colorset = [
            @foreach($impact_areas as $area)
                "{{ $area['color'] }}",
            @endforeach               
        ];

        var impactAreas = [
            @foreach($impact_areas as $area)
                { y: {{ $area['percent'] }}, label: "{{ $area['title'] }}" },
            @endforeach
        ];

        initImpactAreas(colorset, impactAreas);
    }
@else
    var checkImpactAreas = true;
@endif
</script>