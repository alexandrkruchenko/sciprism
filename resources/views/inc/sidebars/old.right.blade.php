@if (Auth::user())
    <div class="col-right col" id="rightbar">
        <div class="profile-complit item">
            <div class="photo"><a href="/profile"><img src="{{ Auth::user()->avatar }}" alt=""></a></div>
            <a href="/profile" class="name">{{ Auth::user()->name }} {{ Auth::user()->last_name }}</a>
            <p>{{ Auth::user()->title }}</p>
            <h5>Profile strength</h5>
            <?php
                $profileComplitenessPercent = Auth::user()->getProfileCompletenessPercent();
            ?>
            <div class="line-progress">
                <span style="width: {{ $profileComplitenessPercent }}%"></span>
            </div>
            <div class="percent-line">{{ $profileComplitenessPercent }}%</div>
        </div>
        <?php
            $impact_areas = App\Services\PaperService::renderImpactAreasForFeedPage(
                App\Paper::getImpactAreasForUser(Auth::user()->id, 5)
            );
        ?>
        <div id="no_scroll">
            <div class="item" id="tour_3">
                
                    <div class="impact">
                        <div class="title">Your impact areas</div>
                        @if ($impact_areas)
                            {!! $impact_areas !!}
                        @else
                            <div class="loading"><img src="/img/ajax-load.gif"></div>
                        @endif
                    </div>
                <?php
                    $eventsCount = 0;
                    $jobsCount = 0;
                    if (Auth::user()->expertises()->count()) {
                        $events = App\Services\EventbriteService::getEvents();
                        $eventsCount = $events->pagination->object_count; 
                        $jobs = App\Services\LinkUpService::getJobs();
                        $jobsCount = $jobs->result_info->total_jobs;
                    }
                ?>
                <div class="opportunity">
                    <div class="title">Your opportunities</div>
                    <ul>
                        <li>
                            <div class="icon">
                                <img src="/img/jobs.png" alt="">
                            </div>
                            <div class="count">{{ $jobsCount }}</div>
                            <span>Jobs</span>
                        </li>
                        <li>
                            <div class="icon">
                                <img src="/img/calendar.png" alt="">
                            </div>
                            <div class="count">{{ $eventsCount }}</div>
                            <span>Events</span>
                        </li>                   
                    </ul>
                    <a href="{{ Auth::user()->getUserLink() }}" class="explore">Explore</a>
                </div>
            </div>
            @include('/inc/footer')
        </div>
    </div>
@else
    <div class="col-right col" id="rightbar">
        <div id="no_scroll">
            <div class="discover-list">
                <div class="discover-item">
                    <div class="image" style="background-image: url(/img/discover1.png)">
                        <a href=""></a>
                    </div>
                    <a href="#" class="title">Discover your impact areas</a>
                    <p>Explore the areas where you can make the most meaningful impact.</p>
                    <a href="#" class="link">Explore</a>
                </div>
                <div class="discover-item">
                    <div class="image" style="background-image: url(/img/discover2.png)">
                        <a href=""></a>
                    </div>
                    <a href="" class="title">Connect your publications</a>
                    <p>Connect your publicatins beyond Pubmed. Connect with industry.</p>
                    <a href="" class="link">Explore</a>
                </div>              
            </div>
            @include('/inc/footer')
        </div>
    </div>
@endif