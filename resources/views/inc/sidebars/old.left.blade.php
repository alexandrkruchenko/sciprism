<div class="sidebar col-left col @if (!Auth::user()) home-sidebar @endif">
    <ul id="sidebar">
        @if (Auth::user())
            <li>
                <a href="/" @if (Route::currentRouteName() == 'index') class="active" @endif>
                    <span class="icon"><img src="/img/home.png" alt=""></span>
                    <span class="text">Home</span>
                </a>
            </li>
            <li>
                <a href="/following" @if (Route::currentRouteName() == 'following') class="active" @endif>
                    <span class="icon"><img src="/img/star.png" alt=""></span>
                    <span class="text">Following</span>
                </a>
            </li>
        @endif
        <?php
            $to = $amount_categories > 8 ? 8 : $amount_categories;
        ?>
        @for ($i = 0; $i < $to; $i++)
            <li>
                <a href="/category/{{ $categories[$i]->slug }}">
                    @if ($categories[$i]->icon && file_exists(public_path($categories[$i]->icon)))
                        <span class="icon">
                            <img src="{{ $categories[$i]->icon }}" 
                                alt="{{ $categories[$i]->title }}">
                        </span>
                    @endif
                    <span class="text">{{ $categories[$i]->title }}</span>
                </a>
            </li>
        @endfor
        @if (Auth::user())
        <li>
            <a href="" data-toggle="preferences-wrapper">
                <span class="icon"><img src="/img/icon-menu11.svg" alt=""></span>
                <span class="text">My preferences</span>
            </a>
        </li>
        @endif
        <div class="other-links-wrapper">
            <li class="all-links-toggle">
                <a href="#">
                    <span class="icon"></span>
                    <span class="text">All Topics</span>
                </a>
            </li>
            @if ($amount_categories > 8)
            <ul class="other-links">
                @for ($i = $to; $i < $amount_categories; $i++)
                    <li>
                        <a href="/category/{{ $categories[$i]->slug }}">
                        @if ($categories[$i]->icon && file_exists(public_path($categories[$i]->icon)))
                            <span class="icon">
                                <img src="{{ $categories[$i]->icon }}" 
                                    alt="{{ $categories[$i]->title }}">
                            </span>
                        @endif
                        <span class="text">{{ $categories[$i]->title }}</span>
                        </a>                        
                    </li>
                @endfor                            
            </ul>
            @endif
        </div>
    </ul>
</div>