<div class="col-right col" id="rightbar">
    <div class="item">
        <div class="career--fit">
            <div class="title">Career fit</div>
             <ul>
                <?php 
                    $bySkills = Auth::user()->getUserProfessionBy(1);
                    $countSkills = count($bySkills);
                ?>
                 <li @if ($countSkills) class="active" @endif>
                    <a href="" 
                        @if ($countSkills) 
                            data-toggle="modal-careerFit" data-target="skills-tab" 
                        @endif
                    >
                         <span>By skills</span>
                         <div class="check"></div>
                     </a>
                 </li>
                 <?php 
                    $byInterests = Auth::user()->getUserProfessionBy(2);
                    $countInterests = count($byInterests);
                 ?>
                 <li @if ($countInterests) class="active" @endif>
                     <a href="" 
                        @if ($countInterests)
                            data-toggle="modal-careerFit" data-target="interests-tab"
                        @endif
                      >
                         <span>By interests</span>
                         <div class="check"></div>
                     </a>
                 </li>
                 <?php 
                    $byValues = Auth::user()->getUserProfessionBy(3);
                    $countValues = count($byValues);
                  ?>
                 <li @if ($countValues) class="active" @endif>
                     <a href=""
                        @if ($countValues)
                            data-toggle="modal-careerFit" data-target="values-tab"
                        @endif
                      >
                         <span>By Values</span>
                         <div class="check"></div>
                     </a>
                 </li>                                               
             </ul>
        </div>
        @if (!$countSkills && !$countInterests && !$countValues)
            <p class="notice">Complete career fit to find opportunities.</p>
        @endif
        <div class="opportunity">
            <div class="title">Opportunities</div>
            <?php
                $eventsCount = 0;
                $jobsCount = 0;
                if (Auth::user()->getProfileKeywords()) {
                    $events = App\Services\EventbriteService::getEvents();
                    $eventsCount = $events->pagination->object_count; 
                    $jobs = App\Services\LinkUpService::getJobs();
                    $jobsCount = $jobs->result_info->total_jobs;
                }
            ?>
            <ul>
                <li>
                    <div class="icon">
                        <img src="/img/jobs.png" alt="">
                    </div>
                    <div class="count">{{ $jobsCount }}</div>
                    <span>Jobs</span>
                </li>
                <li>
                    <div class="icon">
                        <img src="/img/calendar.png" alt="">
                    </div>
                    <div class="count">{{ $eventsCount }}</div>
                    <span>Events</span>
                </li>                   
            </ul>
            <a href="{{ Auth::user()->getUserLink() }}#np-opportunities" class="explore">Explore</a>
        </div>
    </div>
    @include('/inc/footer_mobile')
</div>