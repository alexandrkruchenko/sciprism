<div class="mailing" id="mailing">
    <div class="container">
        <h2 class="h2">Join our mailing list</h2>
        <h4 class="h4">Register here to receive the latest news and updates from SciFax.</h4>
        @if ($errors->any())
            <div class="prompt red">
                <div class="prompt-close"><img src="/img/warrning-close.svg" alt=""></div>
                    <b>Hmm!</b>
                    @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
            </div>
        @endif
        @if (session('success'))
            <div class="prompt green">
                <div class="prompt-close"><img src="/img/close-green.svg" alt=""></div>
                    <p><b>Great!</b> {{ session('success') }}</p>
            </div>
        @endif
        <form action="/subscribers/add#mailing" method="post">
            {{ csrf_field() }}
            <input type="text" class="form-control" placeholder="First name" name="first_name">
            <input type="text" class="form-control" placeholder="Last name" name="last_name">
            <input type="email" class="form-control" placeholder="Your email" name="email">
            <input type="submit" class="sing-up" value="Sign up">
        </form>
    </div>
</div>