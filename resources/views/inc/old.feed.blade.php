<div class="content col-center col" id="center">
    @if (Auth::user())
        @if (session('welcome_message'))
            <div class="welcome">
                <h4>{{ Auth::user()->name }}, welcome to SciStart</h4>
                <p>Thanks for signing up — your account’s ready to go!</p>
            </div>
        @endif
        @if (session('success'))
            <div class="prompt blue">
                <div class="prompt-close"><img src="/img/close-blue.svg" alt=""></div>
                <p>{!! session('success') !!}</p>
            </div>
        @endif
        <div class="add-publikations">
            <h4>Add your publications </h4>
            <form>
                <input type="text" class="text" placeholder="Enter Pubmed ID, DOI, author’s name or title">
                <input id="search_publications" type="submit" class="btn"  value="Add your publications" data-toggle="manage-publication">
            </form>
        </div>
    @endif
    <div class="wrapper-publication">
        @if (Auth::user() && (!isset($search) || !$search))
            <ul class="nav-tabs">
                <li>
                    <a href="/#relevant" class="feed-tab active" data-type="relevant">
                        RELEVANT
                    </a>
                </li>
                <li>
                    <a href="/#popular" class="feed-tab" data-type="popular">
                        POPULAR
                    </a>
                </li>
                <li>
                    <a href="/#recent" class="feed-tab" data-type="recent">
                        RECENT
                    </a>
                </li>
            </ul>
        @endif
        <div class="list-publication">
            @if (Route::currentRouteName() != 'following' && (!isset($search) || !$search))
                <div class="tab-data relevant" data-offset="0"></div>
            @endif
            @if (Auth::user())
                <div class="tab-data popular" style="display: none;" data-offset="0"></div>
                <div class="tab-data recent" @if ((!isset($search) || !$search)) 
                    style="display: none;" @endif data-offset="0"></div>
            @endif
            @if (Route::currentRouteName() == 'following')
                <div class="tab-data following" data-offset="0"></div>
            @endif                                               
        </div>
    </div>  
</div>