<div class="mailing" id="mailing">
    <div class="container">
        <h2 class="h2">Сontact form</h2>
         @if ($errors->any())
            <div class="prompt red">
                <div class="prompt-close"><img src="/img/warrning-close.svg" alt=""></div>
                    <b>Hmm!</b>
                    @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
            </div>
        @endif
        @if (session('success'))
            <div class="prompt green">
                <div class="prompt-close"><img src="/img/close-green.svg" alt=""></div>
                    <p><b>Great!</b> {{ session('success') }}</p>
            </div>
        @endif
        <form action="/page/contact-us/addFeedback#mailing" method="post">
            {{ csrf_field() }}
            <input type="text" class="form-control" placeholder="Your name" name="name" 
                value="{{ old('name') }}">
            <input type="email" class="form-control" placeholder="Your email" name="email"
                value="{{ old('email') }}">
            <input type="text" class="form-control" placeholder="Subject" name="subject"
                value="{{ old('subject') }}">
            <textarea class="form-control" placeholder="Message" name="message">{{ old('message') }}</textarea>
            <input type="submit" class="sing-up" value="Send">
        </form>
    </div>
</div>