<div class="footer mob-hidden">
    <ul>
        @if (Route::currentRouteName() != 'home')
            <li><a href="/page/faq" class="mob-hidden">FAQ</a></li>
        @endif
        <li><a href="/page/terms">Terms</a></li>
        <li><a href="/page/privacy">Privacy</a></li>
        <li><a href="/blog">Blog</a></li>
        @if (Route::currentRouteName() == 'home' || 
            Route::currentRouteName() == 'page' ||
            Route::currentRouteName() == 'blog'

        )
            <li><a href="/page/how-it-works">How it works</a></li>
        @else
            <li><a href="/page/about" class="mob-hidden">About Us</a></li>
        @endif
        <li><a href="/page/contact-us">Contact us</a></li>
    </ul>
    <div class="copyright">© {{ config('app.name', 'Sciprism') }} {{ date('Y') }}</div>
</div>