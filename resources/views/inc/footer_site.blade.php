<div class="footer-site footer">
    <div class="container">
        <ul>
            <li><a href="/page/about">About</a></li>
            <li><a href="/page/how-it-works">How it works</a></li>
            <li><a href="/page/faq">FAQ</a></li>
            <li><a href="/page/terms">Terms</a></li>
            <li><a href="/page/privacy">Privacy</a></li>
            <li><a href="/blog">Blog</a></li>
            <li><a href="/page/contact-us">Contact us</a></li>
        </ul>
        <!--<ul class="social">
            <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
            <li><a href=""><i class="fab fa-linkedin-in"></i></a></li>
            <li><a href=""><i class="fab fa-twitter"></i></a></li>
            <li><a href=""><i class="fab fa-google"></i></a></li>
            <li><a href=""><i class="fab fa-youtube"></i></a></li>
        </ul>-->
        <div class="copyright">© {{ config('app.name', 'Sciprism') }} {{ date('Y') }}</div>
    </div>
</div>