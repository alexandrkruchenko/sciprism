<div class="content col-center col" id="center">
    @if (Auth::user())
        @if (session('success'))
            <div class="prompt blue">
                <div class="prompt-close"><img src="/img/close-blue.svg" alt=""></div>
                <p>{!! session('success') !!}</p>
            </div>
        @endif
    @endif
    <div class="wrapper-publication">
        <div class="top--fead">
            <ul class="nav-tabs">
                <li><a href="" class="first active" data-type="pub-list">PUBLICATIONS</a></li>
                <li><a href="" class="second" data-type="innov-list">INNOVATIONS</a></li>
            </ul>
            <div class="wrap-check">
                <span>Relevant</span>
                <label class="checkbox">
                    <input type="checkbox" name="relevant-saved">
                </label>
                <span>Saved</span>
            </div>                    
        </div>
        <div class="list-publication pub-list">
        </div>
        <div class="list-publication innov-list">
        </div>              
    </div>  
</div>