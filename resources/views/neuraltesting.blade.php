<!DOCTYPE html>
<html>
<head>
    <title>Neural network testing</title>
    <link href="{!! asset('css/app.css') !!}" media="all" rel="stylesheet" type="text/css" />
    <style>
        .main{
            padding-top: 40px;
        }
    </style>
</head>
<body>
    <div class="container main">
        <h1>Testing neural network</h1>
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (session('category'))
                <div class="alert alert-info">
                    Predicted category is <strong>{{ session('category') }}</strong>
                </div>
            @endif
            <form method="post" action="/neuralnetwork/predict">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <label>Article
                        <textarea class="form-control" placeholder="input article here" name="article" style="width: 669px; height: 240px;"></textarea>
                    </label>
                </div>
                <div class="row">
                    <button type="submit" class="btn btn-default">Predict category</button>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript" src="{!! asset('js/app.js') !!}"></script>
</body>
</html>