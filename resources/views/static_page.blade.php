@extends('layouts.scistart')

@section('content')
    {!! $body !!}
    @if($slug == 'contact-us')
        @include('inc.contact_form')
    @elseif ($slug != 'home')
        @include('inc.subscribe_form')
    @endif
    @include('inc.footer_site')
@endsection