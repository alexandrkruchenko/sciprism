@extends('layouts.scistart')

@section('content')

<div class="page">
    <div class="container">
        <!--<div class="toggle-sidebar">
            <span>Menu</span>
            <div class="toggle">
                <span class="bar bar1"></span>
                <span class="bar bar2"></span>
                <span class="bar bar3"></span>
            </div>      
        </div> -->
        @include('inc.sidebars.left')

        @include('inc.sidebars.right')

        @include('inc.feed')
    </div>
</div>

@if (Auth::user())
    @include('inc.popups.add_publications')
    @include('inc.popups.pin')
    @include('inc.popups.preferences')
    @include('inc.popups.career_fit')
@endif

@include('inc.popups.card')
@include('inc.popups.paper')

<script>
    /*var categoryId = @if (isset($category_id)) {{ $category_id }} @else 0 @endif;
    var search = @if (isset($search)) '{{ $search }}' @else '' @endif;*/
</script>

@endsection