<?php
// config
$link_limit = 5; // maximum number of links (a little bit inaccurate, but will be ok for now)
?>

@if ($paginator->lastPage() > 1)
    <ul class="paginaion">
        @if ($paginator->currentPage() != 1)
            <li>
                <a class="prev" href="{{ $paginator->url(1) }}">First</a>
            </li>
        @endif
        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            <?php
            $half_total_links = floor($link_limit / 2);
            $from = $paginator->currentPage() - $half_total_links;
            $to = $paginator->currentPage() + $half_total_links;
            if ($paginator->currentPage() < $half_total_links) {
               $to += $half_total_links - $paginator->currentPage();
            }
            if ($paginator->lastPage() - $paginator->currentPage() < $half_total_links) {
                $from -= $half_total_links - ($paginator->lastPage() - $paginator->currentPage()) - 1;
            }
            ?>
            @if ($from < $i && $i < $to)
                <li>
                    <a class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}" 
                        href="{{ $paginator->url($i) }}">{{ $i }}</a>
                </li>
            @endif
        @endfor
        @if ($paginator->currentPage() != $paginator->lastPage())
        <li>
            <a class="next" href="{{ $paginator->url($paginator->lastPage()) }}">Last</a>
        </li>
        @endif
    </ul>
@endif