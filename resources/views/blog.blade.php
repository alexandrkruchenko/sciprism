@extends('layouts.scistart')

@section('content')
    <div class="about-us">
        <div class="about-main" style="background-image: url(/img/about.jpg)">
            <div class="container">
                <h2>Blog</h2>
            </div>
        </div>
    </div>
    <div class="blog">
        <div class="container">
            <div class="blog-list">
                @foreach($posts as $post)
                    <div class="item">
                        <div class="image" 
                            style="
                            @if ($post->thumbnail) 
                                background-image: url({{ url($post->thumbnail) }})
                            @else
                                background-image: url(/img/about.jpg) 
                            @endif
                            "
                            >
                            <a href="/blog/{{ $post->slug }}"></a>
                        </div>
                        
                        <a href="/blog/{{ $post->slug }}" class="title">
                            {{ $post->title }}
                        </a>
                        <?php
                            $post_body = strip_tags($post->body);
                        ?>
                        @if ($post_body)
                            {{ App\Services\CardService::substrwords(strip_tags($post_body), 256) }}
                        @endif
                    </div>
                @endforeach
            </div>
            {{ $posts->links('pagination.blog') }}
        </div>
    </div>
    @include('inc.contact_form')
    @include('inc.footer_site')
@endsection