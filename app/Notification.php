<?php

namespace App;

use App\UserNotification;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class Notification extends Model
{
    public $timestamps = false;

    /**
     * Send notification email
     * @param  User $user
     * @return void
     */
    public function sendEmail($user)
    {
        try {
            $subject = $this->generateText(
                $this->subject,
                [
                    'username' => $user->name,
                    'projectname' => config('app.name')
                ]
            );
            $text = $this->generateText(
                $this->text,
                [
                    'username' => $user->name,
                    'projectname' => config('app.name')
                ]
            );
            $emailData = [
                'to' => $user->email,
                'subject' => $subject
            ];
            Mail::send(
                'emails.empty', 
                ['text' => $text], 
                function($message) use ($emailData) {
                    $message->to($emailData['to'])
                        ->subject($emailData['subject']);
            });
        } catch(\Exception $e) {
        }
    }

    /**
     * Send notification internal
     * @param  User $user
     * @return void
     */
    public function sendInternal($user)
    {
        $notification = new UserNotification;
        $notification->user_id = $user->id;
        $notification->text = $this->generateText(
            $this->internal_text,
            [
                'name' => $user->name,
                'site' => config('app.name')
            ]
        );
        $notification->event_uri = $this->generateText(
            $this->event_uri,
            [
                'user_id' => $user->id
            ]
        );
        $notification->save();
    }

    /**
     * Generate text from template
     * @param  string $text
     * @param  array $data
     * @return string
     */
    private function generateText($text, $data)
    {
        return str_replace(
            config('settings.notification_vars'),
            $data,
            $text
        );
    }
}
