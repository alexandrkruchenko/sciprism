<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaperFollow extends Model
{
    public $timestamps = false;
}
