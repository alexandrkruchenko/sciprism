<?php

namespace App\Listeners;

use App\Notification;

use App\Events\UserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendWelcomeNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        $user = $event->user;
        $notification = Notification::where('type', 'welcome')->first();
        if ($notification && $notification->enabled) {
            if ($notification->email) {
                $notification->sendEmail($user);
            }
        }
    }
}
