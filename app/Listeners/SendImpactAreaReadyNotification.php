<?php

namespace App\Listeners;

use App\Notification;

use App\Events\ImpactAreaReady;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendImpactAreaReadyNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ImpactAreaReady  $event
     * @return void
     */
    public function handle(ImpactAreaReady $event)
    {
        $user = $event->user;
        $notification = Notification::where('type', 'impact_areas_ready')->first();
        if ($notification && $notification->enabled) {
            if ($notification->email) {
                $notification->sendEmail($user);
            }
            if ($notification->internal) {
                $notification->sendInternal($user);
            }
        }
    }
}
