<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Paper extends Model
{
    protected $fillable = [
        'external_id',
        'type',
        'was_predicted',
        'title',
        'description',
        'journal',
        'authors',
        'publication_date'
    ];

    /**
     * The category cards.
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category', 'category_paper');
    }

    /**
     * The category cards.
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'paper_user');
    }


    /**
     * The category cards.
     */
    public function meta()
    {
        return $this->hasMany('App\PaperMeta');
    }

    /**
     * The paper pinned cards.
     */
    public function pinnedCards()
    {
        return $this->hasMany('App\UserPinCard');
    }

    /**
     * The user pin.
     */
    public function userPin()
    {
        return $this->belongsToMany('App\User', 'paper_user', 'paper_id', 'user_id');
    }

    /**
     * The paper cards
     */
    public function pinnedCard()
    {
        return $this->belongsToMany('App\Card', 'user_pin_card');
    }

    /**
     * Get realted paper
     * @param  integer $limit
     * @return Paper
     */
    public function getRelated($limit = 5)
    {
        return Paper::whereHas('categories', function ($query) {
            $categoryIds = $this->categories()->pluck('categories.id')->all();
            $query->whereIn('categories.id', $categoryIds);
        })->where('id', '<>', $this->id)->inRandomOrder()->limit($limit)->get();
    }

    /**
     * Get pubmed link
     * @return string
     */
    public function getPubmedLink()
    {
        return 'https://www.ncbi.nlm.nih.gov/pubmed/'.$this->external_id;
    }

    /**
     * Get pubmed link
     * @return string
     */
    public function getPaperDescription()
    {
        $description = $this->getPubmedAuthors().' / '.$this->journal.' / ';
        if ($this->publication_date != '0000-00-00 00:00:00') {
            $description .= date('Y', strtotime($this->publication_date)).' / ';
        }
        $description .= 'Pubmed ID: #'.$this->external_id;
        return $description;
    }


    /**
     * Get pubmed authors
     * @return string
     */
    public function getPubmedAuthors()
    {
        $authorsString = '';
        $meta = $this->meta()->where('name', 'authors')->first();
        if ($meta) {
            $authorsString = $meta->value;
        } else {
            $authorsString = $this->authors;
        }
        return $authorsString;
    }

    /**
     * Impact areas for user
     * @param  integer $userId
     * @return void
     */
    public static function getImpactAreasForUser($userId, $limit)
    {
        return DB::select('
            SELECT 
                categories.title, categories.color, COUNT(categories.id) as count
            FROM papers
            JOIN paper_user ON paper_user.paper_id = papers.id
            JOIN user_pin_card ON user_pin_card.paper_id = papers.id
            JOIN category_card ON category_card.card_id = user_pin_card.card_id
            JOIN categories ON categories.id = category_card.category_id
            WHERE paper_user.user_id = ?
            GROUP BY categories.id
            ORDER BY count DESC
            LIMIT ?',
            [$userId, $limit]
        );
    }

    /**
     * Paper followers
     */
    public function followers()
    {
        return $this->belongsToMany('App\User', 'paper_follows');
    }

    /**
     * User followed?
     * @param  integer $userId
     * @return void
     */
    public function isFollowed($userId)
    {
        return $this->followers()->where('users.id', $userId)->first();

    }

    /**
     * The paper comments.
     */
    public function comments()
    {
        return $this->hasMany('App\PaperComment');
    }

    /**
     * Get relevant papers by user id
     * @param integer $userId
     * @param integer $offset
     * @param integer $limit
     * @return 
     */
    /*public static function getRelevantPapersByUserId($userId, $offset, $limit)
    {
        return DB::select(
            'SELECT
                papers.id as id, papers.title as title,
                papers.description as description,
                COUNT(paper_pinned_user.id) as count_pinned_pappers,
                (IF(user_categories.category_id != 0, 1, 0)) as user_interested,
                (IF(categories.title != "", categories.title, "Uncategorized")) as category_title,
                users.name as owner
            FROM papers
            LEFT OUTER JOIN paper_pinned_user ON paper_pinned_user.paper_id = papers.id
            LEFT OUTER JOIN paper_user ON paper_user.paper_id = papers.id
            LEFT OUTER JOIN users ON users.id = paper_user.user_id
            LEFT OUTER JOIN category_paper ON category_paper.paper_id = papers.id
            LEFT OUTER JOIN (
                SELECT category_paper.paper_id as paper_id, category_user.category_id as category_id
                FROM category_paper
                LEFT JOIN category_user ON category_user.category_id = category_paper.category_id
                WHERE category_user.user_id = ?
            ) as user_categories ON user_categories.paper_id = papers.id
            LEFT OUTER JOIN categories ON categories.id = category_paper.paper_id
            GROUP BY papers.id
            ORDER BY user_interested DESC,count_pinned_pappers DESC
            LIMIT ?, ?', 
            [$userId, $offset, $limit]
        );
    }*/
}
