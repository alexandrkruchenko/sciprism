<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareerFitQuestion extends Model
{
    public $timestamps = false;
    protected $table = 'careerfit_questions';
}
