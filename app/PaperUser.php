<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class PaperUser extends Model
{
    public $timestamps = false;
    protected $table = 'paper_user';
}
