<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Page;
use App\Category;

class PageController extends Controller
{
    public function index($slug)
    {
        $categories = Category::limit(20)->get();
        $page = Page::where('slug', $slug)
            ->first();
        if (!$page) {
            return abort(404);
        }
        $data = [
            'slug' => $slug,
            'title' => $page->title,
            'body' => $page->body,
            'meta_title' => $page->meta_title,
            'meta_description' => $page->meta_description,
            'meta_keywords' => $page->meta_keywords,
            'categories' => $categories,
            'amount_categories' => count($categories),
        ];
        return view('static_page', $data);
    }
}
