<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use App\Category;

class BlogController extends Controller
{
    /**
     * Main blog page
     * @return
     */
    public function index()
    {
        $categories = Category::get();
        $posts = Post::paginate(9);
        $data = [
            'categories' => $categories,
            'amount_categories' => count($categories),
            'posts' => $posts
        ];
        return view('blog', $data);
    }

    /**
     * Post page
     * @return
     */
    public function post($slug)
    {
        $post = Post::where('slug', $slug)
            ->first();
        if (!$post) {
            return abort(404);
        }
        $categories = Category::get();
        $data = [
            'post' => $post,
            'meta_title' => $post->meta_title,
            'meta_description' => $post->meta_description,
            'meta_keywords' => $post->meta_keywords,
            'categories' => $categories,
            'amount_categories' => count($categories)
        ];
        return view('blog_inner', $data);
    }
}
