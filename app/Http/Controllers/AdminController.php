<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use App\User;
use App\Paper;
use App\UserPinCard;
use Response;
use Image;
use View;

use Khill\Lavacharts\Lavacharts;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Export user to CSV
     * @return Response
     */
    public function usersToCsv()
    {
        $headers = [
            'Content-type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename=users.csv',
            'Pragma' => 'no-cache',
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Expires' => '0'
        ];

        $users = User::get();
        $columns = [
            '#',
            'Name', 
            'Email address', 
            'IP address', 
            'Registration Date', 
            'Network', 
            'Blocked', 
            'Impact areas'
        ];

        $callback = function() use ($users, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($users as $user) {
                $categories = $user->categories()->get();
                $categoryString = '';
                foreach ($categories as $key => $category) {
                    if ($key == 0) {
                        $categoryString .= $category->title;
                    } else {
                        $categoryString .= ', '.$category->title;
                    }
                }
                fputcsv($file,[
                    $user->id, 
                    $user->name, 
                    $user->ip, 
                    date('d.m.Y H:i:s', strtotime($user->create_at)), 
                    $user->network,
                    $user->blocked, 
                    $categoryString
                ]);
            }
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
    }

    /**
     * Save file on server
     * @param  Request $request
     * @return view
     */
    public function imageSave(Request $request)
    {
        //generate file name
        $filename = $request->file('upload')->store('');

        //save
        $request->file('upload')->move(public_path('/uploads/admin/images'), $filename);
        $result = [
            'url' => url('/uploads/admin/images/'.$filename), 
            'value' => public_path('/uploads/admin/images/'.$filename)
        ];

        if ($request->CKEditorFuncNum && $request->CKEditor && $request->langCode) {
            //that handler to upload image CKEditor from Dialog
            $funcNum = $request->CKEditorFuncNum;
            $CKEditor = $request->CKEditor;
            $langCode = $request->langCode;
            $token = $request->ckCsrfToken;
            return view('helper.ckeditor.upload_file', compact('result', 'funcNum', 'CKEditor', 'langCode', 'token'));
        }

        return $result;
    }

    /**
     * Generate charts
     * @return
     */
    public function getUsersCharts()
    {
        $lava = new Lavacharts;
        $charts = [
            [
                'table' => 'users',
                'periods' => [
                    '-1day',
                    '-1week',
                    '-1month',
                    '-1year',
                ],
                'where' => '',
                'title' => 'Number of users',
                'divided_on_users' => 1
            ],
            [
                'table' => 'papers',
                'periods' => [
                    '-1day',
                    '-1week',
                    '-1month',
                    '-1year',
                ],
                'where' => '',
                'title' => 'Number of uploaded publications from Pubmed',
                'divided_on_users' => 1
            ],
            /*[
                'table' => 'user_pin_card',
                'periods' => [
                    '-1day',
                    '-1week',
                    '-1month',
                    '-1year',
                ],
                'where' => ' AND auto = 0'."\n",
                'title' => 'Number of manually pinned publications',
                'divided_on_users' => 1
            ],*/
            [
                'table' => 'user_pin_card',
                'periods' => [
                    '-1day',
                    '-1week',
                    '-1month',
                    '-1year',
                ],
                'where' => ' AND auto = 1'."\n",
                'title' => 'Number of automatically pinned publications',
                'divided_on_users' => 1
            ],
            [
                'table' => 'statistics',
                'periods' => [
                    '-1day',
                    '-1week',
                    '-1month',
                    '-1year',
                ],
                'where' => '',
                'title' => 'Number of total clicks',
                'divided_on_users' => 1
            ],
            [
                'table' => 'statistics',
                'periods' => [
                    '-1day',
                    '-1week',
                    '-1month',
                    '-1year',
                ],
                'where' => ' AND object = "searchBar"'."\n",
                'title' => 'Number of clicks on Search bar',
                'divided_on_users' => 1
            ],
            /*[
                'table' => 'statistics',
                'periods' => [
                    '-1day',
                    '-1week',
                    '-1month',
                    '-1year',
                ],
                'where' => ' AND object = "pinBtn"'."\n",
                'title' => 'Number of clicks on “Pin” button on cards',
                'divided_on_users' => 1
            ],*/
            [
                'table' => 'statistics',
                'periods' => [
                    '-1day',
                    '-1week',
                    '-1month',
                    '-1year',
                ],
                'where' => ' AND object = "publicationAdd"'."\n",
                'title' => 'Number of clicks on all “Add publications” buttons',
                'divided_on_users' => 1
            ],

            [
                'table' => 'statistics',
                'periods' => [
                    '-1day',
                    '-1week',
                    '-1month',
                    '-1year',
                ],
                'where' => ' AND object = "exploreBtn"'."\n",
                'title' => 'Number of clicks on “Explore” button',
                'divided_on_users' => 1
            ],

            [
                'table' => 'statistics',
                'periods' => [
                    '-1day',
                    '-1week',
                    '-1month',
                    '-1year',
                ],
                'where' => ' AND object = "jobsTab"'."\n",
                'title' => 'Number of clicks on “Jobs” tab',
                'divided_on_users' => 1
            ],

            [
                'table' => 'statistics',
                'periods' => [
                    '-1day',
                    '-1week',
                    '-1month',
                    '-1year',
                ],
                'where' => ' AND object = "networkingTab"'."\n",
                'title' => 'Number of clicks on “Networking” tab',
                'divided_on_users' => 1
            ],

            [
                'table' => 'statistics',
                'periods' => [
                    '-1day',
                    '-1week',
                    '-1month',
                    '-1year',
                ],
                'where' => ' AND object = "profile"'."\n",
                'title' => 'Number of visits into own profile',
                'divided_on_users' => 1
            ],

            [
                'table' => 'statistics',
                'periods' => [
                    '-1day',
                    '-1week',
                    '-1month',
                    '-1year',
                ],
                'where' => ' AND object = "survey"'."\n",
                'title' => 'Number of clicks on “Survey” button in user profile',
                'divided_on_users' => 1
            ],
        ];
        $usersCount = User::count();
        foreach ($charts as $key => $chart) {
            foreach ($chart['periods'] as $period) {
                $lavaChart = $lava->DataTable();
                $lavaChart->addDateColumn('Date')
                    ->addNumberColumn('Amount');
                if ($chart['divided_on_users']) {
                    $lavaChart->addNumberColumn('Amount divided on users');
                }
                $result = $this->buildChartForPeriod(
                    $period, 
                    $lavaChart, 
                    $chart['table'],
                    $chart['where'],
                    $chart['divided_on_users'],
                    $usersCount
                );
                $ticks = [];
                if ($result[1] < 10) {
                    $ticks = [0, 5, 10];
                }
                $options = [
                    'title' => '',
                    'vAxis' => [
                        'minValue' => 0,
                    ],
                    'width' => '600'
                ];
                if ($ticks) {
                    $options['vAxis']['ticks'] = $ticks;
                }
                $lava->LineChart($chart['table'].$key.$period, $result[0], $options);
            }
        }
        return View::make('admin.dashboard', ['lava' => $lava, 'charts' => $charts]);
    }

    /**
     * Build chart for period(interval 1 day)
     * @param  string $period
     * @param  lava $chart
     * @param  string $table
     * @param  string $where
     * @param  integer $divided
     * @param  integer $usersCount
     * @return array
     */
    private function buildChartForPeriod($period, $chart, $table, $where = '', 
        $divided = 0, $usersCount = 1) {
        $date1 = date('Y-m-d H:i:s', strtotime($period.' 00:00:00'));
        $date2 = date('Y-m-d').' 23:59:00';
        $arrayPeriod = new \DatePeriod(
             new \DateTime($date1),
             new \DateInterval('P1D'),
             new \DateTime($date2)
        );
        $result = \DB::select('
            SELECT 
                COUNT(id) as amount, 
                FLOOR(UNIX_TIMESTAMP(created_at)/86400) AS timekey
            FROM '.$table.'
                WHERE created_at >= ? AND created_at <= ?'.$where.'
            GROUP BY timekey', [
                $date1,
                $date2
            ]
        );

        $max = 0;
        foreach ($result as $row) {
            if ($row->amount > $max) {
                $max = $row->amount;
            }
        }

        foreach ($arrayPeriod as $date) {
            $amount = 0;
            $currentDate = $date->format('Y-m-d');   
            foreach ($result as $row) {
                if ($currentDate == date('Y-m-d', $row->timekey * 86400)) {
                    $amount = $row->amount;
                    break;
                }
            }
            $data = [
                $currentDate,
                $amount
            ];
            if ($divided) {
                $data[] = $amount / $usersCount;
            }
            $chart->addRow($data); 
        }
        return [$chart, $max];
    }
}