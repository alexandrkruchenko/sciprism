<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use App\Category;
use App\Page;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application index page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::limit(20)->get();
        $slug = 'home';
        if ($request->input('p')) {
            $slug = $request->input('p');
        } 
        $home = Page::where('slug', $slug)->first();
        $data = [
            'slug' => 'home',
            'title' => $home->title,
            'body' => $home->body,
            'meta_title' => $home->meta_title,
            'meta_description' => $home->meta_description,
            'meta_keywords' => $home->meta_keywords,
            'categories' => $categories,
            'amount_categories' => count($categories),
            'orcid_auth' => false,
            'orcid_error' => '',
            'ga_code' => $home->ga_code
        ];
        if ($request->input('auth') == 'orcid') {
            $client = new Client();
            try {
                $response = $client->request('POST', 'https://orcid.org/oauth/token', [
                    'body' => http_build_query([
                        'client_id' => config('settings.orcid_id'),
                        'client_secret' => config('settings.orcid_secret'),
                        'grant_type' => 'authorization_code',
                        'code' => $request->input('code'),
                        'redirect_uri' => url('home?auth=orcid'),
                    ]),
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded'
                    ]
                ]);
                $result = @json_decode($response->getBody());
                $data['orcid_auth'] = true;  
                $data['orcid_data'] = [
                    'name' => $result->name,
                    'access_token' => $result->access_token,
                    'orcid' => $result->orcid
                ];
            } catch (GuzzleException $e) {
                $data['orcid_error'] = 'Error login via ORCID. Try later.';
            }
        }
        return view('static_page', $data);
    }
}
