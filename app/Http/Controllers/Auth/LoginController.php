<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use Facebook\Facebook;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Exceptions\FacebookResponseException;

use Google_Client;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use Image;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/feed';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Scistart login via email process
     * @param  Request $request
     * @return Response 
     */
    public function loginEmail(Request $request)
    {
        $error = '';
        $credentials = $request->only('email', 'password');
        if (!Auth::attempt($credentials, $request->has('remember'))) {
            $error = 'Wrong login or password';
        }
        return response()->json([
            'error' => $error
        ]);
    }

    /**
     * Scistart login process
     * @param  Request $request
     * @return Response 
     */
    public function login(Request $request)
    {
        $error = '';
        $userLogged = false;
        $userData = $request->input('user');
        if (isset($userData['params']['social']) && isset($userData['params']['access_token'])) {
            $user = User::where([
                'network' => $userData['params']['social'],
                'network_user_id' => $userData['id'],
            ])->first();
            if ($user) {
                //get user social data
                switch ($userData['params']['social']) {
                        
                    /*case 'facebook':
                        $fb = new Facebook([
                            'app_id' => config('settings.facebook_app_id'),
                            'app_secret' => config('settings.facebook_app_secret'),
                            'default_graph_version' => 'v2.2',
                        ]);
                        try {
                            $response = $fb->get('/me?fields=id,first_name,last_name,email', $userData['params']['access_token']);
                        } catch(FacebookResponseException $e) {
                            $error = 'Facebook Graph returned an error: ' . $e->getMessage();
                        } catch(FacebookSDKException $e) {
                            $error = 'Facebook SDK returned an error: ' . $e->getMessage();
                        }
                        $me = $response->getGraphUser();
                        $img = 'http://graph.facebook.com/'.$me->getId().
                                '/picture?height=160&width=160';
                        $data = [
                            'name' => $me['first_name'],
                            'last_name' => $me['last_name'],
                            'email' => $me['email']
                        ];
                    break;*/

                    case 'orcid':
                            $client = new Client();
                            $img = '';
                            try {
                                $response = $client->get('https://pub.orcid.org/v2.1/'.
                                    $userData['id'].'/record',
                                    [
                                        'headers' => [
                                            'Accept' => 'application/vnd.orcid+json'
                                        ]
                                    ]
                                );
                                $result = @json_decode($response->getBody());
                                if ($result && $result->person->emails->email && $result->person->name) {
                                    $data = [
                                        'name' => $result->person->name->{'given-names'}->value,
                                        'last_name' => $result->person->name->{'family-name'}->value,
                                        'email' => $result->person->emails->email[0]->email
                                    ];
                                } else {
                                    $error = 'ORCID Access denied. Please, give public access for name and email';
                                }
                            } catch(GuzzleException $e) {
                                $error = 'ORCID API eror.';
                            }
                        break;

                    case 'linkedin':
                        //create http client
                        $client = new Client();
                        try {
                            $result = $client->get('https://api.linkedin.com/v1/people/~:'.
                                '(id,firstName,lastName,picture-url,email-address)?format=json',
                                [
                                    'headers' => [
                                        'oauth_token' => $userData['params']['access_token']
                                    ]
                                ]
                            );
                            $me = @json_decode($result->getBody(), true);
                            if ($me) {
                                $data = [
                                    'name' => $me['firstName'],
                                    'last_name' => $me['lastName'],
                                    'email' => $me['emailAddress']
                                ];
                                if (isset($me['pictureUrl'])) {
                                    $img = $me['pictureUrl'];
                                }
                            }
                        } catch(GuzzleException $e) {
                            $error = 'Linkedin SDK returned an error: ' . $e->getMessage();
                        }
                    break;

                    case 'google':
                        $client = new Google_Client([
                            'client_id' => config('settings.google_client_id')
                        ]);
                        $me = $client->verifyIdToken($userData['params']['access_token']);
                        if ($me) {
                            $img = $me['picture'];
                            $data = [
                                'name' => $me['given_name'],
                                'last_name' => $me['family_name'],
                                'email' => $me['email']
                            ];
                        } else {
                            $error = 'Google SDK returned an error:  Invalid ID token';
                        }
                    break;
                }
                if (empty($error)) {
                    $userLogged = true;
                    if (!$user->blocked) {
                        if (isset($data['network_token'])) {
                            $user->network_token = $data['network_token'];
                        }
                        if (isset($data['name'])) {
                            $user->name = $data['name'];
                        }
                        if (isset($data['last_name'])) {
                            $user->last_name = $data['last_name'];
                        }
                        if (isset($data['email'])) {
                            $user->email = $data['email'];
                        }

                        //save avatar
                        if ($img) {
                            $filename = 'avatar.png';
                            try {
                                $image = Image::make($img)->resize(160, 160);
                            } catch(\Intervention\Image\Exception\NotReadableException $e) {
                                $image = Image::make(public_path('/uploads/default_user.png'))->resize(160, 160);
                            }
                            $userFolder = public_path('uploads/'.$user->id);
                            $avatarPath = $userFolder.'/'.$filename;
                            $avatarUrl = '/uploads/'.$user->id.'/'.$filename;
                            if (!file_exists($userFolder)) {
                                mkdir($userFolder, 0775, true);
                            }
                            $image->save($avatarPath);
                            $user->avatar = $avatarUrl;   
                        }
                        $user->save();
                        $this->authenticated($request, $user);
                        Auth::login($user, true);
                    }
                }
            }
        } else {
            $error = 'social_params_not_setted';
        }
        return response()->json([
            'error' => $error,
            'user_logged' => $userLogged
        ]);
    }

    /**
     * Save IP user when authenticated
     * @param  Request $request
     * @param  User  $user
     * @return void
     */
    public function authenticated(Request $request, $user)
    {
        $user->ip = $request->getClientIp();
        $user->save();
    }

}
