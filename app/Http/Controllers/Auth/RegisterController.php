<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Paper;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use Facebook\Facebook;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Exceptions\FacebookResponseException;

use Google_Client;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use App\Services\PaperService;

use App\Events\UserRegistered;

use Image;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/feed';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $newUser = new User;
        $newUser->name = $data['name'];
        $newUser->last_name = $data['last_name'];
        $newUser->email = $data['email'];
        $newUser->password = bcrypt($data['password']);
        $newUser->network = $data['network'];
        $newUser->network_user_id = $data['network_user_id'];
        $newUser->network_token = $data['network_token'];
        $newUser->ip = $data['ip'];
        $newUser->save();
        $newUser->roles()->sync([2]);

        event(new UserRegistered($newUser));

        return $newUser;
    }

    /**
     * Do email registration
     * @param  array  $data
     * @return Response
     */
    public function registerEmail(Request $request)
    {
        $error = '';
        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users|string',
            'full_name' => 'required|string',
            'password' => 'required|string|min:8',
        ]);

        if ($validator->fails()) {
           $error = $validator->errors()->first();
        } else {
            $data = [
                'name' => $request->input('full_name'),
                'last_name' => '',
                'email' => $request->input('email'),
                'password' => $request->input('password'),
            ];
        }

        if (empty($error)) {
            $img = public_path('/uploads/default_user.png');
            $data['network'] = 'email';
            $data['network_user_id'] = $data['email'];
            $data['network_token'] = '';
            $data['ip'] = $request->getClientIp();

            //create user
            $user = $this->create($data);

            //save avatar
            if ($img) {
                $filename = 'avatar.png';
                $image = Image::make(public_path('/uploads/default_user.png'))
                    ->resize(160, 160);
                $userFolder = public_path('uploads/'.$user->id);
                $avatarPath = $userFolder.'/'.$filename;
                $avatarUrl = '/uploads/'.$user->id.'/'.$filename;
                if (!file_exists($userFolder)) {
                    mkdir($userFolder, 0775, true);
                }
                $image->save($avatarPath);
                $user->avatar = $avatarUrl;
                $user->save();
            }

            Auth::login($user);
        }
        return response()->json([
            'error' => $error
        ]);
    }

    /**
     * Do registration
     * @param  array  $data
     * @return Response
     */
    public function register(Request $request)
    {
        $error = '';
        $userData = $request->input('user');
        $papers = $request->input('papers');
        $interests = $request->input('interests');
        //if (count($interests)) {
            if (
                isset($userData['params']['social']) && 
                isset($userData['params']['access_token'])) {
                $user = User::where([
                    'network' => $userData['params']['social'],
                    'network_user_id' => $userData['id'],
                ])->first();
                if (!$user) {
                    $data = [];
                    $img = '';
                    //get user social data
                    switch ($userData['params']['social']) {
                        
                        /*case 'facebook':
                            $fb = new Facebook([
                                'app_id' => config('settings.facebook_app_id'),
                                'app_secret' => config('settings.facebook_app_secret'),
                                'default_graph_version' => 'v2.2',
                            ]);
                            try {
                                $response = $fb->get('/me?fields=id,first_name,last_name,email', 
                                    $userData['params']['access_token']
                                );
                            } catch(FacebookResponseException $e) {
                                $error = 'Facebook Graph returned an error: ' . $e->getMessage();
                            } catch(FacebookSDKException $e) {
                                $error = 'Facebook SDK returned an error: ' . $e->getMessage();
                            }
                            $me = $response->getGraphUser();
                            $img = 'http://graph.facebook.com/'.$me->getId().
                                    '/picture?height=85&width=85';
                            $data = [
                                'name' => $me['first_name'],
                                'last_name' => $me['last_name'],
                                'email' => $me['email']
                            ];
                        break;*/

                        case 'orcid':
                            $client = new Client();
                            $img = public_path('/uploads/default_user.png');
                            try {
                                $response = $client->get('https://pub.orcid.org/v2.1/'.
                                    $userData['id'].'/record',
                                    [
                                        'headers' => [
                                            'Accept' => 'application/vnd.orcid+json'
                                        ]
                                    ]
                                );
                                $result = @json_decode($response->getBody());
                                if ($result && $result->person->emails->email && $result->person->name) {
                                    $data = [
                                        'name' => $result->person->name->{'given-names'}->value,
                                        'last_name' => $result->person->name->{'family-name'}->value,
                                        'email' => $result->person->emails->email[0]->email
                                    ];
                                } else {
                                    $error = 'ORCID Access denied. Please, give public access for name and email';
                                }
                            } catch(GuzzleException $e) {
                                $error = 'ORCID API eror.';
                            }
                        break;

                        case 'linkedin':
                            //create http client
                            $client = new Client();
                            try {
                                $result = $client->get('https://api.linkedin.com/v1/people/~:'.
                                    '(id,firstName,lastName,picture-url,email-address)?format=json',
                                    [
                                        'headers' => [
                                            'oauth_token' => $userData['params']['access_token']
                                        ]
                                    ]
                                );
                                $me = @json_decode($result->getBody(), true);
                                if ($me) {
                                    $data = [
                                        'name' => $me['firstName'],
                                        'last_name' => $me['lastName'],
                                        'email' => $me['emailAddress']
                                    ];
                                    if (isset($me['pictureUrl'])) {
                                        $img = $me['pictureUrl'];
                                    }
                                }
                            } catch(GuzzleException $e) {
                                $error = 'Linkedin SDK returned an error: ' . $e->getMessage();
                            }
                        break;

                        case 'google':
                            $client = new Google_Client([
                                'client_id' => config('settings.google_client_id')
                            ]);
                            $me = $client->verifyIdToken($userData['params']['access_token']);
                            if ($me) {
                                $img = $me['picture'];
                                $data = [
                                    'name' => $me['given_name'],
                                    'last_name' => $me['family_name'],
                                    'email' => $me['email']
                                ];
                            } else {
                                $error = 'Google SDK returned an error:  Invalid ID token';
                            }
                        break;
                    }
                    if (empty($error)) {
                        $data['network'] = $userData['params']['social'];
                        $data['network_user_id'] = $userData['id'];
                        $data['network_token'] = $userData['params']['access_token'];
                        $data['password'] = Hash::make(str_random(8));
                        $data['ip'] = $request->getClientIp();

                        //create user
                        $user = $this->create($data);

                        //save avatar
                        if ($img) {
                            $filename = 'avatar.png';
                            try {
                                $image = Image::make($img)->resize(160, 160);
                            } catch(\Intervention\Image\Exception\NotReadableException $e) {
                                $image = Image::make(public_path('/uploads/default_user.png'))->resize(160, 160);
                            }
                            $userFolder = public_path('uploads/'.$user->id);
                            $avatarPath = $userFolder.'/'.$filename;
                            $avatarUrl = '/uploads/'.$user->id.'/'.$filename;
                            if (!file_exists($userFolder)) {
                                mkdir($userFolder, 0775, true);
                            }
                            $image->save($avatarPath);
                            $user->avatar = $avatarUrl;
                            $user->save();
                        }

                        //set user interests
                        //$user->categories()->sync($interests);

                        if (count($papers)) {

                            $papersIds = PaperService::savePumbmedPapers($papers);

                            if (count($papersIds)) {
                                //set user papers
                                $user->papers()->sync($papersIds);
                            }
                        }

                        Auth::login($user, true);
                        $request->session()->flash('welcome_message', true);
                    }
                } else {
                    $error = 'user_already_registered';
                }
            } else {
                $error = 'social_params_not_setted';
            }
        /*} else {
            $error = 'user_must_have_interests';
        }*/

        return response()->json([
            'error' => $error
        ]);
    }
}
