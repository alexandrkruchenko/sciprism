<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use TeamTNT\TNTSearch\Classifier\TNTClassifier;

class NeuralNetworkTrainingController extends Controller
{
    /**
     * Start process neural network training
     * @return void
     */
    public function doTraining()
    {
        $classifier = new TNTClassifier();
        /*$classifier->learn("A great game", "Sports");
        $classifier->learn("The election was over", "Not sports");
        $classifier->learn("Very clean match", "Sports");
        $classifier->learn("A clean but forgettable game", "Sports");

        $guess = $classifier->predict("It was a close election");
        var_dump($guess['label']); //returns "Not sports"*/
        if (($handle = fopen(public_path('articles.csv'), 'r')) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
                $classifier->learn($data[1], $data[0]);
            }
            fclose($handle);
        }
        $classifier->save(public_path('data.cls'));
    }


    public function test()
    {
        return view('neuraltesting');
    }

    public function predict(Request $request)
    {
        $this->validate($request,[
            'article' => 'required|string'
        ]);

        $classifier = new TNTClassifier();
        $classifier->load(public_path('data.cls'));
        $guess = $classifier->predict($request->input('article'));
        return redirect()->action('NeuralNetworkTrainingController@test')
            ->with('category', $guess['label']);

    }

    public function convert()
    {
        $category = 'Bioengineering';
        $textArray = file(public_path('articles.txt'));
        $rowsCount = count($textArray);
        $csv = '';
        $csvArray = [];
        $count = 1;
        for ($i = 0; $i < $rowsCount; $i++) {
            if ($count == 4) {
                $count = 1;
            }
            $textArray[$i] = trim($textArray[$i]);
            if ($textArray[$i] != '' && $count != 1) {
                $csv .= $category.';'.$textArray[$i]."\n";
                $csvArray[] = [
                    $category,
                    $textArray[$i]
                ];
            }
            $count++;
        }
        print_r($csvArray);
        //file_put_contents(public_path('articles.csv'), $csv);
    }
}
