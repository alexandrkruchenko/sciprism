<?php

namespace App\Http\Controllers;

use App\Paper;
use App\PaperUser;
use App\Card;
use App\UserPinCard;
use App\Follow;
use App\PaperFollow;
use App\User;
use App\Comment;
use App\PaperComment;
use App\Expertise;
use App\UserSetting;
use App\Event;
use App\Job;
use App\UserNotification;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use App\Services\PaperService;
use App\Services\AjaxService;
use App\Services\CardService;
use App\Services\EventbriteService;
use App\Services\LinkUpService;

use App\Events\ImpactAreaReady;

use Validator;
use Image;
use stdClass;

class AjaxController extends Controller
{
    
    /**
     * Get paper from Pubmed for sign page by author, ID or DOI
     * @param  Request $request
     * @return 
     */
    public function getPubmedPaperSign(Request $request)
    {
        $offset = intval($request->input('offset'));
        $offset = $offset ? $offset : 0;
        $limit  = 10;
        $q = htmlspecialchars($request->input('q'));
        $html = '';
        $result = PaperService::getPubmedPaperByCreteria($q, $offset, $limit);
        if ($result && $result['xml']) {
            $xml = $result['xml'];
            $papersCount = $result['count'];
            $html = '<div class="tab-papers"><form><div class="wrap-list">';
            foreach ($xml->children() as $doc) {
                $paperTitile = '';
                $date = '';
                $journal = '';
                $paperAuthors = [];
                $docParams = $doc->children();
                foreach ($docParams->Item as $item) {
                    $name = (string)$item['Name'];
                    if ($name == 'Title') {
                        $paperTitile = (string)$item;
                    }
                    if ($name == 'AuthorList') {
                        foreach ($item->children() as $children) {
                            $paperAuthors[] = (string)$children;
                        }
                    }
                    if ($name == 'PubDate') {
                        $date = (string)$item;
                    }
                    if ($name == 'FullJournalName') {
                        $journal = (string)$item;
                    }
                }
                $authors = '';
                $countAuthors = count($paperAuthors);
                if ($countAuthors > 2) {
                    $authors = reset($paperAuthors).'...'.end($paperAuthors);
                } else if($countAuthors == 1) {
                    $authors = $paperAuthors[0];
                }
                $authorsString = 
                $html .= '<div class="form-group">'.
                            '<input type="checkbox" id="title-'.$doc->Id.
                                '" name=papers['.$doc->Id.'] class="sign-papers">';
                $html .=    '<label for="title-'.$doc->Id.'">'.
                                '<span class="check">'.
                                    '<img src="/img/check.svg" alt="">'.
                                '</span>';
                $html .=        '<span class="title">'.$paperTitile.'</span>';
                $html .=        '<span class="text">'.$authors.'.</span>';
                $html .=        '<span class="text">'.($journal ? $journal.'.' : '').
                    ' '.($date ? $date.'.' : '').'</span>';
                $html .=     '</label>';
                $html .= '</div>';
            }
            $html .= '</div>';

            //build pagination
            $html .= AjaxService::buildAjaxPagination($papersCount, $offset, $limit);
            
            $html .= '</form></div>';
        }
        return response()->json(['html' => $html]);
    }

    /**
     * Get Pubmed paper for user by author, ID or DOI
     * @param  Request $request
     * @return 
     */
    public function getPubmedPaper(Request $request)
    {
        $offset = intval($request->input('offset'));
        $offset = $offset ? $offset : 0;
        $limit  = 10;
        $q = htmlspecialchars($request->input('q'));
        $html = '';
        $result = PaperService::getPubmedPaperByCreteria($q, $offset, $limit);
        if ($result && $result['xml']) {
            $xml = $result['xml'];
            $papersCount = $result['count'];
            $html = '<div class="popup-search-results"><table><tbody>';
            foreach ($xml->children() as $doc) {
                $paperTitile = '';
                $date = '';
                $journal = '';
                $paperAuthors = [];
                $docParams = $doc->children();
                foreach ($docParams->Item as $item) {
                    $name = (string)$item['Name'];
                    if ($name == 'Title') {
                        $paperTitile = (string)$item;
                    }
                    if ($name == 'AuthorList') {
                        $count = 1;
                        foreach ($item->children() as $children) {
                            if ($count > 3) {
                                break;
                            }
                            $paperAuthors[] = (string)$children;
                            $count++;
                        }
                    }
                }
                $html .= '<tr>'.
                            '<td>'.
                                '<input class="search-result-papers"'.
                                    ' type="checkbox" id="ch-pinned-'.$doc->Id.'"'.
                                'name=papers['.$doc->Id.']>'.
                                '<label class="checkbox-pinned" for="ch-pinned-'.$doc->Id.'">'.
                                    '<i class="fas fa-check-circle"></i>'.
                                '</label>'.
                            '</td>'.
                            '<td>'.
                                '<div class="icon">'.
                                    '<img src="/img/file-2.svg" alt="">'.
                                '</div>'.
                                '<div class="text">'.
                                    '<h5>'.$paperTitile.'</h5>'.
                                    '<p>'.implode(', ', $paperAuthors).'</p>'.
                                '</div>'.
                            '</td>'.
                         '</tr>';
            }
            $html .= '</tbody></table>';

            //build pagination
            $html .= AjaxService::buildAjaxPagination($papersCount, $offset, $limit);

            $html .= '<button class="add">Add</button></div>';
        }
        return response()->json(['html' => $html]);
    }

    /**
     * Get user publications for pinned function
     * @param Request $request
     * @return
     */
    public function getUserCardPapers(Request $request)
    {
        $offset = intval($request->input('offset'));
        $cardId = intval($request->input('card_id'));
        $offset = $offset ? $offset : 0;
        $limit = 10;
        $html = '';
        $papers = Auth::user()->papers()
            ->offset($offset)
            ->limit($limit)
            ->get();
        $papersCount = Auth::user()->papers()->count();
        $html .= '<table>';
        foreach ($papers as $paper) {
            $pinned = UserPinCard::where('paper_id', $paper->id)
                ->where('card_id', $cardId)
                ->where('user_id', Auth::user()->id)
                ->first();
            $html .= '<tr>'.
                        '<td>'.
                            '<div class="icon">'.
                                '<img src="/img/file-2.svg" alt="">'.
                            '</div>'.
                            '<div class="text">'.
                                '<h5>'.$paper->title.'</h5>'.
                                '<p>'.$paper->getPaperDescription().'</p>'.
                            '</div>'.
                        '</td>'.
                        '<td>'.
                            '<input class="pin-checkbox" type="checkbox"'.
                                ' id="ch-pinned-'.$paper->id.'" '.($pinned ? 'checked=""' : '').'>'.
                            '<label class="checkbox-pinned" for="ch-pinned-'.$paper->id.'">'.
                                '<i class="fas fa-check-circle"></i>'.
                                '<span></span>'.
                            '</label>'.
                        '</td>'.
                    '</tr>';
        }
        $html .= '</table>';
        $html .= AjaxService::buildAjaxPagination($papersCount, $offset, $limit);
        return response()->json(['html' => $html]);
    }

    /**
     * Get user publications for pinned function
     * @param Request $request
     * @return
     */
    public function getUserPapers(Request $request)
    {
        $offset = intval($request->input('offset'));
        $offset = $offset ? $offset : 0;
        $limit = 8;
        $html = '';
        $papers = Auth::user()->papers()
            ->offset($offset)
            ->limit($limit)
            ->get();
        $papersCount = Auth::user()->papers()->count();
        foreach ($papers as $paper) {
            $html .= '<div class="form-group">
                        <input type="checkbox" id="paper-'.$paper->id.
                            '" name="papers['.$paper->id.']" checked="">
                        <label for="paper-'.$paper->id.'">
                            <span class="check">
                                <img src="/img/check.svg" alt="">
                            </span>
                            <span class="title">'.$paper->title.'</span>
                            <span class="text">'.$paper->getPaperDescription().'</span>
                        </label>
                    </div>';
        }
        $html .= AjaxService::buildAjaxPagination($papersCount, $offset, $limit);
        return response()->json(['html' => $html]);
    }

    /**
     * Add pin to card
     * @param Request $request
     * @return
     */
    public function addPinToCard(Request $request)
    {
        $this->middleware('auth');
        $paperId = intval($request->input('paper_id'));
        $cardId = intval($request->input('card_id'));
        $pinned = UserPinCard::where('card_id', $cardId)
            ->where('paper_id', $paperId)
            ->where('user_id', Auth::user()->id)
            ->first();
        if ($pinned) {
            $pinned->delete(); 
        } else {
            if (UserPinCard::where('user_id', Auth::user()->id)->count() == 0) {
                event(new ImpactAreaReady(Auth::user()));
            }
            $pin = new UserPinCard;
            $pin->user_id = Auth::user()->id;
            $pin->paper_id = $paperId;
            $pin->card_id = $cardId;
            $pin->save();
        }
    }

    /**
     * Add card to follow
     * @param Request $request
     * @return
     */
    public function addToFollow(Request $request)
    {
        $this->middleware('auth');
        $objectId = intval($request->input('object_id'));
        $type = $request->input('type') == 'card' ? 'card' : 'paper';
        if ($type == 'card') {
            $followed = Follow::where('card_id', $objectId)
                ->where('user_id', Auth::user()->id)
                ->first();
        } else {
            $followed = PaperFollow::where('paper_id', $objectId)
                ->where('user_id', Auth::user()->id)
                ->first();
        }
        if ($followed) {
            $followed->delete();
        } else {
            $follow = $type == 'card' ? new Follow : new PaperFollow;
            $follow->user_id = Auth::user()->id;
            $follow->{$type.'_id'} = $objectId;
            $follow->save();
        }
    }

    /**
     * Get user revelant cards for main page
     * @param Request $request
     * @return
     */
    public function getFeedCards(Request $request)
    {
        $offset = intval($request->input('offset'));
        $type = $request->input('type');
        $categoryId = $request->input('category_id');
        $q = $request->input('q');
        $offset = $offset ? $offset : 0;
        $limit  = 10;
        $userId = Auth::user() ? Auth::user()->id : 0;
        $cards = [];
        $isSearch = false;
        /*switch($type) {
            case 'relevant':
                $cards = Card::getRelevantCards($userId, $categoryId, $offset, $limit);
            break;

            case 'recent':
                $cards = Card::getRecentCards($userId, $categoryId, $offset, $limit, $q);
            break;

            case 'popular':
                $cards = Card::getPopularCards($userId, $categoryId, $offset, $limit);
            break;

            case 'following':
                if ($userId) { 
                    $cards = Card::getFollowingCards($userId, $offset, $limit);
                }
            break;

        }*/
        if ($type == 'recent') {
            $cards = Card::offset($offset)->limit($limit);
            if ($categoryId || $q) {
                $isSearch = true;
                if ($categoryId) {
                    $cards = $cards->whereHas('categories', function($query) use($categoryId){
                         $query->where('categories.id', $categoryId);
                    });
                }
                if ($q) {
                    $cards->whereRaw('MATCH (cards.title, cards.short_description,'. 
                        ' cards.description, cards.owner)'.
                        ' AGAINST (? IN BOOLEAN MODE) OR cards.title LIKE ? OR '.
                        'cards.short_description LIKE ? OR cards.description LIKE ?', 
                        [$q, '%'.$q.'%', '%'.$q.'%', '%'.$q.'%']);
                }
                $cards = $cards->inRandomOrder();
            } else {
                $cards = $cards->orderBy('created_at', 'desc');
                
            }
        } else {
            $cards = Card::whereHas('followers', function ($query) {
                $query->where('users.id', Auth::user()->id);
            })->orderBy('created_at', 'desc');
        }
        $count = $cards->count();
        $cards = $cards->offset($offset)->limit($limit)->get();
        $html = CardService::buildFeedHtml($cards);
        $charts = CardService::getImpact($cards);
        return response()->json([
            'html' => $html,
            'charts' => $charts,
            'offset' => count($cards) ? $offset + $limit : -1,
            'count' => $count
        ]);
    }

    /**
     * Save user papers
     * @param Request $request
     */
    public function addUserPapers(Request $request)
    {
        $this->middleware('auth');
        $papers = $request->input('papers');
        $noflash = $request->input('noflash');
        if (count($papers)) {

            $papersIds = PaperService::savePumbmedPapers($papers);

            if (count($papersIds)) {
                //set user papers
                Auth::user()->papers()->syncWithoutDetaching($papersIds);
                if (!$noflash) {
                    $request->session()
                        ->flash(
                            'success', 
                            '<b>Done!</b> Publications are successfully added.'
                        );
                } else {
                    $request->session()->flash('welcome_message', true);
                }
            }
        }
    }

    /**
     * Save user img on server via Ajax
     * @param  Request $request
     * @return
     */
    public function saveUserImg(Request $request)
    {
        $this->middleware('auth');
        $img = '';
        $validator = Validator::make($request->all(), [
            'image' => 'required|image|max:801',
        ]);
        if (!$validator->fails()) {
            $filename = md5(Auth::user()->id.time()).'.png';
            
            //save file
            Image::make($request->file('image')->getRealPath())
                ->resize(160, 160)
                ->save(public_path('/uploads/'.Auth::user()->id.'/'.$filename));
            $img = '/uploads/'.Auth::user()->id.'/'.$filename;
        }
        return response()->json([
            'img' => $img
        ]);
    }

    /**
     * Search and show results
     * @param Request $request
     * @return
     */
    public function searchDataBySearchBar(Request $request)
    {
        $q = htmlspecialchars($request->input('q'));
        $html = '';
        if ($q) {
            $members = User::WhereRaw('MATCH(name) AGAINST (\''.$q.'\' IN BOOLEAN MODE)')
                ->orWhere('name', 'like', '%'.$q.'%')
                ->orWhere('last_name', 'like', '%'.$q.'%')
                ->get();
            $topics = Card::getRecentCards(0, 0, 0, 6, $q);
            if ($topicCount = count($topics)) {
                $html = '<div class="item"><h4>Topics</h4><ul class="list-topics">';
                foreach ($topics as $key => $topic) {
                    if ($key == 5) {
                        break;
                    }
                    $html .= '<li>
                                <div class="image">
                                    <a href="#card-id-'.$topic->id.'" data-toggle="card-page">
                                        <img src="'.$topic->image.'" alt="">
                                    </a>
                                </div>
                                <div class="text">
                                    <a href="#card-id-'.$topic->id.'" class="title" data-toggle="card-page">'.$topic->title.'</a>
                                    <p>'.$topic->short_description.'</p>
                                </div>
                            </li>';
                }
                $html .= '</div>';
                if ($topicCount > 0) {
                    $html .= '<a href="/?search='.urlencode($q).
                        '" class="all-search">Show all results</a>';
                }
            }
            if (count($members)) {
                $html = '<div class="item"><h4>Members</h4><ul class="members-list">';
                foreach ($members as $member) {
                    $html .= '<li>
                                <div class="image">
                                    <a href="'.$member->getUserLink().'">
                                        <img src="'.$member->avatar.'" alt="">
                                    </a>
                                </div>
                                <a href="'.$member->getUserLink().'" class="name">'.
                                    $member->getUserFullName().'</a>
                            </li>';
                }
                $html .= '</div>';
            }
        }
        return response()->json([
            'html' => $html
        ]);
    }

    /**
     * Load card data
     * @param  Request $request
     * @return Response
     */
    public function getCard(Request $request)
    {
        $cardId = $request->input('card_id');
        $card = Card::find($cardId);
        $relatedCards = $card->getRelated();
        if (Auth::user()) {
            $comments = $card->comments()
                ->where(function ($query) {
                    $query->where('private', 0)
                    ->orWhere(function ($query) {
                        $query->where('private', 1)
                        ->where('user_id', Auth::user()->id);
                    });
                })
                ->limit(5)
                ->orderBy('created_at', 'desc')->get();
        } else {
            $comments = $card->comments()
                ->where('private', 0)
                ->orderBy('created_at', 'desc')
                ->limit(5)
                ->get();
        }
        $commentsCount = $card->comments()
            ->where(function ($query) {
                $query->where('private', 0)
                ->orWhere(function ($query) {
                    $query->where('private', 1)
                    ->where('user_id', Auth::user()->id);
                });
            })->count();
        $publications = $card->pinsPaper()->limit(10)->orderBy('created_at', 'desc')->get();
        $publicationsCount = $card->pinsPaper()->count();
        $isFollow = $card->isFollowed(Auth::user()->id);
        if (Auth::user()) {
            $pinsCount = UserPinCard::where('card_id', $card->id)
                ->where('user_id', Auth::user()->id)
                ->count();
        } else {
            $pinsCount = UserPinCard::where('card_id', $card->id)
                ->count();
        }
        $categories = $card->categories->implode('title', ', ');
        if (!$categories) {
            $categories = 'Uncategorized';
        }
        $charts = CardService::getImpact([$card], 6);
        $chart = $charts[0];
        $html = '<div class="left">
                    <div class="item-card">
                        <div class="row">
                            <div class="image">'.
                                /*<a href="javascript:void(0);"> 
                                    <img src="'.($card->image ? url($card->image) : '').'" alt="">
                                </a>*/
                            '</div>
                            <div class="info">
                                <div class="wr-left">
                                    <a href="javascript:void(0);" class="title">'.$card->title.'</a>
                                    <p>'.$card->short_description.'</p>
                                    <ul class="tech">
                                        <li>
                                            <div class="icon">
                                                <img src="/img/diagnostic.png" alt="">
                                            </div>
                                            <a>'.
                                                $categories.'</a>
                                        </li>
                                        <li>
                                            <div class="icon"><img src="/img/university.png" alt=""></div>
                                            <a>'.$card->owner.'</a>
                                        </li>                           
                                    </ul>
                                </div>
                                <div class="wr-right">'.($card->source_link ? 
                                    '<a href="'.$card->source_link.'" target="_blank" class="view_pubmed btn">
                                        View on Source
                                    </a>' : ''                               
                                    ).'<div class="right-btn">
                                        <a href="" class="upload"></a>
                                        <a href="" class="follow'.
                                            ($isFollow ? ' active' : '').'" data-card-id="'.
                                            $card->id.'"></a>  
                                    </div>                                  
                                </div>
                            </div>
                            <div class="row-impact">
                                <div class="side--impact">
                                    <div class="wr-chart-side">'.
                                    ($chart['data'][0] ? 
                                        '<div class="title">This innovation\'s impact</div>
                                        <div class="wr-chart">   
                                               <a href="">
                                                   <div class="chart-semi-card  chart-semi" style="min-width: 195px; height: 190px; max-width: 600px; margin: 0 auto"></div>
                                                   <span>Impact</span>
                                               </a>
                                            </div>' : '').  
                                    '</div>';
        if ($chart['data'][0]) {
            $html .= '<ul class="percent-ul">';
            foreach ($chart['data'][0] as $index => $item) {
                $html .= '<li class="percent'.($index + 1).'">
                            <span class="percent" style="background-color:'
                                .$chart['data'][1][$index].'">'.$item[1].'</span>
                            <span class="text">'.$item[0].'</span>
                        </li>';
            }                                                                 
            $html .= '</ul>'; 
        }                            
                                    
        $html .= '</div>              
                            </div>
                        </div>
                    </div>  
                    <ul class="linkTab-card">
                        <li><a href="#" class="active" data-tab="card-overview">Overview</a></li>
                        <li><a href="#" 
                        '.(Auth::user() ? 'data-tab="card-comments"' : 
                                'onClick="$(\'.sing-up.btn\').trigger(\'click\');  return false;"').'
                        >Comments (<span id="commentsCount">'
                            .$commentsCount.'</span>)</a></li>';
        if ($publicationsCount) {
            $html .= '<li><a href="#"
            '.(Auth::user() ? 'data-tab="card-publications"' : 
                                'onClick="$(\'.sing-up.btn\').trigger(\'click\'); return false;"').'
            >Publications ('.
                            $publicationsCount.')</a></li>';
        }
        $html .= '</ul>
                    <div class="tabs-card">
                        <div class="overview-tab tab active" id="card-overview">
                            <p>'.$card->description.'</p>
                        </div>';
        if (Auth::user()) {
            $html .= '<div class="card-comments tab" id="card-comments">
                            <form id="add_card_comment" action="/ajax/addCardComment" method="post">
                                <input type="hidden" name="card_id" value="'.$card->id.'">
                                <textarea placeholder="Your comment..." name="comment"></textarea>
                                <button class="add">Add comment</button>
                                <div class="checkbox">
                                    <input type="checkbox" id="comment-private" name="private">
                                    <label for="comment-private">
                                        <span class="check"></span>
                                        <span class="text">Make private</span>
                                    </label>
                                </div>
                            </form>';
            if ($commentsCount) {
                $html .= '<ul class="comments-list">';
                foreach ($comments as $comment) {
                    $html .= CardService::renderComment($comment);
                }
                $html .= '</ul>';
                if ($commentsCount > 5) {
                    $html .= '<div class="more-wrapper">
                                <a href="#" class="button more comments" data-offset="5" data-id="'.
                                    $card->id.'">Show more</a>
                            </div>';
                }
            }
            $html .= '      </div>';
            if ($publicationsCount) {
                $html .= '   <div class="card-publications tab" id="card-publications"><ul class="list">';
                foreach ($publications as $publication) {
                    $html .= PaperService::renderPublication($publication);
                }
                if ($publicationsCount > 10) {
                    $html .= AjaxService::buildAjaxPagination($publicationsCount, 0, 10);
                }
                $html .= '</ul></div>';
            }
        }
        $html .= '  </div>      
                </div>';
        if ($relatedCards) {
            $html .= '<div class="releter-topics">
                        <h4>Related innovations</h4>';
            foreach ($relatedCards as $card) {
                $image = $card->image ? url($card->image) : '';
                $html .= '<div class="item">
                            <div class="image">'.
                                /*<a href="/innovation/'.$card->id.'" class="related" data-toggle="card-page"> 
                                    <img src="'.$image.'" alt="">
                                </a>*/
                            '</div>
                            <div class="text">
                                <a href="/innovation/'.$card->id.'" class="related" data-toggle="card-page">'.$card->title.'</a>
                                <p>'.CardService::substrwords($card->short_description, 75).'</p>
                            </div>
                        </div>';
            }
                        
        }
        return response()->json([
            'html' => $html,
            'chart' => $chart
        ]);
    }

    /**
     * Load paper data
     * @param  Request $request
     * @return Response
     */
    public function getPaper(Request $request)
    {
        $paperPubmedId = $request->input('paper_pubmed_id');
        $paper = Paper::where('external_id', $paperPubmedId)->first();
        $relatedPapers = $paper->getRelated();
        if (Auth::user()) {
            $comments = $paper->comments()
                ->where(function ($query) {
                    $query->where('private', 0)
                    ->orWhere(function ($query) {
                        $query->where('private', 1)
                        ->where('user_id', Auth::user()->id);
                    });
                })
                ->limit(5)
                ->orderBy('created_at', 'desc')->get();
        } else {
            $comments = $paper->comments()
                ->where('private', 0)
                ->orderBy('created_at', 'desc')
                ->limit(5)
                ->get();
        }
        $commentsCount = $paper->comments()
            ->where(function ($query) {
                $query->where('private', 0)
                ->orWhere(function ($query) {
                    $query->where('private', 1)
                    ->where('user_id', Auth::user()->id);
                });
            })->count();
        $pins = $paper->pinnedCard()->limit(5)->orderBy('created_at', 'desc')->get();
        $pinsCount = $paper->pinnedCard()->count();
        $isFollow = PaperFollow::where('paper_id', $paper->id)->first();
        $categories = $paper->categories->implode('title', ', ');
        if (!$categories) {
            $categories = 'Uncategorized';
        }
        $charts = PaperService::getImpact([$paper], 6);
        $chart = $charts[0];
        $authors = explode(',', $paper->getPubmedAuthors());
        $author = '';
        if (count($authors) > 1) {
            $author = reset($authors).'...'.end($authors);
        } else {
            $author = $authors[0];
        }
        $html = '<div class="left">
                    <div class="item-card">
                        <div class="row">
                            <div class="image">'.
                                /*<a href="javascript:void(0);"> 
                                    <img src="'.($card->image ? url($card->image) : '').'" alt="">
                                </a>*/
                            '</div>
                            <div class="info">
                                <div class="wr-left">
                                    <a href="javascript:void(0);" class="title">'.$paper->title.'</a>
                                    <p>'.CardService::substrwords($paper->description, 75).'</p>
                                    <ul class="tech">
                                        <li>
                                            <a>'.$author.'</a>
                                        </li>                               
                                    </ul>
                                    <ul class="tech">
                                        <li>
                                            <div class="icon">
                                                <img src="/img/diagnostic.png" alt="">
                                            </div>
                                            <a>'.
                                                $categories.'</a>
                                        </li>                          
                                    </ul>
                                </div>
                                <div class="wr-right">'.
                                    '<a href="'.$paper->getPubmedLink().'" target="_blank" class="view_pubmed btn">
                                        View on PubMed
                                    </a>'.'<div class="right-btn">
                                        <a href="" class="upload"></a>
                                        <a href="" class="follow'.
                                            ($isFollow ? ' active' : '').'" data-paper-id="'.
                                            $paper->id.'"></a>  
                                    </div>                                  
                                </div>
                            </div>
                            <div class="row-impact">
                                <div class="side--impact">
                                    <div class="wr-chart-side">'.
                                        ($chart['data'] ? 
                                        '<div class="title">This paper\'s impact</div>
                                            <div class="wr-chart">   
                                               <a href="">
                                                   <div class="chart-semi-paper  chart-semi" style="min-width: 195px; height: 190px; max-width: 600px; margin: 0 auto"></div>
                                                   <span>Impact</span>
                                               </a>
                                            </div> ': ''). 
                                    '</div>';
        if ($chart['data'][0]) {
            $html .= '<ul class="percent-ul">';
            foreach ($chart['data'][0] as $index => $item) {
                $html .= '<li class="percent'.($index + 1).'">
                            <span class="percent" style="background-color:'.
                                $chart['data'][1][$index].'">'.$item[1].'</span>
                            <span class="text">'.$item[0].'</span>
                        </li>';
            }                                                                 
            $html .= '</ul>'; 
        }                            
                                    
        $html .= '</div>              
                            </div>
                        </div>
                    </div>  
                    <ul class="linkTab-card">
                        <li><a href="#" class="active" data-tab="card-overview">Overview</a></li>
                        <li><a href="#" 
                        '.(Auth::user() ? 'data-tab="card-comments"' : 
                                'onClick="$(\'.sing-up.btn\').trigger(\'click\');  return false;"').'
                        >Comments (<span id="commentsCount">'
                            .$commentsCount.'</span>)</a></li>';
        if ($pinsCount) {
            $html .= '<li><a href="#"
            '.(Auth::user() ? 'data-tab="card-publications"' : 
                                'onClick="$(\'.sing-up.btn\').trigger(\'click\'); return false;"').'
            >Pins ('.
                            $pinsCount.')</a></li>';
        }
        $html .= '</ul>
                    <div class="tabs-card">
                        <div class="overview-tab tab active" id="card-overview">
                            <p>'.$paper->description.'</p>
                        </div>';
        if (Auth::user()) {
            $html .= '<div class="card-comments tab" id="card-comments">
                            <form id="add_card_comment" action="/ajax/addCardComment" method="post">
                                <input type="hidden" name="card_id" value="'.$paper->id.'">
                                <textarea placeholder="Your comment..." name="comment"></textarea>
                                <button class="add">Add comment</button>
                                <div class="checkbox">
                                    <input type="checkbox" id="comment-private" name="private">
                                    <label for="comment-private">
                                        <span class="check"></span>
                                        <span class="text">Make private</span>
                                    </label>
                                </div>
                            </form>';
            if ($commentsCount) {
                $html .= '<ul class="comments-list">';
                foreach ($comments as $comment) {
                    $html .= PaperService::renderComment($comment);
                }
                $html .= '</ul>';
                if ($commentsCount > 5) {
                    $html .= '<div class="more-wrapper">
                                <a href="#" class="button more comments" data-offset="5" data-id="'.$paper->id.'">Show more</a>
                            </div>';
                }
            }
            $html .= '      </div>';
            if ($pinsCount) {
                $html .= '   <div class="card-publications tab" id="card-publications"><ul class="list">';
                foreach ($pins as $pin) {
                    $html .= CardService::renderCard($pin);
                }
                if ($pinsCount > 10) {
                    $html .= AjaxService::buildAjaxPagination($pinsCount, 0, 10);
                }
                $html .= '</ul></div>';
            }
        }
        $html .= '  </div>      
                </div>';
        if ($relatedPapers) {
            $html .= '<div class="releter-topics">
                        <h4>Related publications</h4>';
            foreach ($relatedPapers as $paper) {
                $html .= '<div class="item">
                            <div class="image">'.
                            '</div>
                            <div class="text">
                                <a href="/paper/'.$paper->external_id.'" class="related" data-toggle="card-page-p">'.$paper->title.'</a>
                                <p>'.CardService::substrwords($paper->description, 75).'</p>
                            </div>
                        </div>';
            }
                        
        }
        return response()->json([
            'html' => $html,
            'chart' => $chart
        ]);
    }

    /**
     * Add card comment
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function addCardComment(Request $request)
    {
        $this->middleware('auth');
        $objectId = $request->input('card_id');
        $text = htmlspecialchars($request->input('comment'));
        $private = $request->input('private') == 'true' ? 1 : 0;
        $type = $request->input('type') == 'card' ? 'card' : 'paper';
        if ($type == 'card') {
            $comment = new Comment;
            $comment->card_id = $objectId;
        } else {
            $comment = new PaperComment;
            $comment->paper_id = $objectId;
        }
        $comment->user_id = Auth::user()->id;
        $comment->text = $text;
        $comment->private = $private;
        $comment->save();
        return response()->json([
            'html' => $type == 'card' ? 
                CardService::renderComment($comment) : PaperService::renderComment($comment)
        ]);
    }

    /**
     * Get more comments
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function moreComments(Request $request)
    {
        $this->middleware('auth');
        $offset = intval($request->input('offset'));
        $offset = $offset ? $offset : 0;
        $limit = 5;
        $objectId = intval($request->input('id'));
        $type = $request->input('type') == 'card' ? 'card' : 'paper';
        $html = '';
        if ($type == 'card') {
            $intance = Comment::where('card_id', $objectId);
        } else {
            $intance = PaperComment::where('paper_id', $objectId);
        }
        $comments = $intance
            ->where(function ($query) {
                $query->where('private', 0)
                ->orWhere(function ($query) {
                    $query->where('private', 1)
                    ->where('user_id', Auth::user()->id);
                });
            })
            ->limit($limit)
            ->offset($offset)
            ->orderBy('created_at', 'desc')->get();
        foreach ($comments as $comment) {
            $html .= $type == 'card' ? 
                CardService::renderComment($comment) : PaperService::renderComment($comment);
        }
        $offset = $offset + $limit;
        $commentsCount = count(
            $intance->where(function ($query) {
                $query->where('private', 0)
                ->orWhere(function ($query) {
                    $query->where('private', 1)
                    ->where('user_id', Auth::user()->id);
                });
            })
            ->limit($limit)
            ->offset($offset)
            ->get());
        if (!$commentsCount) {
            $offset = - 1;
        }
        return response()->json([
            'html' => $html,
            'offset' => $offset
        ]);
    }

    /**
     * Get more publications
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function moreArticlesForCardPage(Request $request)
    {
        $this->middleware('auth');
        $offset = intval($request->input('offset'));
        $offset = $offset ? $offset : 0;
        $limit = 10;
        $objectId = intval($request->input('object_id'));
        $type = $request->input('type') == 'paper' ? 'paper' : 'card';
        $html = '';
        $objectsCount = 0;
        if ($type == 'paper') {
            $papers = Card::find($objectId)->pinsPaper()
                ->limit($limit)
                ->offset($offset)
                ->orderBy('created_at', 'desc')
                ->get();
            $objectsCount = Card::find($objectId)->pinsPaper()->count();
            foreach ($papers as $paper) {
                $html .= PaperService::renderPublication($paper);
            }
        } else {
            $paper = Paper::where('external_id', $objectId)->first();
            if ($paper) {
                $cards = $paper->pinnedCard()
                    ->offset($offset)
                    ->limit($limit)->orderBy('created_at', 'desc')
                    ->get();
                $objectsCount = $paper->pinnedCard()->count();
                foreach ($cards as $card) {
                    $html .= CardService::renderCard($card);
                }
            }
        }
        $html .= AjaxService::buildAjaxPagination($objectsCount, $offset, $limit);
        return response()->json([
            'html' => $html
        ]);
    }

    /**
     * Add user expertise 
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function addExpertise(Request $request)
    {
        $this->middleware('auth');
        $expertiseName = htmlspecialchars($request->input('name'));
        $success = 0;
        if (!($userExpertise = Expertise::where('name', $expertiseName)
            ->where('user_id', Auth::user()->id)->first()
        )) {
            $expertise = new Expertise;
            $expertise->user_id = Auth::user()->id;
            $expertise->name = $expertiseName;
            $expertise->save();
            $success = 1;
        }
        return response()->json([
            'success' => $success
        ]);
    }

    /**
     * Remove user expertise
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function removeExpertise(Request $request)
    {
        $this->middleware('auth');
        $expertiseName = htmlspecialchars($request->input('name'));
        Expertise::where('name', $expertiseName)
            ->where('user_id', Auth::user()->id)
            ->delete();
    }

    /**
     * Get user pinned cards
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getUserPinnedCards(Request $request)
    {
        $offset = intval($request->input('offset'));
        $paperId = intval($request->input('paper_id'));
        $offset = $offset ? $offset : 0;
        $limit = 7;
        $html = '';
        $paper = Paper::find($paperId);
        $cards = \DB::table('cards')
            ->leftJoin('user_pin_card', 'cards.id', '=', 'user_pin_card.card_id')
            ->leftJoin('papers', 'user_pin_card.paper_id', '=', 'papers.id')
            ->where('papers.id', $paperId)
            ->where('user_pin_card.user_id', Auth::user()->id)
            ->limit($limit)
            ->offset($offset)
            ->select('cards.*')
            ->get();
        $cardsCount = \DB::table('cards')
            ->leftJoin('user_pin_card', 'cards.id', '=', 'user_pin_card.card_id')
            ->leftJoin('papers', 'user_pin_card.paper_id', '=', 'papers.id')
            ->where('papers.id', $paperId)
            ->where('user_pin_card.user_id', Auth::user()->id)
            ->count();
        $html .= '<h4>'.$paper->title.'</h4>
                    <h3>Pins on this topic</h3>
                    <table>';
        foreach ($cards as $card) {
            $html .= '<tr>
                        <td>
                            <div class="icon">
                                <img src="/img/file-2.svg" alt="">
                            </div>
                            <div class="text">
                                <h5>'.$card->title.'</h5>
                                <p>'.$card->short_description.'</p>
                            </div>
                        </td>
                        <td>
                            <input type="checkbox" id="ch-pinned-'.$card->id.'" checked="" class="pin-checkbox">
                            <label class="checkbox-pinned" for="ch-pinned-'.$card->id.'">
                                <i class="fas fa-check-circle"></i>
                                <span></span>
                            </label>
                        </td>
                    </tr>';
        }
        $html .= '<table>';
        //build pagination
        $html .= AjaxService::buildAjaxPagination($cardsCount, $offset, $limit);
        return response()->json(['html' => $html]);
    }

    /**
     * Save user settings
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function saveUserSettings(Request $request)
    {
        $this->middleware('auth');
        $data = [
            'update_impact_areas' => $request->input('update_impact_areas') ? 1 : 0,
            'update_publications_interest' => $request->input('update_publications_interest') ? 1 : 0,
            'update_technologies_interest' => $request->input('update_technologies_interest') ? 1 : 0,
            'update_co_authors_joining' => $request->input('update_co_authors_joining') ? 1 : 0,
            'update_users_areas_impact' => $request->input('update_users_areas_impact') ? 1 : 0,
            'update_new_matching_jobs' => $request->input('update_new_matching_jobs') ? 1 : 0,
            'update_new_matching_events' => $request->input('update_new_matching_events') ? 1 : 0,
            'private_message' => $request->input('private_message') ? 1 : 0,
            'view_profile' => $request->input('view_profile') ? 1 : 0
        ];
        UserSetting::where('user_id', Auth::user()->id)->delete();
        $saveData = [];
        foreach ($data as $key => $value) {
            $saveData[] = new UserSetting([
                'name' => $key,
                'value' => $value
            ]);
        }
        Auth::user()->settings()->saveMany($saveData);
        return response()->json(['success' => 1]);
    }

    /**
     * Get user events
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getUserEvents(Request $request)
    {
        $offset = intval($request->input('offset'));
        $offset = $offset ? $offset : 0;
        $limit = 50;
        $saved = $request->input('saved') ? 1 : 0;
        $keywords = htmlspecialchars($request->input('keywords'));
        $location = htmlspecialchars($request->input('location'));
        $html = '';
        $userEvents = [];
        if ($saved) {
            $eventList = Auth::user()->events()
                ->offset($offset)
                ->limit($limit)
                ->get();
            $countEvents = Auth::user()->events()->count();
        } else {
            $page = ceil($offset / $limit);
            $events = EventbriteService::getEvents($page, $keywords, $location);
            $eventList = [];
            $eventsIds = [];
            foreach ($events->events as $event) {
                $eventsIds[] = $event->id;
            }
            $userEvents = Auth::user()->events()->whereIn('external_id', $eventsIds)->get();
            foreach ($events->events as $event) {
                $e = new stdClass;
                $e->external_id = $event->id;
                $e->name = $event->name->text;
                $e->link = $event->url;
                $e->start_date = date('Y-m-d H:i:s', strtotime($event->start->utc));
                $eventList[] = $e;
            }
            $countEvents = $events->pagination->object_count;
        }
        $html .= EventbriteService::buildHtml($eventList, $userEvents, $offset > 0);
        $html .= AjaxService::buildAjaxPagination($countEvents, $offset, $limit);
        return response()->json([
            'html' => $html
        ]);
    }

    /**
     * Save user preferences
     * @param  Request $request
     * @return void
     */
    public function saveUserPreferences(Request $request)
    {
        $categories = $request->input('categories');
        $papersToRemove = $request->input('papers');
        if (is_array($categories) && count($categories)) {
            Auth::user()->categories()->sync($categories);
        }
        if (is_array($papersToRemove) && count($papersToRemove)) {
            Auth::user()->papers()->detach($papersToRemove);
            UserPinCard::where('user_id', Auth::user()->id)
                ->whereIn('paper_id', $papersToRemove)
                ->delete();
        }
    }

    /**
     * Add event to user
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function addUserEvent(Request $request)
    {
        $eventData = $request->input('object');
        if (!$event = Auth::user()->events()
            ->where('external_id', $eventData['external_id'])->first()
        ) {
            $event = new Event;
            $event->external_id = $eventData['external_id'];
            $event->name = $eventData['name'];
            $event->link = $eventData['link'];
            $event->start_date = $eventData['start_date'];
            $event->user_id = Auth::user()->id;
            $event->save();
        } else {
            $event->delete();
        }
    }

    /**
     * Get user jobs
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getUserJobs(Request $request)
    {
        $offset = intval($request->input('offset'));
        $offset = $offset ? $offset : 0;
        $limit = 50;
        $saved = $request->input('saved') ? 1 : 0;
        $keywords = htmlspecialchars($request->input('keywords'));
        $location = htmlspecialchars($request->input('location'));
        $html = '';
        $userJobs = [];
        if ($saved) {
            $jobList = Auth::user()->jobs()
                ->offset($offset)
                ->limit($limit)
                ->get();
            $countJobs = Auth::user()->jobs()->count();
        } else {
            $page = ceil($offset / $limit);
            $jobs = LinkUpService::getJobs($page, $keywords, $location);
            $jobList = [];
            $jobsKeys = [];
            foreach ($jobs->jobs as $job) {
                $jobsKeys[] = md5($job->job_title_link);
            }
            $userJobs = Auth::user()->jobs()->whereIn('key', $jobsKeys)->get();
            foreach ($jobs->jobs as $job) {
                $e = new stdClass;
                $e->key = md5($job->job_title_link);
                $e->name = $job->job_title;
                $e->link = $job->job_title_link;
                $jobList[] = $e;
            }
            $countJobs = $jobs->result_info->total_jobs;
        }
        $html .= LinkUpService::buildHtml($jobList, $userJobs, $offset > 0);
        $html .= AjaxService::buildAjaxPagination($countJobs, $offset, $limit);
        return response()->json([
            'html' => $html
        ]);
    }

    /**
     * Add job to user
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function addUserJob(Request $request)
    {
        $jobData = $request->input('object');
        if (!$job = Auth::user()->jobs()
            ->where('key', $jobData['key'])->first()
        ) {
            $job = new Job;
            $job->key = $jobData['key'];
            $job->name = $jobData['name'];
            $job->link = $jobData['link'];
            $job->user_id = Auth::user()->id;
            $job->save();
        } else {
            $job->delete();
        }
    }

    /**
     * Mark notifications as readed
     * @return \Illuminate\Http\Response
     */
    public function notificationsReaded()
    {
        UserNotification::where('user_id', Auth::user()->id)->update(['readed' => 1]);
    }

    public function updateImpactAreas()
    {
        $colors = [];
        $data = [];
        $html = '';
        $impactAreas =  PaperService::prepareImpactAreas(
            Paper::getImpactAreasForUser(Auth::user()->id, 6)
        );
        if ($impactAreas) {
            $html = PaperService::renderImpactAreasForFeedPage(
                $impactAreas
            );
            
            foreach ($impactAreas as $area) {
                $colors[] = $area['color'];
            }
            foreach ($impactAreas as $area) {
                $data[] = [
                    'y' => $area['percent'],
                    'label' => $area['title']
                ];
            }
        }
        return response()->json([
            'html' => $html,
            'colors' => $colors,
            'data' => $data
        ]);
    }

    /**
     * Get user recent publications
     * @param  Request $request
     * @return Response
     */
    public function getUserPublications(Request $request)
    {
        $q = $request->input('q');
        $categoryId = $request->input('category_id') ? $request->input('category_id') : 0;
        $offset = $request->input('offset') ? $request->input('offset') : 0;
        $limit  = $request->input('limit') ? $request->input('limit') : 5;
        $query = Auth::user()->papers();
        if ($categoryId || $q) {
            if ($categoryId) {
                $query = $query->whereHas('categories', function($query) use($categoryId){
                     $query->where('categories.id', $categoryId);
                });
            }
            if ($q) {
                $query = $query->whereRaw('(MATCH (papers.title, papers.description)'.
                    ' AGAINST (? IN BOOLEAN MODE) OR papers.title LIKE ? OR papers.description'.
                    ' LIKE ?)', [$q, '%'.$q.'%', '%'.$q.'%']
                );    
            }
            $papers = $query->inRandomOrder();
        } else {
            $papers = $query->orderBy('created_at', 'desc');
        }
        $count = $papers->count();
        $papers = $query->offset($offset)->limit($limit)->get();

        //turn off search in user papers
        if ($categoryId || $q) {
            $papers = [];
            $count = 0;
        }

        $html = PaperService::buildFeedHtml($papers);
        $charts = PaperService::getImpact($papers);
        $sub_count = $count - ($offset + $limit);
        return response()->json([
            'html' => $html,
            'charts' => $charts,
            'offset' => $count > $offset + $limit ? $offset + $limit : -1,
            'count' => $count,
            'sub_count' => $sub_count ? $sub_count : 0
        ]);

    }


    /**
     * Get user recent publications
     * @param  Request $request
     * @return Response
     */
    public function getAllPublications(Request $request)
    {
        $q = $request->input('q');
        $categoryId = $request->input('category_id') ? $request->input('category_id') : 0;
        $offset = $request->input('offset') ? $request->input('offset') : 0;
        $type = $request->input('type') == 'following' ? 'following' : 'recent';
        $limit  = 10;
        if ($type == 'following') {
            $papers = Paper::whereHas('followers', function ($query) {
                $query->where('users.id', Auth::user()->id);
            })->orderBy('created_at', 'desc');
        } else {
            if ($categoryId || $q) {
                $query = Paper::inRandomOrder();
                if ($categoryId) {
                    $query = $query->whereHas('categories', function($query) use($categoryId){
                         $query->where('categories.id', $categoryId);
                    });
                }
                if ($q) {
                    $query->whereRaw('(MATCH (papers.title, papers.description)'.
                        ' AGAINST (? IN BOOLEAN MODE) OR papers.title LIKE ? OR papers.description'.
                        ' LIKE ?)', [$q, '%'.$q.'%', '%'.$q.'%']
                    );
                }
                $papers = $query;
            } else {
                $query = Paper::where(function($query){
                    $query->doesntHave('users')->orWhereHas('users', function ($query) {
                        $query->where('users.id', '<>', Auth::user()->id);
                    });
                });
                $papers = $query->orderBy('created_at', 'desc');
            }
        }
        $count = $papers->count();
        $papers = $papers->offset($offset)->limit($limit)->get();
        $html = PaperService::buildFeedHtml($papers);
        $charts = PaperService::getImpact($papers);
        return response()->json([
            'html' => mb_convert_encoding($html, 'UTF-8', 'UTF-8'),
            'charts' => $charts,
            'offset' => count($papers) ? $offset + $limit : -1,
            'count' => $count
        ]);

    }

    /**
     * Get user publications for profile page
     * @param  Request $request
     * @return Response
     */
    public function getUserPublicationsForProfile(Request $request)
    {
        $limit = 3;
        $offset = intval($request->input('offset')) ? (int)$request->input('offset') : 0;
        $query = Auth::user()->papers();
        $papersCount = $query->count();
        $papers = $query->offset($offset)->limit($limit)->get();
        $html = PaperService::buildPapersForProfileHtml($papers);
        $html .= AjaxService::buildAjaxPagination($papersCount, $offset, $limit);
        return response()->json([
            'html' => mb_convert_encoding($html, 'UTF-8', 'UTF-8')
        ]);
    }
}
