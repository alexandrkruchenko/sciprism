<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use App\Category;
use App\User;
use App\Paper;
use App\CareerFitQuestion;

use App\Services\PaperService;

class MainController extends Controller
{
    private $categories;

    public function __construct()
    {
        $this->categories = Category::get();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->input('search');
        $cardId = 0;
        if ($request->is('innovation/*')) {
            $cardId = $request->route('id');
        }
        $paperPubmedId = 0;
        if ($request->is('paper/*')) {
            $paperPubmedId = $request->route('id');
        }
        $data = [
            'categories' => $this->categories,
            'amount_categories' => count($this->categories),
            'search' => $search,
            'impact_areas' => PaperService::prepareImpactAreas(
                Paper::getImpactAreasForUser(Auth::user()->id, 6)
            ),
            'card_id' => $cardId,
            'paper_pubmed_id' => $paperPubmedId,
            'questions_by_skills' => CareerFitQuestion::where('type_id', 1)->get(),
            'questions_by_interests' => CareerFitQuestion::where('type_id', 2)->get(),
            'questions_by_values' => CareerFitQuestion::where('type_id', 3)->get(),
            'questions_results' => Auth::user()->careerFitQuestionResult,
            'show_demo' => $request->input('showDemo') ? true : false,
            'scripts' => [
                'js/index.js',
            ]
        ];
        return view('page', $data);
    }

    /**
     * Show the user following.
     * @return \Illuminate\Http\Response
     */
    public function following()
    {
        $data = [
            'categories' => $this->categories,
            'amount_categories' => count($this->categories),
            'category_id' => 0,
            'scripts' => [
                'js/following.js'
            ]
        ];
        return view('page', $data);
    }

    /**
     * Load category
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function category($slug)
    {
        $categoryId = 0;
        if ($slug) {
            $category = Category::where('slug', $slug)->first();
            $categoryId = $category->id;
        }
        $data = [
            'categories' => $this->categories,
            'amount_categories' => count($this->categories),
            'category_id' => $categoryId,
            'scripts' => [
                Auth::user() ? 'js/index.js' : 'js/home.js',
            ]
        ];
        return view('page', $data);
    }
}
