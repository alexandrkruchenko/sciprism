<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Subscriber;

class SubscriberController extends Controller
{
    /**
     * Add subscriber to db
     * @param Request $request
     * @return void
     */
    public function add(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email|unique:subscribers'
        ]);

        Subscriber::create($request->all());

        return redirect()->back()->with('success', 'You have successfully subscribed!');
    }
}
