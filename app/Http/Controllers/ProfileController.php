<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Education;
use App\Experience;
use App\User;
use App\Category;
use App\Paper;
use App\CareerFitQuestion;
use App\Statistic;

use App\Services\PaperService;

class ProfileController extends Controller
{
    private $categories;

    public function __construct()
    {
        $this->categories = Category::get();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        if (intval($slug)) {
            $user = User::find($slug);
        } else {
            $user = User::where('slug', $slug)->first();
        }
        $educations = $user->educations;
        $experiences = $user->experiences;
        $papers = $user->papers;
        $expertises = $user->expertises;
        $userDistincAreas = PaperService::renderImpactAreasForProfilePage(
            Paper::getImpactAreasForUser($user->id, 5)
        );
        $data = [
            'categories' => $this->categories,
            'amount_categories' => count($this->categories),
            'user' => $user,
            'educations' => $educations,
            'experiences' => $experiences,
            'papers' => $papers,
            'expertises' => $expertises,
            'impact_areas' => PaperService::prepareImpactAreas(
                Paper::getImpactAreasForUser(Auth::user()->id, 6)
            ),
            'count_papers' => $user->papers()->count(),
            'pins_count' => $user->pins()->count(),
            'count_educations' => count($expertises),
            'count_experiences' => count($experiences),
            'count_papers' => count($papers),
            'count_expertises' => count($expertises),
            'user_distinc_areas' => $userDistincAreas,
            'is_owner' => Auth::user() && $user->id == Auth::user()->id,
            'questions_by_skills' => CareerFitQuestion::where('type_id', 1)->get(),
            'questions_by_interests' => CareerFitQuestion::where('type_id', 2)->get(),
            'questions_by_values' => CareerFitQuestion::where('type_id', 3)->get(),
            'questions_results' => Auth::user()->careerFitQuestionResult,
            'scripts' => [
                'js/profile.js'
            ]
        ];
        if ($data['is_owner']) {
            if (!session('success')) {
                $statistic = new Statistic;
                $statistic->event = 'visit';
                $statistic->object = 'profile';
                if (Auth::user()) {
                    $statistic->user_id = Auth::user()->id;
                }
                $statistic->save();
            }
        }
        return view('profile', $data);
    }

    /**
     * Save profile data
     * @param  Request $request
     * @return
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'title' => 'sometimes|nullable|string|max:191',
            'name' => 'required|string|max:128',
            'last_name' => 'required|string|max:128',
            'avatar' => 'required|string',
            'summary' => 'sometimes|nullable|string',
            'educations.*.school' => 'sometimes|nullable|string|max:190',
            'educations.*.from_year' => 'sometimes|nullable|integer',
            'educations.*.to_year' => 'sometimes|nullable|integer',
            'experiences.*.title' => 'sometimes|nullable|string|max:190',
            'experiences.*.company' => 'sometimes|nullable|string|max:190',
            'experiences.*.from_year' => 'sometimes|nullable|integer',
            'experiences.*.to_year' => 'sometimes|nullable|integer'
        ]);

        $user = Auth::user();
        $user->name = $request->input('name');
        $user->last_name = $request->input('last_name');
        $user->avatar = $request->input('avatar');
        $user->title = $request->input('title');
        $user->summary = $request->input('summary');
        $user->save();

        Education::where('user_id', Auth::user()->id)->delete();
        $educations = [];
        if (count($request->input('educations')) > 1) {
            foreach ($request->input('educations') as $education) {
                if (!$education['school']) {
                    continue;
                }
                $educations[] = [
                    'school' => $education['school'],
                    'from_year' => $education['from_year'],
                    'to_year' => $education['to_year'],
                    'user_id' => Auth::user()->id
                ];
            }
            Education::insert($educations);
        }

        Experience::where('user_id', Auth::user()->id)->delete();
        $experiences = [];
        if (count($request->input('experiences')) > 1) {
            foreach ($request->input('experiences') as $experience) {
                if (!$experience['title']) {
                    continue;
                }
                $experiences[] = [
                    'title' => $experience['title'],
                    'company' => $experience['company'],
                    'from_year' => $experience['from_year'],
                    'to_year' => $experience['to_year'],
                    'user_id' => Auth::user()->id
                ];
            }
            Experience::insert($experiences);
        }

        return redirect()->back()->with('success', 'Profile successfully updated!');
    }

    /**
     * Save profile data
     * @param  Request $request
     * @return
     */
    public function saveTestResults(Request $request)
    {
        $this->validate($request, [
            'questions' => 'required|array',
            'questions.*' => 'required|integer',
        ]);

        $careerFitQuestionResult = [];
        foreach ($request->input('questions') as $key => $score) {
            $careerFitQuestionResult[] = [
                'question_id' => $key,
                'score' => $score
            ];
        }

        Auth::user()->careerFitQuestionResult()->delete();
        Auth::user()->careerFitQuestionResult()->createMany($careerFitQuestionResult);

        return redirect()->back()->with('success', 'Your career survey is successfully saved!');
    }
}
