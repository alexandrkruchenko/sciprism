<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use TeamTNT\TNTSearch\Classifier\TNTClassifier;

use App\Category;
use App\Card;
use App\CategoryCard;
use App\CategoryPaper;
use App\Event;
use App\Paper;
use App\UserPinCard;
use App\User;

use App\Events\ImpactAreaReady;

use DB;


class CronController extends Controller
{

    /**
     * Autopin
     * @return void
     */
    public function autopin()
    {
        $papers = DB::select('
            SELECT papers.id as id, papers.description as description, paper_user.user_id as user_id
            FROM papers
            LEFT JOIN paper_user ON paper_user.paper_id = papers.id
            WHERE papers.id NOT IN (SELECT user_pin_card.paper_id FROM user_pin_card WHERE auto = 1) AND paper_user.user_id > 0 AND papers.description != "" AND papers.id = 915
            GROUP BY papers.id
            LIMIT 100'
        );
        foreach ($papers as $paper)
        {
            $classifier = new TNTClassifier();
            $classifier->load(public_path('data.cls'));
            $results = $classifier->predict($paper->description, 6);
            foreach ($results as $guess) {
                $category = Category::where('title', 'like', '%'.$guess['label'].'%')->first();
                if ($category) {
                    if (
                        UserPinCard::where('user_id', $paper->user_id)->count() == 0 &&
                        CategoryCard::where('category_id', $category->id)->count() > 0
                    ) {
                        $user = User::find($paper->user_id);
                        if ($user) {
                            event(new ImpactAreaReady($user));
                        }
                    }
                    if ($paper->user_id) {
                        DB::insert(
                            "INSERT INTO user_pin_card(user_id, card_id, paper_id, auto)
                            SELECT ?, cards.id, ?, 1 FROM cards
                            LEFT JOIN category_card ON category_card.card_id = cards.id WHERE category_id = ".$category->id,
                            [$paper->user_id, $paper->id]
                        );  
                    }               
                }
            }
            if (!$results) {
                DB::insert(
                    "INSERT INTO user_pin_card(user_id, card_id, paper_id, auto)
                    VALUES(?, ?, ?, 1 )",
                    [$paper->user_id, 0, $paper->id]
                );
            }
        }
    }

    /**
     * Remove started events
     * @return void
     */
    public function removeEvents()
    {
        Event::where('start_date', '<', date('Y-m-d H:i:s'))->delete();
    }

    /**
     * Predict category for card and set it 
     * @return void
     */
    public function predictAndSetCategory()
    {
        $cards = Card::where('was_predicted', 0)->limit(25)->get();
        if (count($cards)) {
            foreach ($cards as $card) {
                $classifier = new TNTClassifier();
                $classifier->load(public_path('data.cls'));
                $results = $classifier->predict($card->description, 6);
                foreach ($results as $guess) {
                    $category = Category::where('title', 'like', '%'.$guess['label'].'%')->first();
                    $card->was_predicted = 1;
                    if ($category) {
                        $categoryCard = CategoryCard::firstOrCreate([
                            'category_id' => $category->id,
                            'card_id' => $card->id,
                        ]);
                        if (!$categoryCard) {
                            $categoryCard->category_id = $category->id;
                            $categoryCard->card_id = $card->id;
                            $categoryCard->save();
                        }
                    }
                    $card->save();
                }
            }
        }
    }

    /**
     * Predict category for paper and set it 
     * @return void
     */
    public function predictAndSetCategoryForPaper()
    {
        $papers = Paper::where('was_predicted', 0)->limit(25)->get();
        if (count($papers)) {
            foreach ($papers as $paper) {
                $classifier = new TNTClassifier();
                $classifier->load(public_path('data.cls'));
                $results = $classifier->predict($paper->description, 6);
                foreach ($results as $guess) {
                    $category = Category::where('title', 'like', '%'.$guess['label'].'%')->first();
                    $paper->was_predicted = 1;
                    if ($category) {
                        $categoryPaper = CategoryPaper::firstOrCreate([
                            'category_id' => $category->id,
                            'paper_id' => $paper->id,
                        ]);
                        if (!$categoryPaper) {
                            $categoryPaper->category_id = $category->id;
                            $categoryPaper->paper_id = $paper->id;
                            $categoryPaper->save();
                        }
                    }
                    $paper->save();
                }
            }
        }
    }
}
