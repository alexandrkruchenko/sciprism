<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class BlockedUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if ($user && $user->blocked) {
            Auth::logout();
             return redirect('/home');
        }
        return $next($request);
    }
}
