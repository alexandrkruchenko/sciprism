<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expertise extends Model
{
    public $timestamps = false;
    protected $table = 'expertises';
}
