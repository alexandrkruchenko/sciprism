<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use UserCategory;

use UserPaper;

use DB;

use App\Services\PaperService;

use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

     public $sortable = [ 
        'name'
    ];


     /**
     * The user categories.
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    /**
     * The user papers.
     */
    public function papers()
    {
        return $this->belongsToMany('App\Paper');
    }

    /**
     * The user educations.
     */
    public function educations()
    {
        return $this->hasMany('App\Education');
    }

    /**
     * The user experiences.
     */
    public function experiences()
    {
        return $this->hasMany('App\Experience');
    }

    /**
     * The user expertises.
     */
    public function expertises()
    {
        return $this->hasMany('App\Expertise');
    }

    /**
     * The user comments.
     */
    public function comment()
    {
        return $this->hasMany('App\Comment');
    }

    /**
     * The user comments.
     */
    public function paperComment()
    {
        return $this->hasMany('App\PaperComment');
    }

    /**
     * The user events.
     */
    public function events()
    {
        return $this->hasMany('App\Event');
    }

    /**
     * The user jobs.
     */
    public function jobs()
    {
        return $this->hasMany('App\Job');
    }

    /**
     * The user settings.
     */
    public function settings()
    {
        return $this->hasMany('App\UserSetting');
    }

    /**
     * Get roles
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    /**
     * Pins
     */
    public function pins()
    {
        return $this->belongsToMany('App\Card', 'user_pin_card');
    }

    /**
     * Question result
     */
    public function careerFitQuestionResult()
    {
        return $this->hasMany('App\CareerFitQuestionResult');
    }

    /**
     * Check is admin.
     */
    public function isAdmin()
    {
        foreach ($this->roles()->get() as $role)
        {
            if ($role->name == 'admin')
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Get user link.
     */
    public function getUserLink()
    {
        return '/profile/'.($this->slug ? $this->slug : $this->id);
    }

    /**
     * Get user full name.
     */
    public function getUserFullName()
    {
        return $this->name.' '.$this->last_name;
    }

    /**
     * Get user profile completeness percent
     */
    public function getProfileCompletenessPercent()
    {
        $percent = 0;
        if ($this->summary) {
            $countWords = explode(' ', trim($this->summary));
            $percent += $countWords <= 25 ? 12 : 25;
        }
        $expertiseCount = $this->expertises()->count();
        if ($expertiseCount) {
            $percent += $expertiseCount <= 2 ? 12 : 25;
        }
        $educationCount = $this->educations()->count();
        if ($educationCount) {
            $percent += $educationCount <= 2 ? 12 : 25;
        }
        $papersCount = $this->papers()->count();
        if ($papersCount) {
            $percent += $papersCount <= 1 ? 12 : 25;
        }
        return $percent;
    }

    /**
     * Get user profession.
     */
    public function getUserProfessionBy($typeId)
    {
        return DB::select(
            'SELECT careerfit_professions.name as name
            FROM careerfit_q_results
            LEFT JOIN careerfit_questions ON 
                careerfit_questions.id = careerfit_q_results.question_id
            LEFT JOIN careerfit_p_q_scores ON 
                careerfit_p_q_scores.question_id = careerfit_q_results.question_id
            LEFT JOIN careerfit_professions ON 
                careerfit_professions.id = careerfit_p_q_scores.profession_id
            WHERE careerfit_questions.type_id = ? AND careerfit_q_results.user_id = ? 
                AND careerfit_p_q_scores.score = careerfit_q_results.score
            GROUP BY careerfit_professions.id
            ORDER BY COUNT(careerfit_professions.id) DESC, name ASC
            LIMIT 3', [$typeId, $this->id]);
    }

    public function getProfileKeywords()
    {
        $keywordsArray = [];
        $result = '';
        $impactAreas = PaperService::prepareImpactAreas(
            Paper::getImpactAreasForUser(Auth::user()->id, 3)
        );
        if ($impactAreas) {
            $keywordsArray = array_column($impactAreas, 'title');
        } else {
            $bySkills = $this->getUserProfessionBy(1);
            if ($bySkills) {
                $keywordsArray[] = $bySkills[0]->name;
            }
            $byInterests = $this->getUserProfessionBy(2);
            if ($byInterests) {
                $keywordsArray[] = $byInterests[0]->name;
            }
            $byValues = $this->getUserProfessionBy(3);
            if ($byValues) {
                $keywordsArray[] = $byValues[0]->name;
            }
        }
        if (!empty($keywordsArray)) {
            $keywordsArray = array_unique($keywordsArray);
            $result = implode(',', $keywordsArray);
        } else {
            $keywordsArray = $this->expertises()->get();
            if ($keywordsArray) {
                $result = $keywordsArray->implode('name', ',');
            } else {
                $keywordsArray = explode(' ', $this->title);
                $result = implode(',', $keywordsArray);
            }
        }
        return $result;
    }

    public function getAuthPassword() {
        return $this->password;
    }
}
