<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPinCard extends Model
{
    protected $table = 'user_pin_card';
}
