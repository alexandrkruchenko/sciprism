<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class CategoryPaper extends Model
{
    public $timestamps = false;
    protected $table = 'category_paper';
    protected $fillable = [
        'category_id', 
        'paper_id'
    ];
}