<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;

    /**
     * The category cards.
     */
    public function categories()
    {
        return $this->belongsToMany('App\Card');
    }

    /**
     * The category cards.
     */
    public function parentCategory()
    {
        return $this->belongsTo('App\Category', 'parent_id');
    }
}
