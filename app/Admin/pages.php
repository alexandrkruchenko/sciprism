<?php

AdminSection::registerModel(\App\Page::class, function (
    \SleepingOwl\Admin\Model\ModelConfiguration $model) {

    $model->setTitle('Pages');

    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync()
            ->setColumns([
                    AdminColumn::text('id', '#')->setOrderable(false),
                    AdminColumn::text('title', 'Page name')
            ]);
            $display->paginate(25);
        return $display;
    });
    
    //Edit
    $model->onCreateAndEdit(function($id = null) {
        $form = AdminForm::panel()->addBody(
            AdminFormElement::text('title', 'Title')->required(),
            AdminFormElement::text('slug', 'Slug')->required(),
            AdminFormElement::wysiwyg('body', 'Body'),
            AdminFormElement::text('meta_title', 'Meta title'),
            AdminFormElement::text('meta_description', 'Meta description'),
            AdminFormElement::text('meta_keywords', 'Meta keywords'),
            AdminFormElement::textarea('ga_code', 'GA code')
        )->addScript('custom', asset('/js/admin.js'), ['admin-default']);
        return $form;
    });
});