<?php

AdminSection::registerModel(\App\Notification::class, function (
    \SleepingOwl\Admin\Model\ModelConfiguration $model) {

    $model->setTitle('Notifications');

    //Display
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync()
            ->setColumns([
                    AdminColumn::text('id', '#'),
                    AdminColumn::text('type', 'Notification name'),
                    AdminColumn::custom('Email notification, Internal notification or both', 
                        function(\Illuminate\Database\Eloquent\Model $model) {
                            $value = 'Both';
                            if ($model->email && !$model->internal) {
                                $value = 'Email notification';
                            } else if (!$model->email && $model->internal) {
                                $value = 'Internal notification';
                            }
                            return $value;
                    }),
                    AdminColumn::custom('Enabled', 
                        function(\Illuminate\Database\Eloquent\Model $model) {
                            $value = 'Yes';
                            if (!$model->enabled) {
                                $value = 'No';
                            }
                            return $value;
                    }),
            ]);
        $display->paginate(25);
        $control = $display->getColumns()->getControlColumn();
        $button = new \SleepingOwl\Admin\Display\ControlButton(function (\Illuminate\Database\Eloquent\Model $model) {
            return route('admin.disable_notification', $model->getKey());
        }, 'Off/On', 50);
        $button->hideText();
        $button->setIcon('fa fa-power-off');
        $control->addButton($button);
        return $display;
    });
    
    //Edit
    $model->onEdit(function($id = null) {
        $form = AdminForm::panel()->addBody(
            AdminFormElement::text('subject', 'Subject')->required(),
            AdminFormElement::wysiwyg('text', 'Text', 'ckeditor')->required()
        );
        return $form;
    });
})  
    ->addMenuPage(\App\Notification::class, 400)
    ->setIcon('fa fa-bell');