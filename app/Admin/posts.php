<?php

AdminSection::registerModel(\App\Post::class, function (
    \SleepingOwl\Admin\Model\ModelConfiguration $model) {

    $model->setTitle('Blog');

    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync()
            ->setColumns([
                    AdminColumn::text('id', '#')->setOrderable(false),
                    AdminColumn::text('title', 'Post title')
            ]);
            $display->paginate(25);
        return $display;
    });
    
    //Edit
    $model->onCreateAndEdit(function($id = null) {
        $form = AdminForm::panel()->addBody(
            AdminFormElement::text('title', 'Title')->required(),
            AdminFormElement::text('slug', 'Slug')->required(),
            AdminFormElement::wysiwyg('body', 'Body'),
            AdminFormElement::text('author', 'Author'),
            AdminFormElement::text('linkedin_link', 'LinkedIn link'),
            AdminFormElement::image('thumbnail', 'Post thumbnail'),
            AdminFormElement::text('meta_title', 'Meta title'),
            AdminFormElement::text('meta_description', 'Meta description'),
            AdminFormElement::text('meta_keywords', 'Meta keywords')
        )->addScript('custom', asset('/js/admin.js'), ['admin-default']);
        return $form;
    });
});