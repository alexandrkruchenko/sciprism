<?php

AdminSection::registerModel(\App\Feedback::class, function (
    \SleepingOwl\Admin\Model\ModelConfiguration $model) {

    $model->setTitle('Feedbacks');

    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync()
            ->setColumns([
                    AdminColumn::text('id', '#')->setOrderable(false),
                    AdminColumn::text('name', 'Name'),
                    AdminColumn::text('email', 'Email'),
                    AdminColumn::text('subject', 'Subject'),
                    AdminColumn::text('message', 'Message'),
            ]);
            $display->paginate(25);
        return $display;
    });
})
    ->addMenuPage(\App\Feedback::class, 600)
    ->setIcon('fa fa-comments');