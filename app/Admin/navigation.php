<?php

use SleepingOwl\Admin\Navigation\Page;

return [
    
    [
        'title' => 'Back to front',
        'icon'  => 'fa fa-sign-out',
        'url'   => route('admin.front'),
        'priority' => 9999,
    ],
    [
        'title' => 'Content',
        'icon'  => 'fa fa-newspaper-o',
        'pages' => [
            [
              'title' => 'Blog',
              'url' => '/admin/posts'
            ],
            [
              'title' => 'Pages',
              'url' => '/admin/pages'
           ],
        ],
        'priority' => 300,
    ],
    [
        'title' => 'Default images',
        'icon'  => 'fa fa-file-image-o',
        'url' => '/admin/images',
        'priority' => 9997,
    ],
    [
        'title' => 'Users',
        'icon'  => 'fa fa-users',
        'pages' => [
            [
              'title' => 'Export to CSV',
              'url' => '/admin/users/csv'
           ],
            [
              'title' => 'List',
              'url' => '/admin/users'
           ],
        ],
        'priority' => 9998,
    ],
    [
        'title' => 'Dashboard',
        'icon'  => 'fa fa-dashboard',
        'url'   => route('admin.dashboard'),
        'priority' => 0,
    ],
];