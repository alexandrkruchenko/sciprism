<?php

AdminSection::registerModel(\App\Category::class, function (
    \SleepingOwl\Admin\Model\ModelConfiguration $model) {
    $model->setTitle('Categories');

    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync()
            ->setColumns([
                AdminColumn::text('id', '#')->setOrderable(false),
                AdminColumn::text('title', 'Category name'),
                AdminColumn::custom('Parent category', 
                    function(\Illuminate\Database\Eloquent\Model $model) {
                        $parent = $model->parentCategory()->get();
                        return count($parent) ? $parent[0]->title : '';
            })->setWidth('250px')
        ]);
        $display->paginate(25);
        return $display;
    });
    
    //Edit
    $model->onCreateAndEdit(function($id = null) {
        $form = AdminForm::panel()->addBody(
            AdminFormElement::text('title', 'Title')->required(),
            AdminFormElement::text('slug', 'Slug')->required(),
            AdminFormElement::wysiwyg('description', 'Description', 'ckeditor'),
            AdminFormElement::select('parent_id', 'Parent category', \App\Category::class)
                ->exclude([$id])->nullable(),
            AdminFormElement::image('icon', 'Icon(16x16)'),
            AdminFormElement::text('color', 'Input color or HEX(#ffffff)')
        )->addScript('custom', asset('/js/admin.js'), ['admin-default']);
        return $form;
    });
})
    ->addMenuPage(\App\Category::class, 100)
    ->setIcon('fa fa-tags');