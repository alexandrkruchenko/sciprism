<?php

AdminSection::registerModel(\App\Card::class, function (
    \SleepingOwl\Admin\Model\ModelConfiguration $model) {
    $model->setTitle('Cards');

    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync()
            ->setColumns([
                AdminColumn::text('id', '#')->setOrderable(false),
                AdminColumn::text('title', 'Title'),
                AdminColumn::datetime('created_at', 'Start Date')->setFormat('d.m.Y H:i:s'),
                AdminColumn::text('owner', 'Owner')->setOrderable(false),
                AdminColumn::custom('Number of pinned publications', function(\Illuminate\Database\Eloquent\Model $model) {
                    $pinsPaper = $model->pinsPaper()->get();
                    return count($pinsPaper);
                })->setWidth('150px'),
                AdminColumn::custom('Assigned categories', function(\Illuminate\Database\Eloquent\Model $model) {
                    return \App\Services\CategoryService::getCategoriesString($model->categories()->get());
                })
        ]);
        $display->paginate(25);
        $display->with('categories');
        return $display;
    });
    
    //Edit
    $model->onCreateAndEdit(function() {
        $form = AdminForm::panel()->addBody(
            AdminFormElement::text('title', 'Title')->required(),
            AdminFormElement::text('owner', 'Owner'),
            AdminFormElement::wysiwyg('short_description', 'Summary', 'ckeditor')->required(),
            AdminFormElement::wysiwyg('description', 'Description', 'ckeditor')->required(),
            AdminFormElement::custom(function ($model) {
                $model->image = request()->image;
            })->setDisplay(function($model) {
                return view('admin.custom.image', ['model' => $model]);
            }),
            AdminFormElement::multiselect('categories', 'Assigned categories', \App\Category::class)
            ->setDisplay('title'),
            AdminFormElement::multiselect('pinsPaper', 'Pinned publications', \App\Paper::class)
                ->setDisplay('title'),
            AdminFormElement::text('source_link', 'Source Link')
        )->addScript('cards.js', asset('/js/admin/cards.js'), ['admin-default']);
        return $form;
    });
})
    ->addMenuPage(\App\Card::class, 200)
    ->setIcon('fa fa-file');