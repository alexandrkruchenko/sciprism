<?php

AdminSection::registerModel(\App\User::class, function (
    \SleepingOwl\Admin\Model\ModelConfiguration $model) {
    $model->setTitle('Users');
    
    $model->disableCreating();

    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync()
            ->setColumns([
                AdminColumn::text('id', '#')->setOrderable(false),
                AdminColumn::text('name', 'Name'),
                AdminColumn::text('email', 'Email address')->setOrderable(false),
                AdminColumn::text('ip', 'IP address')->setOrderable(false),
                AdminColumn::datetime('created_at', 'Registration Date')->setFormat('d.m.Y H:i:s'),
                AdminColumn::text('network', 'Network')->setOrderable(false),
                AdminColumn::custom('Blocked', function(\Illuminate\Database\Eloquent\Model $model) {
                    return $model->blocked ? 'yes' : 'no';
                }),
                AdminColumn::custom('Impact areas', function(\Illuminate\Database\Eloquent\Model $model) {
                    return \App\Services\CategoryService::getCategoriesString($model->categories()->get());
            })->setWidth('250px')
        ]);
        $display->paginate(25);
        $control = $display->getColumns()->getControlColumn();
        $button = new \SleepingOwl\Admin\Display\ControlButton(function (\Illuminate\Database\Eloquent\Model $model) {
            return route('admin.block_user', $model->getKey());
        }, 'Block/Unblock', 50);
        $button->hideText();
        $button->setIcon('fa fa-ban');
        $control->addButton($button);
        $display->with('categories');
        return $display;
    });
    
    //Edit
    $model->onEdit(function() {
        $form = AdminForm::panel()->addBody(
            AdminFormElement::text('name', 'Name')->required(),
            AdminFormElement::text('email', 'Email address')->unique(),
            AdminFormElement::multiselect('categories', 'Impact areas', \App\Category::class)
                ->setDisplay('title')
        );
        return $form;
    });
});