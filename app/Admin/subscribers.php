<?php

AdminSection::registerModel(\App\Subscriber::class, function (
    \SleepingOwl\Admin\Model\ModelConfiguration $model) {

    $model->setTitle('Subscribers');

    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync()
            ->setColumns([
                    AdminColumn::text('id', '#')->setOrderable(false),
                    AdminColumn::text('first_name', 'Fisrt name'),
                    AdminColumn::text('last_name', 'Last name'),
                    AdminColumn::text('email', 'Email')
            ]);
            $display->paginate(25);
        return $display;
    });
    
    //Edit
    $model->onCreateAndEdit(function($id = null) {
        $form = AdminForm::panel()->addBody(
            AdminFormElement::text('fisrt_name', 'Fisrt name')->required(),
            AdminFormElement::text('last_name', 'Last name')->required(),
            AdminFormElement::text('email', 'Email')->setValidationRules(['email'])->required()
        );
        return $form;
    });
})
    ->addMenuPage(\App\Subscriber::class, 500)
    ->setIcon('fa fa-envelope-square');