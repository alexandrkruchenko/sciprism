<?php

Route::get('', ['as' => 'admin.dashboard', function () {
    $controller = new \App\Http\Controllers\AdminController;
	$content = $controller->getUsersCharts();
	return AdminSection::view($content, 'Dashboard');
}]);

Route::get('/toFront', ['as' => 'admin.front', function () {
    return redirect('/');
}]);

Route::post('/users/block/{id}', ['as' => 'admin.block_user', function($id) {
    $user = App\User::find($id);
    $user->blocked = !$user->blocked;
    $user->save();
    return redirect('/admin/users');
}]);

Route::get('/users/csv', 
    ['uses' => '\App\Http\Controllers\AdminController@usersToCsv', 'as' => 'admin.users_to_csv']
);

Route::post('/notifications/disable/{id}', ['as' => 'admin.disable_notification', function($id) {
    $notification = App\Notification::find($id);
    $notification->enabled = !$notification->enabled;
    $notification->save();
    return redirect('/admin/notifications');
}]);

Route::post('/imageSave', [
    'as'   => 'upload.image',
    'uses' => '\App\Http\Controllers\AdminController@imageSave'
]);

Route::get('/images', ['as' => 'admin.images', function() {
    return AdminSection::view(
        '<iframe style="width:100%;height:600px;border:none;" src="/packages/barryvdh/elfinder/browser.html?tnbrowser=fileid"></iframe>', 
        'Default images'
    );
}]);