<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareerFitQuestionResult extends Model
{
    public $timestamps = false;
    protected $table = 'careerfit_q_results';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'question_id',
        'score'
    ];

    /**
     * Questions
     */
    public function questions()
    {
        return $this->belongsTo('App\CareerFitQuestion');
    }
}
