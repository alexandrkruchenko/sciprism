<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'value'
    ];

    /**
     * Find setting by name
     * @param  [type] $query [description]
     * @param  [type] $name  [description]
     * @return [type]        [description]
     */
    public function scopeName($query, $name)
    {
        return $query->where('name', $name);
    }
}
