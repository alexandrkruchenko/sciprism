<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaperMeta extends Model
{
    public $timestamps = false;
    
    protected $table = 'paper_meta';

    protected $fillable = [
        'paper_id', 'name', 'value',
    ];
}
