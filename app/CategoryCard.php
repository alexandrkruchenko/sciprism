<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class CategoryCard extends Model
{
    public $timestamps = false;
    protected $table = 'category_card';
    protected $fillable = [
        'category_id', 
        'card_id'
    ];
}