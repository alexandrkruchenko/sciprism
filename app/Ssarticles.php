<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ssarticles extends Model
{
    protected $table = 'ss_con_articles';
    public $timestamps = false;
}