<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Card extends Model
{
    /**
     * The card categories.
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category', 'category_card');
    }

    /**
     * The card comments.
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    /**
     * The card pins paper.
     */
    public function pinsPaper()
    {
        return $this->belongsToMany('App\Paper', 'user_pin_card');
    }

    /**
     * Get realted cards
     * @param  integer $limit
     * @return Card
     */
    public function getRelated($limit = 5)
    {
        return Card::whereHas('categories', function ($query) {
            $categoryIds = $this->categories()->pluck('categories.id')->all();
            $query->whereIn('categories.id', $categoryIds);
        })->where('id', '<>', $this->id)->inRandomOrder()->limit($limit)->get();
    }

    /**
     * Card followers
     */
    public function followers()
    {
        return $this->belongsToMany('App\User', 'follows');
    }

    /**
     * User followed?
     * @param  integer $userId
     * @return void
     */
    public function isFollowed($userId)
    {
        return $this->followers()->where('users.id', $userId)->first();

    }

    /**
     * Get relevant cards
     * @param integer $userId
     * @param integer $offset
     * @param integer $limit
     * @return 
     */
    public static function getRelevantCards($userId = 0, $categoryId = 0, $offset, $limit)
    {
        if ($userId) {
            $query = 'SELECT
                        cards.id as id, cards.title as title,
                        cards.short_description as short_description,
                        COUNT(DISTINCT user_pins.id) as count_pinned_papers,
                        (IF(categories.title != "", categories.title, "Uncategorized")) as category_title,
                        (IF(user_categories.category_id != 0, 1, 0)) as user_interested,
                        (IF(user_follows.id, 1, 0)) as followed,
                        cards.owner as owner,
                        cards.image as image,
                        users.name as user_name,
                        users.last_name as user_last_name,
                        users.slug as user_slug,
                        users.id as user_id,
                        users.avatar as user_avatar,
                        papers.title as paper_title,
                        DATE_FORMAT(papers.publication_date, \'%d %b %Y\') as paper_date,
                        papers.journal as paper_journal
                    FROM cards
                    LEFT OUTER JOIN (
                        SELECT
                        user_pin_card.id as id, 
                        user_pin_card.card_id as card_id, 
                        user_pin_card.paper_id as paper_id,
                        user_pin_card.user_id as user_id
                        FROM user_pin_card
                        WHERE user_pin_card.user_id = '.$userId.'
                    ) as user_pins ON user_pins.card_id = cards.id
                    LEFT JOIN (SELECT
                        category_card.category_id as category_id,
                        category_card.card_id as card_id
                        FROM category_card GROUP BY card_id
                    ) as cat_card ON cards.id = cat_card.card_id
                    LEFT JOIN categories ON cat_card.category_id = categories.id
                    LEFT JOIN users ON user_pins.user_id = users.id
                    LEFT JOIN papers ON user_pins.paper_id = papers.id
                    LEFT OUTER JOIN follows ON follows.user_id = users.id 
                    LEFT OUTER JOIN ( 
                        SELECT category_card.card_id as card_id, category_user.category_id as category_id 
                        FROM category_card 
                        LEFT JOIN category_user ON category_user.category_id = category_card.category_id 
                        WHERE category_user.user_id = '.$userId.' ) 
                        as user_categories ON user_categories.card_id = cards.id 
                    LEFT OUTER JOIN ( 
                        SELECT follows.id as id, follows.card_id as card_id 
                        FROM follows WHERE follows.user_id = '.$userId.' ) 
                        as user_follows ON user_follows.card_id = cards.id'.
                    ($categoryId ? ' WHERE categories.id = '.$categoryId : '').'
                    GROUP BY cards.id 
                    ORDER BY user_interested DESC,count_pinned_papers DESC
                    LIMIT ?, ?';
        } else {
            $query = 'SELECT
                        cards.id as id, cards.title as title,
                        cards.short_description as short_description,
                        COUNT(DISTINCT user_pin_card.id) as count_pinned_papers,
                        (IF(categories.title != "", categories.title, "Uncategorized")) as category_title,
                        cards.owner as owner,
                        cards.image as image
                    FROM cards
                    LEFT JOIN user_pin_card ON cards.id = user_pin_card.card_id
                    LEFT JOIN (SELECT 
                                category_card.category_id as category_id, 
                                category_card.card_id as card_id
                                FROM category_card GROUP BY card_id
                    ) as cat_card ON cards.id = cat_card.card_id
                    LEFT JOIN categories ON cat_card.category_id = categories.id
                    '.($categoryId ? ' WHERE categories.id = '.$categoryId : '').'
                GROUP BY cards.id
                ORDER BY count_pinned_papers DESC
                LIMIT ?, ?';
        }
        return DB::select($query, [$offset, $limit]);
    }

    /**
     * Get recent cards
     * @param integer $userId
     * @param integer $offset
     * @param integer $limit
     * @param string  $q
     * @return 
     */
    public static function getRecentCards(
        $userId = 0, $categoryId = 0, $offset, $limit, $q = false
    ){
        if ($userId) {
            $query = 'SELECT
                        cards.id as id, cards.title as title,
                        cards.short_description as short_description,
                        COUNT(DISTINCT user_pins.id) as count_pinned_papers,
                        (IF(categories.title != "", categories.title, "Uncategorized")) as category_title,
                        (IF(user_categories.category_id != 0, 1, 0)) as user_interested,
                        (IF(user_follows.id, 1, 0)) as followed,
                        cards.owner as owner,
                        cards.image as image,
                        users.name as user_name,
                        users.last_name as user_last_name,
                        users.slug as user_slug,
                        users.id as user_id,
                        users.avatar as user_avatar,
                        papers.title as paper_title,
                        DATE_FORMAT(papers.publication_date, \'%d %b %Y\') as paper_date,
                        papers.journal as paper_journal
                    FROM cards
                    LEFT OUTER JOIN (
                        SELECT
                        user_pin_card.id as id, 
                        user_pin_card.card_id as card_id, 
                        user_pin_card.paper_id as paper_id,
                        user_pin_card.user_id as user_id
                        FROM user_pin_card
                        WHERE user_pin_card.user_id = '.$userId.'
                    ) as user_pins ON user_pins.card_id = cards.id
                    LEFT JOIN (SELECT
                        category_card.category_id as category_id,
                        category_card.card_id as card_id
                        FROM category_card GROUP BY card_id
                    ) as cat_card ON cards.id = cat_card.card_id
                    LEFT JOIN categories ON cat_card.category_id = categories.id
                    LEFT JOIN users ON user_pins.user_id = users.id
                    LEFT JOIN papers ON user_pins.paper_id = papers.id
                    LEFT OUTER JOIN follows ON follows.user_id = users.id 
                    LEFT OUTER JOIN ( 
                        SELECT category_card.card_id as card_id, category_user.category_id as category_id 
                        FROM category_card 
                        LEFT JOIN category_user ON category_user.category_id = category_card.category_id 
                        WHERE category_user.user_id = '.$userId.' ) 
                        as user_categories ON user_categories.card_id = cards.id 
                    LEFT OUTER JOIN ( 
                        SELECT follows.id as id, follows.card_id as card_id 
                        FROM follows WHERE follows.user_id = '.$userId.' ) 
                        as user_follows ON user_follows.card_id = cards.id'.
                    ($categoryId ? ' WHERE categories.id = '.$categoryId : '').
                    ($q ? ' WHERE MATCH (cards.title, cards.short_description, cards.description, cards.owner) AGAINST (\''.$q.'\' IN BOOLEAN MODE) OR cards.title LIKE \'%'.$q.
                        '%\' OR cards.short_description LIKE \'%'.$q.'%\' 
                        OR cards.description LIKE \'%'.$q.'%\'' : '').'
                    GROUP BY cards.id 
                    ORDER BY cards.created_at DESC
                    LIMIT ?, ?';
        } else {
            $query = 'SELECT
                        cards.id as id, cards.title as title,
                        cards.short_description as short_description,
                        COUNT(DISTINCT user_pin_card.id) as count_pinned_papers,
                        (IF(categories.title != "", categories.title, "Uncategorized")) as category_title,
                        cards.owner as owner,
                        cards.image as image
                    FROM cards
                    LEFT JOIN user_pin_card ON cards.id = user_pin_card.card_id
                    LEFT JOIN (SELECT 
                                category_card.category_id as category_id, 
                                category_card.card_id as card_id
                                FROM category_card GROUP BY card_id
                    ) as cat_card ON cards.id = cat_card.card_id
                    LEFT JOIN categories ON cat_card.category_id = categories.id
                    '.($categoryId ? ' WHERE categories.id = '.$categoryId : '').
                    ($q ? ' WHERE MATCH (cards.title, cards.short_description, cards.description, cards.owner) AGAINST (\''.$q.'\' IN BOOLEAN MODE) OR cards.title LIKE \'%'.$q.
                        '%\' OR cards.short_description LIKE \'%'.$q.'%\' 
                        OR cards.description LIKE \'%'.$q.'%\'' : '').'
                GROUP BY cards.id
                ORDER BY cards.created_at DESC
                LIMIT ?, ?';
        }
        return DB::select($query, [$offset, $limit]);
    }

    /**
     * Get popular cards
     * @param integer $userId
     * @param integer $offset
     * @param integer $limit
     * @return 
     */
    public static function getPopularCards($userId = 0, $categoryId = 0, $offset, $limit)
    {
        if ($userId) {
            $query = 'SELECT
                        cards.id as id, cards.title as title,
                        cards.short_description as short_description,
                        COUNT(DISTINCT user_pins.id) as count_pinned_papers,
                        (IF(categories.title != "", categories.title, "Uncategorized")) as category_title,
                        (IF(user_categories.category_id != 0, 1, 0)) as user_interested,
                        (IF(user_follows.id, 1, 0)) as followed,
                        cards.owner as owner,
                        cards.image as image,
                        users.name as user_name,
                        users.last_name as user_last_name,
                        users.slug as user_slug,
                        users.id as user_id,
                        users.avatar as user_avatar,
                        papers.title as paper_title,
                        DATE_FORMAT(papers.publication_date, \'%d %b %Y\') as paper_date,
                        papers.journal as paper_journal
                    FROM cards
                    LEFT OUTER JOIN (
                        SELECT
                        user_pin_card.id as id, 
                        user_pin_card.card_id as card_id, 
                        user_pin_card.paper_id as paper_id,
                        user_pin_card.user_id as user_id
                        FROM user_pin_card
                        WHERE user_pin_card.user_id = '.$userId.'
                    ) as user_pins ON user_pins.card_id = cards.id
                    LEFT JOIN (SELECT
                        category_card.category_id as category_id,
                        category_card.card_id as card_id
                        FROM category_card GROUP BY card_id
                    ) as cat_card ON cards.id = cat_card.card_id
                    LEFT JOIN categories ON cat_card.category_id = categories.id
                    LEFT JOIN users ON user_pins.user_id = users.id
                    LEFT JOIN papers ON user_pins.paper_id = papers.id
                    LEFT OUTER JOIN follows ON follows.user_id = users.id 
                    LEFT OUTER JOIN ( 
                        SELECT category_card.card_id as card_id, category_user.category_id as category_id 
                        FROM category_card 
                        LEFT JOIN category_user ON category_user.category_id = category_card.category_id 
                        WHERE category_user.user_id = '.$userId.' ) 
                        as user_categories ON user_categories.card_id = cards.id 
                    LEFT OUTER JOIN ( 
                        SELECT follows.id as id, follows.card_id as card_id 
                        FROM follows WHERE follows.user_id = '.$userId.' ) 
                        as user_follows ON user_follows.card_id = cards.id'.
                    ($categoryId ? ' WHERE categories.id = '.$categoryId : '').'
                    GROUP BY cards.id 
                    ORDER BY count_pinned_papers DESC
                    LIMIT ?, ?';
        } else {
            $query = 'SELECT
                        cards.id as id, cards.title as title,
                        cards.short_description as short_description,
                        COUNT(DISTINCT user_pin_card.id) as count_pinned_papers,
                        (IF(categories.title != "", categories.title, "Uncategorized")) as category_title,
                        cards.owner as owner,
                        cards.image as image
                    FROM cards
                    LEFT JOIN user_pin_card ON cards.id = user_pin_card.card_id
                    LEFT JOIN (SELECT 
                                category_card.category_id as category_id, 
                                category_card.card_id as card_id
                                FROM category_card GROUP BY card_id
                    ) as cat_card ON cards.id = cat_card.card_id
                    LEFT JOIN categories ON cat_card.category_id = categories.id
                    '.($categoryId ? ' WHERE categories.id = '.$categoryId : '').
                    ($q ? ' WHERE MATCH (cards.title, cards.short_description, cards.description, cards.owner) AGAINST (\''.$q.'\' IN BOOLEAN MODE) OR cards.title LIKE \'%'.$q.
                        '%\' OR cards.short_description LIKE \'%'.$q.'%\' 
                        OR cards.description LIKE \'%'.$q.'%\'' : '').'
                GROUP BY cards.id
                ORDER BY count_pinned_papers DESC
                LIMIT ?, ?';
        }
        return DB::select($query, [$offset, $limit]);
    }

    /**
     * Get popular cards
     * @param integer $userId
     * @param integer $offset
     * @param integer $limit
     * @return 
     */
    public static function getFollowingCards($userId, $offset, $limit)
    {
        $query = 'SELECT
                    cards.id as id, cards.title as title,
                    cards.short_description as short_description,
                    COUNT(DISTINCT user_pins.id) as count_pinned_papers,
                    (IF(categories.title != "", categories.title, "Uncategorized")) as category_title,
                    cards.owner as owner,
                    cards.image as image,
                    users.name as user_name,
                    users.last_name as user_last_name,
                    users.slug as user_slug,
                    users.id as user_id,
                    users.avatar as user_avatar,
                    papers.title as paper_title,
                    DATE_FORMAT(papers.publication_date, \'%d %b %Y\') as paper_date,
                    papers.journal as paper_journal, 
                    1 as followed
                FROM follows
                LEFT JOIN cards ON cards.id = follows.card_id
                LEFT OUTER JOIN ( 
                        SELECT user_pin_card.id as id, user_pin_card.card_id, user_pin_card.paper_id as paper_id
                        FROM user_pin_card
                        WHERE user_pin_card.user_id = '.$userId.' )
                    as user_pins ON user_pins.card_id = cards.id
                LEFT JOIN category_card ON category_card.card_id = cards.id
                LEFT JOIN categories ON categories.id = category_card.category_id
                LEFT JOIN paper_user ON paper_user.paper_id = user_pins.paper_id
                LEFT JOIN papers ON papers.id = paper_user.paper_id
                LEFT JOIN users ON users.id = paper_user.user_id
                WHERE follows.user_id = '.$userId.'
                GROUP BY cards.id
                ORDER BY follows.add_date DESC
                LIMIT ?, ?';
        return DB::select($query, [$offset, $limit]);
    }

    /**
     * Get pins for card
     * @param integer $id
     * @param integer $offset
     * @param integer $limit
     * @return 
     */
    public static function getPinsInfoForCard($id, $offset, $limit)
    {
        $query = 'SELECT 
                    users.avatar as user_avatar, 
                    users.id, 
                    count(DISTINCT user_pin_card.id) as count 
                FROM user_pin_card
                LEFT JOIN users ON users.id = user_pin_card.user_id
                WHERE user_pin_card.card_id = ?
                GROUP BY users.id
                LIMIT ?, ?';
        return DB::select($query, [$id, $offset, $limit]);
    }

    /**
     * Search cards by title, summary, description
     * @param  string $q
     * @param  integer $offset
     * @param  integer $limit
     * @return stdClass
     */
    public static function searchByString($q, $offset, $limit = 5)
    {
        $sql = 'SELECT id, title, short_description, image FROM cards 
                WHERE MATCH (title, short_description, description, owner) AGAINST (? IN BOOLEAN MODE) OR title LIKE ? OR short_description LIKE ? 
                OR description LIKE ? LIMIT ?, ?';
        return \DB::select($sql, [$q, '%'.$q.'%', '%'.$q.'%', '%'.$q.'%', $offset, $limit]);
    }
}