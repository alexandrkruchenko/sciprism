<?php
namespace App\Services;

class AjaxService {

    /**
     * Build pagination for Ajax requests
     * @param  integer $countPages
     * @param  integer $offset
     * @param  integer $limit
     * @return String
     */
    public static function buildAjaxPagination($countItems, $offset, $limit)
    {
        $html = '';
        if (($countItems / $limit) > 1) {
            $html .= '<div class="paginattion-new"><ul>';
            $currentPage = ($offset / $limit) + 1;
            $firstPage = 1;
            $lastPage = ceil($countItems / $limit);
            $whole = 10;
            $half = $whole / 2;
            $from = $currentPage < $whole ? 1 : floor($currentPage / $whole) * $whole;
            $to = $currentPage >= $whole ? 
                (ceil($currentPage / $whole) * $whole > $lastPage ? $lastPage : ceil($currentPage / $whole) * $whole) : ($whole < $lastPage ? $whole : $lastPage);
            $to = $to == $from ? 
                ($from + $whole > $lastPage ? $lastPage : $from + $whole) : $to;
            if ($currentPage > $half + 1) {
                $html .= '<li><a href="#" data-offset="0">«</a></li>';
            }
            for($i = $from; $i <= $to; $i++) {
                $html .= '<li>'.($currentPage == $i ? 
                    '<a href="#" class="active" onclick="return false;">'.
                        $i.'</a></li>' : '<a href="#" data-offset="'.
                        (($i - 1)* $limit).'">'.$i.'</a></li>');
            }
            if ($currentPage < $lastPage - ($half - 1)) {
                $html .= '<li><a href="#" data-offset="'.(($lastPage - 1) * $limit)
                    .'">»</a></li>';
            }
            $html .= '</ul></div>';
        }
        return $html;
    }
}