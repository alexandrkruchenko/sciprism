<?php
namespace App\Services;

use App\Card;
use App\UserPinCard;

use Illuminate\Support\Facades\Auth;

class CardService {


    /**
     * Build cards html feed for page
     * @param  Card $cards
     * @param  bool $isFollowing
     * @return string
     */
    public static function buildFeedHtml($cards, $isFollowing = false)
    {
        $html = '';
        if (count($cards)) {
            foreach ($cards as $card) {
                $followed = $card->isFollowed(Auth::user()->id);
                $cardCategory = $card->categories;
                $cardLink = '/innovation/'.$card->id;
                /*$pinBlockLeftHtml = '';
                if ($forUser) {
                    $userLink = '/profile/'.($card->user_slug ? $card->user_slug : $card->user_id);
                    if ($card->paper_title) {
                        $pinBlockLeftHtml = '<div class="photo">'.
                                                '<img src="'.url($card->user_avatar).'" alt="">'.
                                            '</div>'.
                                            '<div class="text">'.
                                                '<p>'.$card->paper_title.'</p>'.
                                                '<div class="info-person">'.
                                                    '<a href="'.$userLink.'" class="name">'.$card->user_name.' '.$card->user_last_name.
                                                    '</a>'.
                                                    '<span>'.$card->paper_journal.'</span>'.
                                                    '<div class="date">'.$card->paper_date.'</div>'.
                                                '</div>'.
                                            '</div>';
                    }
                } else {
                    $pins = Card::getPinsInfoForCard($card->id, 0, 3);
                    if ($pins) {
                        $pinBlockLeftHtml = '<div class="pinned"><ul>';
                        foreach ($pins as $pin) {
                            $pinBlockLeftHtml .= '<li><a href="#"><img src="'.$pin->user_avatar
                                .'" alt=""></a></li>';
                        }
                        $pinBlockLeftHtml .= '<li><a href="#">'.
                            ($card->count_pinned_papers > 10 ? '+10' : 
                                $card->count_pinned_papers).'</li>';
                        $pinBlockLeftHtml .= '</ul><a href="#" class="added" '.'onClick="$(\'.sing-up.btn\').trigger(\'click\');  return false;"'.'>Pinned Publications</a></div>';
                    }
                }*/
                $html .= '<div class="item">'.
                            '<div class="row">'.
                                '<div class="image">'.
                                    /*'<a href="#card-id-'.$card->id.'" data-toggle="card-page">'.
                                        ($card->image ? 
                                            '<img src="'.url($card->image).'" alt="">' :
                                            ''
                                        ).
                                    '</a>'.*/
                                '</div>'.
                                '<div class="info">'.
                                        '<div class="wr-left">'.
                                        /*'<a href="javascript:void(0);" class="pin" data-card-id="'.$card->id.'"'. 
                                            ($forUser ? 'data-toggle="papers-wrapper"' : 'onClick="$(\'.sing-up.btn\').trigger(\'click\');  return false;"').'>'.
                                            '<span class="plus"></span>'.
                                            '<span class="text">pin</span>'.
                                            '<span class="count">'.$card->count_pinned_papers.'</span>'.
                                        '</a>'.*/
                                        '<a class="title" data-toggle="card-page" href="'.
                                            $cardLink.'">'.
                                            CardService::substrwords($card->title, 90).
                                        '</a>'.
                                        '<p>'.CardService::substrwords($card->short_description, 275).'</p>'.
                                        '<ul class="tech">'.
                                            '<li>'.
                                                '<div class="icon">'.
                                                    '<img src="/img/diagnostic.png" alt="">'.
                                                '</div>'.
                                                '<a >'.(count($cardCategory) ? $cardCategory->implode('title', ', ') : '').'</a>'.
                                            '</li>'.
                                            '<li>'.
                                                '<div class="icon"><img src="/img/university.png" alt=""></div>'.
                                                '<a >'.$card->owner.'</a>'.
                                            '</li>'.                             
                                        '</ul>'.
                                        '<div class="right-btn">'.
                                            '<a href="" class="upload"></a>'.
                                            '<a href="" class="follow'.
                                                ($followed ? ' active' : '').'" data-card-id="'.$card->id.'""></a>'.
                                        '</div>'.
                                    '</div>'.
                                    '<div class="wr-right">'.
                                        '<div class="wr-chart">'.   
                                           '<a href="'.$cardLink.'" data-toggle="card-page">'.
                                               '<div class="chart-semi'.$card->id.' chart-semi"'.  
                                                    'style="min-width: 175px; height: 160px;'. 
                                                    'max-width: 600px; margin: 0 auto"></div>'.
                                               '<span>Impact</span>'.
                                           '</a>'.
                                        '</div>'.                              
                                    '</div>'.
                                '</div>'.
                            '</div>'.
                            /*'<div class="row-bottom">'.
                                ($forUser && $pinBlockLeftHtml ? '<h4>Your pins:</h4>' : '').
                                '<div class="pins">'.
                                    '<div class="left'.($pinBlockLeftHtml ? '' : ' empty').'">'.
                                    $pinBlockLeftHtml.
                                    '</div>'.
                                    '<div class="right">'.
                                        '<a href="#" class="upload"></a>'.
                                        ($forUser ? '<a href="javascript:void(0);" class="follow '.
                                            ($card->followed ? 'active' : '').'"'. 
                                                ' data-card-id="'.$card->id.'"></a>' : ''). 
                                    '</div>'.
                                '</div>'.
                            '</div>'.*/
                        '</div>';
            }
        }
        return $html;
    }

    /**
     * Cut string
     * @param  string $text
     * @param  integer $maxchar
     * @param  string $end
     * @return string
     */
    public static function substrwords($text, $maxchar, $end='...')
    {
        if (!$text) {
            return '';
        }
        if (strlen($text) > $maxchar || $text == '') {
            $words = preg_split('/\s/', $text);    
            $output = '';
            $i      = 0;
            while (1) {
                $length = strlen($output)+strlen($words[$i]);
                if ($length > $maxchar) {
                    break;
                } 
                else {
                    $output .= " " . $words[$i];
                    ++$i;
                }
            }
            $output .= $end;
        } 
        else {
            $output = $text;
        }
        return $output;
    }

    /**
     * Render comment for Card
     * @param  Comment $comment
     * @return string
     */
    public static function renderComment($comment)
    {
        $commentAuthor = $comment->user;
        return '<li>
                <div class="left">
                    <div class="photo">
                        <a href="'.$commentAuthor->getUserLink().'">
                            <img src="'.$commentAuthor->avatar.'" alt="">
                        </a>
                    </div>
                </div>
                <div class="text">
                    <a href="'.$commentAuthor->getUserLink().
                        '" class="name">'.$commentAuthor->name.' '.
                        $commentAuthor->last_name.'</a>
                    <div class="date">'.(self::getDate($comment->created_at)).'</div>
                    <p>'.$comment->text.'</p>
                </div>
            </li>';
    }

    /**
     * Get date today, yesterday
     * @param  [type] $date [description]
     * @return [type]       [description]
     */
    public static function getDate($timestamp)
    {
        $prefix = '';
        $timestamp = strtotime($timestamp);
        $date = date('d/m/Y', $timestamp);
        $newDate = '';
        if ($date == date('d/m/Y')) {
            $prefix = 'today';
        } else if ($date == date('d/m/Y', time() - (24 * 60 * 60))) {
            $prefix = 'yesterday';
        }
        if ($prefix) {
            $newDate = $prefix.' at '.date('h:i A', $timestamp);
        } else {
            $newDate = date('h:i A M Y', $timestamp);
        }
        return $newDate;
    }


    public static function getImpact($cards, $amount = 3)
    {
        $impacts = [];
        foreach ($cards as $card) {
            $categories = Card::find($card->id)->categories;
            $countCategories = count($categories);
            $divider = $countCategories > $amount ? $amount : $countCategories;
            $data = [];
            $colors = [];
            foreach ($categories as $index => $category) {
                if ($amount == $index) {
                    break;
                }
                $data[] = [
                    $category->title,
                    round((100 / $divider))
                ];
                $colors[] = $category->color;
            }
            $impacts[] = [
                'id' => $card->id,
                'data' => [
                    $data,
                    $colors
                ]
            ];
        }
        return $impacts;
    }

    /**
     * Render card for publication page
     * @param Card card
     * @return string
     */
    public static function renderCard($card)
    {
        $html = '<li>
                    <div class="icon">
                        <img src="/img/file-2.svg" alt="">
                    </div>
                    <div class="center">
                        <a href="'.$card->id.'" target="_blank" class="title">'
                            .$card->title.'</a>
                        <p>'.$card->short_description.'</p>
                    </div>
                <div class="right">
                    <div class="check-pinned">
                        <i class="fas fa-check-circle"></i>
                        <span>Pinned</span>
                    </div>
                </div>
            </li>';
        return $html;
    }
}