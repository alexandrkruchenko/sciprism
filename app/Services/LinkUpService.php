<?php
namespace App\Services;

use App\User;
use App\Userjob;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class LinkUpService {

    public static $apiKey = 'A6DE9EFAC634C49E6E2C77ED117E9574';
    public static $searchKey = '13bf73687d3cac49040178356f73c25d';

    /**
     * Get jobs by keywords
     * @param integer $page
     * @return stdClass
     */
    public static function getJobs($page = 0, $keywords = '', $location = '')
    {
        if ($keywords) {
             $q = urlencode($keywords);
        } else {
            $q = Auth::user()->getProfileKeywords();
        }
        $query = 'https://www.linkup.com/developers/v-1/search-handler.js?api_key='.
            self::$apiKey.'&embedded_search_key='.self::$searchKey.'&orig_ip='.
            $_SERVER['REMOTE_ADDR'].'&keyword='.$q;
        if ($location) {
            $query .= '&location='.urlencode($location);
        }
        if ($page) {
            $query .= '&page='.$page;
        }
        $cacheName = md5($query);
        $response = Cache::get('jobs_'.$cacheName);
        if (!$response) {
            $client = new Client();
            $response = @json_decode($client->get($query)->getBody());
            Cache::put('jobs_'.$cacheName, $response, 20);
        }
        return $response;


    }

    /**
     * Build html for network tab
     * @param  array $jobList
     * @param  array $userJobs
     * @param  bool
     * @return string
     */
    public static function buildHtml($jobList, $userJobs, $more)
    {
        $html = '';
        if (count($jobList)) {
            foreach ($jobList as $key => $job) {
                $job->name = str_replace("'", '', $job->name);
                if ($key % 5 == 0 || $key == 0) {
                    if ($key != 0) {
                        $html .= '</tbody></table></div>';
                    }
                    $html .= '<div class="col"><table class="table-jobs"><tbody>';
                    if (($key == 0 || $key == 5) && !$more) {
                        $html .= '<thead><tr><td>Job</td><td></td></tr></thead><tbody>';
                    }
                }
                $isActive = true;
                if ($userJobs) {
                    if (!$userJobs->contains('key', $job->key)) {
                        $isActive = false;
                    }
                }
                $html .= '<tr>
                            <td>
                                <div class="icon">
                                    <img src="/img/jobs.png">
                                </div>
                                <p>'.$job->name.'</p>
                            </td>
                            <td>
                                <a href="'.$job->link.'" class="link" target="_blank">View</a>
                                <a href="#" class="star'.($isActive ? ' active' : '').'" data-object=\''.json_encode($job).'\'>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>';
            }
            $html .= '</tbody></table></div>';
        } else {
            $html = '<div class="wrap-empty">
                        <p>jobs not found.</p>
                    </div>';
        }
        return $html;             
        
    }
}