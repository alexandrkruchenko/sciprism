<?php
namespace App\Services;

use App\Category;

class CategoryService {

    /**
     * Get string categories via comma
     * @param  $model
     * @return string
     */
    public static function getCategoriesString($categories)
    {
        $categoryString = '';
        foreach ($categories as $key => $category) {
            if ($key == 0) {
                $categoryString .= $category->title;
            } else {
                $categoryString .= ', '.$category->title;
            }
        }
        return $categoryString;
    }
}