<?php
namespace App\Services;

use App\Paper;
use App\User;
use App\PaperMeta;

use Illuminate\Support\Facades\Auth;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class PaperService {


    /**
     * Get paper from Pubmed by author, ID or DOI
     * @param  string $q
     * @param integet $offset
     * @param integet $limit
     * @return array
     */
    public static function getPubmedPaperByCreteria($q, $offset, $limit = 10)
    {
        //create http client
        $client = new Client();

        //determine field and get pmids id
        $field = '';
        $papersCount = 1;
        $pmids  = [];
        if (intval($q)) { //id
            $pmids[] = $q;
        } elseif (strpos($q, '/') !== FALSE) { //doi
            $result = $client->get('https://www.ncbi.nlm.nih.gov/pmc/utils/idconv/v1.0/?tool='.config('app.name').'&email='.config('settings.name').'&ids='.$q);
            $xml = @simplexml_load_string($result->getBody());
            if ($xml) {
                $pmids[] = $xml->record['pmid'];
            }
        } else { //author
            $result = $client->get('https://eutils.ncbi.nlm.nih.gov/entrez/eutils/'.'esearch.fcgi?db=pubmed&retstart='.$offset.
                    '&retmode=xml&retmax='.$limit.'&term='.$q.'[author]&sort=most+recent');
            $xml = @simplexml_load_string($result->getBody());
            if ($xml) {
                foreach ($xml->IdList->children() as $id) {
                    $pmids[] = (int)$id;
                }
                $papersCount = (int)$xml->Count;
            }
        }

        if (count($pmids)) {
            //get papers
            $result = $client->get('https://eutils.ncbi.nlm.nih.gov/entrez/eutils/'.'esummary.fcgi?db=pubmed&id='.implode(',', $pmids).'&retmode=xml');
            $xml = @simplexml_load_string($result->getBody());
            if ($xml) {
                return [
                    'xml' => $xml,
                    'count' => $papersCount
                ];
            }
        }
        return [];
    }

    /**
     * Save pubmed articles in db
     * @param  array $ids 
     * @return array
     */
    public static function savePumbmedPapers($ids) {
        $papersIds = [];
        $client = new Client();
        $result = $client->get('https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id='.implode(',', $ids).'&retmode=xml');
        $xml = @simplexml_load_string($result->getBody());
        if ($xml) {
            foreach ($xml->children() as $pubmedArticle) {
                $meta = [];
                /*$paper = Paper::firstOrCreate([
                    'external_id' => (int)$pubmedArticle->MedlineCitation->PMID
                ]);*/
                $paper = new Paper;
                $paper->title = (string)$pubmedArticle->MedlineCitation->Article->ArticleTitle;
                $paper->description = (string)$pubmedArticle->MedlineCitation->Article
                    ->Abstract->AbstractText;
                $paper->external_id = (int)$pubmedArticle->MedlineCitation->PMID;
                $paper->type = config('constants.pubmed_paper');
                $paper->journal = (string)$pubmedArticle->MedlineCitation->Article->Journal->Title;

                $pubDate = '';
                if(isset($pubmedArticle->MedlineCitation->Article->Journal->JournalIssue->PubDate->Year)){
                    $pubDate = date('Y-m-d H:i:s', strtotime(
                        $pubmedArticle->MedlineCitation
                            ->Article->Journal->JournalIssue->PubDate->Year.'-'.
                        $pubmedArticle->MedlineCitation
                            ->Article->Journal->JournalIssue->PubDate->Month.'-'
                        .$pubmedArticle->MedlineCitation
                            ->Article->Journal->JournalIssue->PubDate->Day
                    ));
                } else {
                    $pubDate = date('Y-m-d H:i:s', strtotime(
                        $pubmedArticle->MedlineCitation
                            ->DateRevised->Year.'-'.
                        $pubmedArticle->MedlineCitation
                            ->DateRevised->Month.'-'
                        .$pubmedArticle->MedlineCitation
                            ->DateRevised->Day
                    ));
                }
                $paper->publication_date = $pubDate;

                $authors = [];
                $affiliations = [];
                foreach ($pubmedArticle->MedlineCitation
                    ->Article->AuthorList->children() as $author) {
                    $authors[] = $author->LastName.' '.$author->ForeName;
                    if (isset($author->AffiliationInfo->Affiliation)) {
                        $affiliations[] = $author->AffiliationInfo->Affiliation;
                    }
                }
                $paper->authors = implode(',', array_slice($authors, 0 , 2));
                $paper->save();
                $papersIds[] = $paper->id;

                /***save meta****/
                //authors
                $meta[] = [
                    'paper_id' => $paper->id,
                    'name' => 'authors',
                    'value' => implode(',', $authors)
                ];

                //affiliations
                $meta[] = [
                    'paper_id' => $paper->id,
                    'name' => 'affiliations',
                    'value' => implode(',', $affiliations)
                ];

                //emails
                $emails = [];
                foreach ($affiliations as $affiliation) {
                    preg_match_all(
                        '/[a-z0-9_\-\+\.]+@[a-z0-9\-]+\.([a-z]{2,4})(?:\.[a-z]{2})?/i', 
                        $affiliation, 
                        $matches
                    );
                    if (isset($matches[0][0])) {
                        $emails[] = $matches[0][0];
                    }
                }
                $meta[] = [
                    'paper_id' => $paper->id,
                    'name' => 'emails',
                    'value' => implode(',', $emails)
                ];

                //journal issue
                if (isset($pubmedArticle->MedlineCitation->Article->Journal->JournalIssue->Issue)) {
                    $meta[] = [
                        'paper_id' => $paper->id,
                        'name' => 'journal_issue',
                        'value' => 
                            $pubmedArticle->MedlineCitation->Article->Journal->JournalIssue->Issue
                    ];
                }

                //abstract
                if (isset($pubmedArticle->MedlineCitation->Article->Abstract->AbstractText)) {
                    $meta[] = [
                        'paper_id' => $paper->id,
                        'name' => 'abstract',
                        'value' => (string)$pubmedArticle->MedlineCitation
                             ->Article->Abstract->AbstractText
                    ];
                }

                //keywords
                if(isset($pubmedArticle->MedlineCitation->KeywordList)) {
                    $keywords = [];
                    foreach ($pubmedArticle->MedlineCitation->KeywordList->children() as $keyword) {
                        $keywords[] = (string)$keyword;
                    }
                    $meta[] = [
                        'paper_id' => $paper->id,
                        'name' => 'keywords',
                        'value' => implode(',', $keywords)
                    ];
                }
                PaperMeta::insert($meta);           

            }
        }
        return $papersIds;
    }


    /**
     * Render publication for paper page
     * @param  Publication $publication
     * @return string
     */
    public static function renderPublication($publication)
    {
        $user = $publication->userPin()->first();
        $html = '<li>
                    <div class="icon">
                        <img src="/img/file-2.svg" alt="">
                    </div>
                    <div class="center">
                        <a href="'.$publication->getPubmedLink().'" target="_blank" class="title">'
                            .$publication->title.'</a>
                        <p>'.$publication->getPaperDescription().'</p>
                    </div>
                <div class="right">
                    <div class="check-pinned">
                        <i class="fas fa-check-circle"></i>
                        <span>Pinned</span>
                    </div>
                </div>
            </li>';
        return $html;
    }

    /**
     * Render impact areas for profile page
     * @param stdClass $areas
     * @param integer $limit
     * @return string
     */
    public static function renderImpactAreasForProfilePage($areas)
    {
        $html = '';
        if (count($areas)) {
            $html = '<div class="profile--analysis"><ul>';
            $total_count = 0;
            foreach ($areas as $area) {
                $total_count += $area->count;
            }
            foreach ($areas as $key => $area) {
                $percent = round($area->count * 100 / $total_count);
                $color = 'green';
                if ($key == 1) {
                    $color = 'sky-blue';
                } elseif ($key > 1) {
                    $color = 'blue';
                }
                $html .= '<li>
                            <div class="cont '.$color.'" data-pct="'.$percent.'">
                                <svg class="svg" width="200" height="200" viewport="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                  <circle r="90" cx="100" cy="100" fill="transparent" stroke-dasharray="565.48" stroke-dashoffset="0"></circle>
                                  <circle class="bar" r="90" cx="100" cy="100" fill="transparent" stroke-dasharray="565.48" stroke-dashoffset="0" stroke="#24925a" style="stroke-dashoffset: 311.018px;"></circle>
                                </svg>
                                <input class="percent" name="percent" value="'.$percent.'">
                            </div>
                            <div class="text">'.$area->title.'</div>
                        </li>';
            }
            $html .= '</ul></div>';
        }
        return $html;
    }

    /**
     * Render impact areas for feed page
     * @param stdClass $areas
     * @param integer $limit
     * @return string
     */
    public static function prepareImpactAreas($areas)
    {
        if (count($areas)) {
            $totalCount = 0;
            $newAreas = [];
            foreach ($areas as $area) {
                $totalCount += $area->count;
            }
            foreach ($areas as $key => $area) {
                $percent = round($area->count * 100 / $totalCount);
                $newAreas[] = [
                    'title' => $area->title,
                    'percent' => $percent,
                    'color' => $area->color,
                ];
            }
            $areas = $newAreas;
        }
        return $areas;
    }

    /**
     * Build papers html feed for page
     * @param  Paper $papers
     * @return string
     */
    public static function buildFeedHtml($papers)
    {
        $html = '';
        if (count($papers)) {
            foreach ($papers as $paper) {
                $paperCategory = $paper->categories;
                $followed = $paper->isFollowed(Auth::user()->id);
                $authors = explode(',', $paper->getPubmedAuthors());
                $author = '';
                if (count($authors) > 1) {
                    $author = reset($authors).'...'.end($authors);
                } else {
                    $author = $authors[0];
                }
                $paperLink = '/paper/'.$paper->external_id;
                $html .= '<div class="item">'.
                            '<div class="row">'.
                                '<div class="image">'.
                                '</div>'.
                                '<div class="info">'.
                                        '<div class="wr-left">'.
                                        '<a class="title" data-toggle="card-page-p" href="'.
                                            $paperLink.'">'.
                                            CardService::substrwords($paper->title, 90).
                                        '</a>'.
                                        '<p>'.CardService::substrwords($paper->description, 275).'</p>'.
                                        '<div class="author">Journal: '.$paper->journal.'</div>'.
                                        '<ul class="tech">'.
                                            '<li>'.
                                                ($author ? '<a >'.$author.'</a>' : '').
                                            '</li>'.                            
                                        '</ul>'.
                                        '<ul class="tech">'.
                                            '<li>'.
                                                (   count($paperCategory) ?
                                                    '<div class="icon">'.
                                                        '<img src="/img/diagnostic.png" alt="">'.
                                                    '</div>'.
                                                    '<a >'.
                                                        $paperCategory->implode('title', ', ').
                                                        '</a>' : 
                                                    ''
                                                ).
                                            '</li>'.                            
                                        '</ul>'.
                                        '<div class="right-btn">'.
                                            '<a href="" class="upload"></a>'.
                                            '<a href="" class="follow'.
                                            ($followed ? ' active' : '').
                                                '" data-paper-id="'.$paper->id.'"></a>'.
                                        '</div>'.
                                    '</div>'.
                                    '<div class="wr-right">'.
                                        '<div class="wr-chart">'.   
                                           '<a href="'.$paperLink.'" data-toggle="card-page-p">'.
                                               '<div class="chart-semi-p'.$paper->id.' chart-semi"'.  
                                                    'style="min-width: 175px; height: 160px;'. 
                                                    'max-width: 600px; margin: 0 auto"></div>'.
                                               '<span>Impact</span>'.
                                           '</a>'.
                                        '</div>'.                              
                                    '</div>'.
                                '</div>'.
                            '</div>'.
                        '</div>';
            }
        }
        return $html;
    }

    public static function getImpact($papers, $amount = 3)
    {
        $impacts = [];
        foreach ($papers as $paper) {
            $categories = Paper::find($paper->id)->categories;
            $countCategories = count($categories);
            $divider = $countCategories > $amount ? $amount : $countCategories;
            $data = [];
            $colors = [];
            foreach ($categories as $index => $category) {
                if ($amount == $index) {
                    break;
                }
                $data[] = [
                    $category->title,
                    round((100 / $divider))
                ];
                $colors[] = $category->color;
            }
            $impacts[] = [
                'id' => $paper->id,
                'data' => [
                    $data,
                    $colors
                ]
            ];
        }
        return $impacts;
    }

    /**
     * Render comment for Paper
     * @param  PaperComment $comment
     * @return string
     */
    public static function renderComment($comment)
    {
        $commentAuthor = $comment->user;
        return '<li>
                <div class="left">
                    <div class="photo">
                        <a href="'.$commentAuthor->getUserLink().'">
                            <img src="'.$commentAuthor->avatar.'" alt="">
                        </a>
                    </div>
                </div>
                <div class="text">
                    <a href="'.$commentAuthor->getUserLink().
                        '" class="name">'.$commentAuthor->name.' '.
                        $commentAuthor->last_name.'</a>
                    <div class="date">'.(CardService::getDate($comment->created_at)).'</div>
                    <p>'.$comment->text.'</p>
                </div>
            </li>';
    }

    public static function buildPapersForProfileHtml($papers)
    {
        $html = '';
        foreach ($papers as $paper) {
            $pinnededCount = $paper->pinnedCards()->count();
            $html .= '<li>
                        <div class="icon"><i class="fa fa-file-text-o" aria-hidden="true"></i></div>
                        <div class="text">
                            <a href="'.$paper->getPubmedLink().'">'.$paper->title.'</a>
                            <p>'.$paper->getPaperDescription().'</p>
                        </div>'.
                        ($pinnededCount ? 
                            '<a href="" class="pins-count" data-paper-id="'.
                                $paper->id.'" data-toggle="pinned-wrapper">'.
                                $pinnededCount.' Pins</a>' : 
                            '').
                    '</li>';
        }
        return $html;
    }



    public static function renderImpactAreasForFeedPage($impactAreas)
    {
        $html = '';
        if ($impactAreas) {
            $html = '<div class="wr-chart-side">
                        <div class="title">Your impact</div>
                        <div class="side-chart" id="chartContainer" style="height: 150px; width: 100%;"></div>
                    </div>
                    <ul class="percent-ul">';
            foreach($impactAreas as $key => $area) {
                if ($key == 3) {
                    break;
                }
                $html .= '<li class="percent'.$key.'">
                    <span class="percent" style="background-color: '.$area['color'].'">
                        '.$area['percent'].'%
                    </span>
                    <span class="text">'.$area['title'].'</span>
                </li>';
            }                                             
            $html .= '</ul><a href="'.Auth::user()->getUserLink().
                '" class="explore">Explore</a>';
        }
        return $html;
    }
}