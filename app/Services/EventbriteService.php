<?php
namespace App\Services;

use App\User;
use App\UserEvent;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class EventbriteService {

    public static $apiKey = 'ZFRPLVW2D6YMLYYVRQUW';

    /**
     * Get events by keywords
     * @param integer $page
     * @return stdClass
     */
    public static function getEvents($page = 0, $keywords = '', $location = '')
    {
        if ($keywords) {
            $q = urlencode($keywords);
        } else {
            $q = Auth::user()->getProfileKeywords();
        }
        if ($keywords) {
            $q = urlencode($keywords);
        }
        $query = 'https://www.eventbriteapi.com/v3/events/search/?token='.self::$apiKey.'&q='.$q;
        if ($location) {
            $query .= '&location.address='.urlencode($location);
        }
        if ($page) {
            $query .= '&page='.$page;
        }
        $cacheName = md5($query);
        $response = Cache::get('events_'.$cacheName);
        if (!$response) {
            $client = new Client();
            $response = @json_decode($client->get($query)->getBody());
            Cache::put('events_'.$cacheName, $response, 20);
        }
        return $response;


    }

    /**
     * Build html for network tab
     * @param  array $eventList
     * @param  array $userEvents
     * @param  bool
     * @return string
     */
    public static function buildHtml($eventList, $userEvents, $more)
    {
        $html = '';
        if (count($eventList)) {
            foreach ($eventList as $key => $event) {
                $event->name = str_replace("'", '', $event->name);
                if ($key % 5 == 0 || $key == 0) {
                    if ($key != 0) {
                        $html .= '</tbody></table></div>';
                    }
                    $html .= '<div class="col"><table class="table-network"><tbody>';
                    if (($key == 0 || $key == 5) && !$more) {
                        $html .= '<thead><tr><td>Event</td><td></td></tr></thead><tbody>';
                    }
                }
                $isActive = true;
                if ($userEvents) {
                    if (!$userEvents->contains('external_id', $event->external_id)) {
                        $isActive = false;
                    }
                }
                $html .= '<tr>
                            <td>
                                <div class="icon">
                                    <img src="/img/calendar.png" title="'.$event->start_date.'">
                                </div>
                                <p>'.$event->name.'</p>
                            </td>
                            <td>
                                <a href="'.$event->link.'" class="link" target="_blank">View</a>
                                <a href="#" class="star'.($isActive ? ' active' : '').'" data-object=\''.json_encode($event).'\'>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>';
            }
            $html .= '</tbody></table></div>';
        } else {
            $html = '<div class="wrap-empty">
                        <p>Events not found.</p>
                    </div>';
        }
        return $html;             
        
    }
}