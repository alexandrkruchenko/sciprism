<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * Get realted cards
     * @param  integer $limit
     * @return Card
     */
    public function getRelated($limit = 5)
    {
        return Post::where('id', '<>', $this->id)->inRandomOrder()->limit(3)->get();
    }
}
